package com.trafficviolationdriver.ui.common;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.trafficviolationdriver.R;
import com.trafficviolationdriver.databinding.ActivitySplashBinding;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.ui.home.DashBoardActivity;
import com.trafficviolationdriver.util.AppConstants;
import com.trafficviolationdriver.util.SessionManager;
import com.trafficviolationdriver.util.Utils;

public class SplashActivity extends BaseActivity {

    ActivitySplashBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        Utils.setLocale(this, session.getDataByKey(SessionManager.KEY_LANGUAGE, AppConstants.EN));
        runSplash();
    }


    private void runSplash() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                redirectToAnotherPage();
            }
        }, 3500);
    }

    private void redirectToAnotherPage() {
        if (session.isLoggedIn()) {
            startActivity(DashBoardActivity.class);
        } else {
            startActivity(LanguageSelectionActivity.class);
        }
    }

    /**
     * @param cls ClassName.class to start new Activity
     */
    private void startActivity(Class<?> cls) {
        Intent intent = new Intent(SplashActivity.this, cls);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {

    }
}
