package com.trafficviolationdriver.ui.common;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.trafficviolationdriver.R;
import com.trafficviolationdriver.adapter.LanguageSelectionAdapter;
import com.trafficviolationdriver.databinding.ActivityLanguageSelectionBinding;
import com.trafficviolationdriver.interfaces.OnRecyclerViewItemClicked;
import com.trafficviolationdriver.model.LanguageItemModel;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.ui.home.DashBoardActivity;
import com.trafficviolationdriver.ui.login.LoginActivity;
import com.trafficviolationdriver.util.AppConstants;
import com.trafficviolationdriver.util.SessionManager;
import com.trafficviolationdriver.util.Utils;

import java.util.ArrayList;

public class LanguageSelectionActivity extends BaseActivity implements OnRecyclerViewItemClicked<LanguageItemModel> {

	ActivityLanguageSelectionBinding mBinding;
	ArrayList<LanguageItemModel> languageItemList = new ArrayList<>();
	LanguageSelectionAdapter mAdapter;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mBinding = DataBindingUtil.setContentView(LanguageSelectionActivity.this, R.layout.activity_language_selection);
		initView();
	}

	private void initView() {
		if (session.isLoggedIn()) {
			setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.selection_language));
		} else {
			setUpToolbarWithoutBackArrow(getString(R.string.selection_language));
		}
		setUpRecyclerView();
		refreshData();
	}


	private void setUpRecyclerView() {

		LanguageItemModel mLanguageItemModel = new LanguageItemModel(getString(R.string.english), AppConstants.EN);
		languageItemList.add(mLanguageItemModel);
		mLanguageItemModel = new LanguageItemModel(getString(R.string.hindi), AppConstants.HI);
		languageItemList.add(mLanguageItemModel);
		mLanguageItemModel = new LanguageItemModel(getString(R.string.gujarati), AppConstants.GU);
		languageItemList.add(mLanguageItemModel);
		mLanguageItemModel = new LanguageItemModel(getString(R.string.punjabi), AppConstants.PA);
		languageItemList.add(mLanguageItemModel);
		mLanguageItemModel = new LanguageItemModel(getString(R.string.kannad), AppConstants.KN);
		languageItemList.add(mLanguageItemModel);
		mLanguageItemModel = new LanguageItemModel(getString(R.string.malayalam), AppConstants.ML);
		languageItemList.add(mLanguageItemModel);
		mLanguageItemModel = new LanguageItemModel(getString(R.string.bengali), AppConstants.BN);
		languageItemList.add(mLanguageItemModel);
		mLanguageItemModel = new LanguageItemModel(getString(R.string.tamil), AppConstants.TA);
		languageItemList.add(mLanguageItemModel);
		mLanguageItemModel = new LanguageItemModel(getString(R.string.telugu), AppConstants.TE);
		languageItemList.add(mLanguageItemModel);

		mAdapter = new LanguageSelectionAdapter(this, languageItemList, this);
		mBinding.rvLanguageSelection.setAdapter(mAdapter);

		refreshData();
	}

	private void refreshData() {
		if (languageItemList.size() > 0) {
			mBinding.rvLanguageSelection.setVisibility(View.VISIBLE);
			mBinding.tvNoLanguageFound.setVisibility(View.GONE);
		} else {
			mBinding.rvLanguageSelection.setVisibility(View.GONE);
			mBinding.tvNoLanguageFound.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onItemClicked(LanguageItemModel locale) {
		session.storeDataByKey(SessionManager.KEY_LANGUAGE, locale.getLocal());
		Utils.setLocale(this, session.getDataByKey(SessionManager.KEY_LANGUAGE, locale.getLocal()));
		Intent intent = new Intent(LanguageSelectionActivity.this, (session.isLoggedIn() ? DashBoardActivity.class : LoginActivity.class));
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
	}
}
