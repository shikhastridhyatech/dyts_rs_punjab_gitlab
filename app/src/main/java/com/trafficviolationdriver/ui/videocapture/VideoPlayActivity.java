package com.trafficviolationdriver.ui.videocapture;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.databinding.DataBindingUtil;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.databinding.ActivityVideoPlayBinding;
import com.trafficviolationdriver.model.RejectedEventDetailModel;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.ui.base.BaseBinder;
import com.trafficviolationdriver.util.Logger;

import static com.trafficviolationdriver.ui.message.RejectedEventDetailActivity.KEY_IMAGE_URL;
import static com.trafficviolationdriver.ui.message.RejectedEventDetailActivity.KEY_NUMBER_IMAGE_URL;
import static com.trafficviolationdriver.ui.message.RejectedEventDetailActivity.KEY_VIDEO_URL;
import static com.trafficviolationdriver.util.AppConstants.BASE_DOMAIN;

public class VideoPlayActivity extends BaseActivity implements Player.EventListener {
    private static final String TAG = "VideoPlayActivity";

    public static final String KEY_VIDEO_URI = "video_uri";
    public static final String KEY_IMAGE_URI = "image_uri";
    public static final String KEY_NUMBER_IMAGE_URI = "number_plate_uri";
    public static final String KEY_NUMBER_PLATE ="numberPlate" ;

    RejectedEventDetailModel rejectedEventDetailModel;
    ActivityVideoPlayBinding mBinding;
    String videoUrl;
    SimpleExoPlayer player;
    Handler mHandler;
    Runnable mRunnable;
    Uri videoUri;
    private String imageUrl="",numberPlate="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_video_play);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        handleBundleData(getIntent().getExtras());
    }

    private void handleBundleData(Bundle bundle) {
        if (bundle != null) {
            if (bundle.containsKey(KEY_VIDEO_URI)) {
                videoUri = Uri.parse(bundle.getString(KEY_VIDEO_URI));
                setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.violation_video));
                Logger.e(TAG, videoUri.getPath());
                mBinding.spinnerVideoDetails.setVisibility(videoUri != null ? View.GONE : View.VISIBLE);
                setUp();
            }
            if (bundle.containsKey(KEY_IMAGE_URI)) {
                imageUrl = bundle.getString(KEY_IMAGE_URI);
                setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.violation_image));
                mBinding.imgNumberPlate.setVisibility(imageUrl != null ? View.VISIBLE : View.GONE);
                BaseBinder.setImageUri(mBinding.imgNumberPlate, Uri.parse(imageUrl));
                mBinding.spinnerVideoDetails.setVisibility(imageUrl != null ? View.GONE : View.VISIBLE);
            }


            if (bundle.containsKey(KEY_NUMBER_IMAGE_URI)) {
                imageUrl = bundle.getString(KEY_NUMBER_IMAGE_URI);
                numberPlate=bundle.getString(KEY_NUMBER_PLATE);
                setupToolBarWithBackArrow(mBinding.toolbar.toolbar, numberPlate);
                mBinding.imgNumberPlate.setVisibility(imageUrl != null ? View.VISIBLE : View.GONE);
                BaseBinder.setImageUri(mBinding.imgNumberPlate, Uri.parse(imageUrl));
                mBinding.spinnerVideoDetails.setVisibility(imageUrl != null ? View.GONE : View.VISIBLE);
            }


            if (bundle.containsKey(KEY_VIDEO_URL)) {
                videoUrl = bundle.getString(KEY_VIDEO_URL);
                setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.violation_video));
                Logger.e(TAG, videoUrl);
                mBinding.spinnerVideoDetails.setVisibility(videoUrl != null ? View.GONE : View.VISIBLE);
                setUp();
            }


            if (bundle.containsKey(KEY_IMAGE_URL)) {
                imageUrl = bundle.getString(KEY_IMAGE_URL);
                setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.violation_image));
                mBinding.imgNumberPlate.setVisibility(imageUrl != null ? View.VISIBLE : View.GONE);
                BaseBinder.setImageUrl(mBinding.imgNumberPlate, imageUrl);
                mBinding.spinnerVideoDetails.setVisibility(imageUrl != null ? View.GONE : View.VISIBLE);
            }


            if (bundle.containsKey(KEY_NUMBER_IMAGE_URL)) {
                imageUrl = bundle.getString(KEY_NUMBER_IMAGE_URL);
                numberPlate=bundle.getString(KEY_NUMBER_PLATE);
                setupToolBarWithBackArrow(mBinding.toolbar.toolbar, numberPlate);
                mBinding.imgNumberPlate.setVisibility(imageUrl != null ? View.VISIBLE : View.GONE);
                BaseBinder.setImageUrl(mBinding.imgNumberPlate, imageUrl);
                mBinding.spinnerVideoDetails.setVisibility(imageUrl != null ? View.GONE : View.VISIBLE);
            }


        } else {
            showShortToast(getResources().getString(R.string.something_went_wrong));
            finish();
        }
    }


    private void setUp() {
        initializePlayer();
        if (videoUri != null) {
            buildMediaSource(videoUri);
        }
        if (videoUrl != null) {
            getMediaSource(BASE_DOMAIN + videoUrl);
        }
    }


    private void initializePlayer() {
        // 1. Create a default TrackSelector
        if (player == null) {
            player = ExoPlayerFactory.newSimpleInstance(this);
            mBinding.videoFullScreenPlayer.setPlayer(player);
        }
    }

    //TODO Pass Url of video
    private MediaSource getMediaSource(String videoUrl) {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getApplicationContext(), Util.getUserAgent(getApplicationContext(), getString(R.string.app_name)), bandwidthMeter);
        MediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(videoUrl));
        player.prepare(mediaSource);
        player.setPlayWhenReady(true);
        player.addListener(this);
        return mediaSource;
    }

    private void buildMediaSource(Uri mUri) {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getApplicationContext(), Util.getUserAgent(this, getString(R.string.app_name)), bandwidthMeter);
//        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(mUri);
        ExtractorMediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(mUri, null, null);
        player.prepare(mediaSource);
        player.setPlayWhenReady(true);
        player.addListener(this);
    }

    private void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
        }
    }

    private void pausePlayer() {
        if (player != null) {
            player.setPlayWhenReady(false);
            player.getPlaybackState();
        }
    }

    private void resumePlayer() {
        if (player != null) {
            player.setPlayWhenReady(true);
            player.getPlaybackState();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        pausePlayer();
        if (mRunnable != null) {
            mHandler.removeCallbacks(mRunnable);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        resumePlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {

            case Player.STATE_BUFFERING:
                mBinding.spinnerVideoDetails.setVisibility(View.VISIBLE);
                break;
            case Player.STATE_ENDED:
                // Activate the force enable

                break;
            case Player.STATE_IDLE:

                break;
            case Player.STATE_READY:
                mBinding.spinnerVideoDetails.setVisibility(View.GONE);

                break;
            default:
                // status = PlaybackStatus.IDLE;
                break;
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }
}