package com.trafficviolationdriver.ui.videocapture;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.work.WorkManager;

import com.trafficviolationdriver.R;
import com.trafficviolationdriver.adapter.RecordedVideoListAdapter;
import com.trafficviolationdriver.databinding.ActivityVideoListingBinding;
import com.trafficviolationdriver.db.AppDatabase;
import com.trafficviolationdriver.db.dao.VideoDao;
import com.trafficviolationdriver.db.entity.VideoTableModel;
import com.trafficviolationdriver.dialogs.MessageDialog;
import com.trafficviolationdriver.interfaces.OnRecyclerViewItemClicked;
import com.trafficviolationdriver.interfaces.OnVideoEncryptionDecryptionListener;
import com.trafficviolationdriver.ui.analysis.VideoAnalysisActivity;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.util.Logger;
import com.trafficviolationdriver.util.SecureVideoUtil;
import com.trafficviolationdriver.util.SessionManager;
import com.trafficviolationdriver.util.TimeStamp;
import com.trafficviolationdriver.util.Utils;
import com.trafficviolationdriver.viewmodel.VideoDataViewModel;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by viraj.patel on 13-Apr-19
 */
public class VideoListingActivity extends BaseActivity implements OnRecyclerViewItemClicked<VideoTableModel> {

    ActivityVideoListingBinding mBinding;
    RecordedVideoListAdapter mAdapter;
    ArrayList<VideoTableModel> videoList = new ArrayList<>();
    private VideoDao videoDao;
    VideoDataViewModel mVideoDataViewModel;
    private static final int ANALYSIS_RESULT_CODE = 9;
    private boolean isEventCreated;
    AppDatabase appDatabase;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_video_listing);
        initView();
        AppDatabase appDatabase = AppDatabase.getAppDatabase(this);
        if (appDatabase != null) {
            videoDao = appDatabase.videoDao();
        }
        mVideoDataViewModel = ViewModelProviders.of(this).get(VideoDataViewModel.class);
        checkAndUpdateData();
        GetVideosFromDb();
    }

    private void checkAndUpdateData() {

        long storedLastUpdateValue = session.getLongDataByKey(SessionManager.KEY_LAST_CONFIG_UPDATE, 0);
        long currentTime = System.currentTimeMillis() / 1000;

        if ((storedLastUpdateValue + 86400) < currentTime) {
            // Clean Entries from Database
            AppDatabase appDatabase = AppDatabase.getAppDatabase(this);
            appDatabase.videoDao().removeAllVideos();
            appDatabase.eventDao().removeAllEventsExceptLast();
            appDatabase.configDao().resetRemainingMinutes(session.getUserDetail().getUserId());

            // Delete all stored Videos and Images
            Utils.cleanDirectoryData(this);

            // Update time in Session Manager
            session.storeDataByKey(SessionManager.KEY_LAST_CONFIG_UPDATE, (storedLastUpdateValue + 86400));

            String workerUuid = session.getDataByKey(SessionManager.KEY_WORKER_UUID_UPLOAD_EVENT);
            if (!workerUuid.isEmpty()) {
                WorkManager.getInstance().cancelWorkById(UUID.fromString(workerUuid));
            }
        }
    }

    private void GetVideosFromDb() {
        // Update the list when the data changes
        mVideoDataViewModel.getVideoList().observe(this, videoList -> {
            if (videoList != null) {
                this.videoList = new ArrayList<>(videoList);
                setRecordedVideoAdapter();
            }
            mBinding.executePendingBindings();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        GetVideosFromDb();
//        Utils.cleanTempDirectoryData(this);
    }

    private void initView() {
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.recorded_videos));
    }

    private void setRecordedVideoAdapter() {
        if (mAdapter == null) {
            mAdapter = new RecordedVideoListAdapter(this, videoList, this);
            mBinding.rvVideos.setAdapter(mAdapter);
        } else {
            mAdapter.updateMessageList(videoList);
        }
        mBinding.tvNoRecordFound.setVisibility(videoList.size() > 0 ? View.GONE : View.VISIBLE);
        mBinding.rvVideos.setVisibility(videoList.size() > 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == ANALYSIS_RESULT_CODE) {
            GetVideosFromDb();
            isEventCreated = true;
        }
    }

    @Override
    public void onItemClicked(VideoTableModel mVideo) {
        if (mVideo != null) {
            appDatabase = AppDatabase.getAppDatabase(VideoListingActivity.this);

            //   if (!new File(mVideo.getEncryptedVideoPath()).exists()) {
            File f = new File(mVideo.getEncryptedVideoPath());
            if (!f.getAbsoluteFile().exists()) {

                //     if (mVideo.getEncryptedVideoPath() == null || mVideo.getEncryptedVideoPath().isEmpty()) {

                try {
                    if (appDatabase.configDao() != null && appDatabase.configDao().getConfigData() != null) {

                        VideoTableModel mVideoModel = appDatabase.videoDao().getVideoData(mVideo.getId());
                        long challanCaptureTime = mVideoModel.getChallanCaptureTime();
                        long totalDuration = mVideoModel.getDuration();
                        long remainingTime = totalDuration - challanCaptureTime;

                        if (remainingTime > 0) {
                            appDatabase.configDao().updateRemainingQuota(session.getUserDetail().getUserId(), appDatabase.configDao().getConfigData().getRemainingMinutes() + remainingTime);
                            Logger.e("VideoCaptureService", "now recordingQuotaMillis in minutes ==> " + TimeStamp.getMinutesFromMilliseconds(appDatabase.configDao().getConfigData().getRemainingMinutes()));
                        }
                    }
                    Utils.deleteVideo(videoDao.getVideoData(mVideo.getId()).getVideoPath());
                    Utils.deleteVideo(videoDao.getVideoData(mVideo.getId()).getVideoThumbnail());
                    videoDao.removeVideo(mVideo.getId());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                new MessageDialog(this).setMessage(getString(R.string.error_video_deleted)).cancelable(false).setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                    dialog.dismiss();
                }).show();
            } else {

                File mOutputFile = new File(Utils.getTempVideoFolder(this), mVideo.getVideoName().concat(".mp4"));
                SecureVideoUtil.getInstance().doDecryption(this, mVideo.getEncryptedVideoPath(), mOutputFile.getAbsoluteFile(), new OnVideoEncryptionDecryptionListener() {
                    @Override
                    public void onVideoOperationSucceed() {

                        VideoTableModel videoTableModel = appDatabase.videoDao().getVideoData(mVideo.getId());
                        videoTableModel.setDecryptedVideoPath(mOutputFile.getAbsolutePath());
                        videoTableModel.setDecryptedPercentage(100);
                        appDatabase.videoDao().updateVideoData(videoTableModel);

                        Intent intent = new Intent(VideoListingActivity.this, VideoAnalysisActivity.class);
                        intent.putExtra(VideoAnalysisActivity.EXTRA_DATA_VIDEO_ID, mVideo.getId());
                        intent.putExtra("From", "Listing");
                        startActivityForResult(intent, ANALYSIS_RESULT_CODE);
                    }

                    @Override
                    public void onProgressUpdate(int progress) {
                        appDatabase.videoDao().updateVideoDecryptionProgress(mVideo.getId(), progress);
                    }

                    @Override
                    public void onVideoOperationFailed(String message) {
                        showLongToast(message);
                    }
                });
            }
        }
    }

  /*  @Override
    public void onBackPressed() {
        Intent mIntent = new Intent();
        setResult(isEventCreated ? RESULT_OK : RESULT_CANCELED, mIntent);
        super.onBackPressed();
    }*/
}