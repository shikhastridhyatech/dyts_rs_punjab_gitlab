package com.trafficviolationdriver.ui.videocapture;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.trafficviolationdriver.R;
import com.trafficviolationdriver.databinding.ActivityVideoCaptureBinding;
import com.trafficviolationdriver.db.AppDatabase;
import com.trafficviolationdriver.dialogs.MessageDialog;
import com.trafficviolationdriver.dialogs.VideoAnalysisDialog;
import com.trafficviolationdriver.interfaces.OnDialogClickListener;
import com.trafficviolationdriver.service.VideoCaptureService;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.util.AppConstants;
import com.trafficviolationdriver.util.PermissionUtils;
import com.trafficviolationdriver.util.TimeStamp;

public class VideoCaptureActivity extends BaseActivity implements SurfaceHolder.Callback, OnDialogClickListener<String> {

    public static final String RECORDING_START_STOP = "RECORDING_START_STOP";
    public ActivityVideoCaptureBinding binding;
    VideoCaptureService videoCaptureServiceObj = null;
    public SurfaceView surfaceView;
    public static SurfaceHolder mHolder;
    private boolean isVideoServicesBounded;
    private Context me;
    private VideoAnalysisDialog numberDialog;

    //A reference to the service used to get location updates.
    // private LocationUpdatesService mLocationService = null;
    //Tracks the bound state of the service.
    //  private boolean mLocationBound = false;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private boolean recordingStarted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_video_capture);
        me = this;

        initializeSurfaceAndReceiver();
        numberDialog = new VideoAnalysisDialog(this, this);
        initView();
        binding.ibStartStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleStartStop(!binding.ibStartStop.isSelected());
            }
        });

        try {
            AppDatabase appDatabase = AppDatabase.getAppDatabase(this);
            assert appDatabase != null;
            if (appDatabase.configDao() != null && appDatabase.configDao().getConfigData() != null) {

                new MessageDialog(VideoCaptureActivity.this)
                        .setMessage(getString(R.string.you_have) + " " + TimeStamp.getMinutesFromMilliseconds(appDatabase.configDao().getConfigData().getRemainingMinutes()) + " " + getString(R.string.minutes_left_in_your_daily_recording_quota))
                        .cancelable(true)
                        .setPositiveButton(getString(R.string.ok)
                                , (dialog, which) -> {
                                    dialog.dismiss();
                                }).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initView() {
        binding.toolbar.toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.transparent));
        setupToolBarWithBackArrow(binding.toolbar.toolbar, "");
    }
    /*@Override
    protected void onStart() {
        super.onStart();
        initializeSurfaceAndReceiver();
    }
*/
    private void initializeSurfaceAndReceiver() {
        Log.e("VideoCaptureActivity", "initializeSurfaceAndReceiver");

        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter(RECORDING_START_STOP));

        surfaceView = findViewById(R.id.surface1);
        mHolder = surfaceView.getHolder();
        mHolder.addCallback(VideoCaptureActivity.this);
        binding.surface1.setClickable(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        PermissionUtils.isCheck(this, new String[]{PermissionUtils.CAMERA, PermissionUtils.READ_EXTERNAL, PermissionUtils.WRITE_EXTERNAL, PermissionUtils.RECORD_AUDIO, PermissionUtils.LOCATION, PermissionUtils.ACCESS_LOCATION}, PermissionUtils.CAMERA_CODE, permissionListener);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.hasExtra("ServiceState")) {
                handleStartStop(intent.getBooleanExtra("ServiceState", false));
            } else if (intent != null && intent.hasExtra("UpdateTimer")) {
                binding.tvTimer.setText(intent.getStringExtra("UpdateTimer"));
            } else if (intent != null && intent.hasExtra("FinishActivity") && intent.getBooleanExtra("FinishActivity", false)) {
                finish();
            }
            /*else if (intent != null && intent.hasExtra("RecordNext") && intent.getBooleanExtra("RecordNext", false)) {
                binding.ibStartStop.setSelected(!binding.ibStartStop.isSelected());
            }*/
        }
    };

    private void handleStartStop(boolean isChecked) {
        Log.e("VideoCaptureActivity", "handleStartStop called");

        if (isChecked) {
            if (videoCaptureServiceObj != null) {
                if (isVideoServicesBounded) {
                    videoCaptureServiceObj.startRecording();
                    //mLocationService.requestLocationUpdates();
                    recordingStarted = true;
                    binding.ibStartStop.setSelected(isChecked);
                }
            } else {
                startAndBindService();
            }
        } else {
            try {
                handleStopEvent();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startAndBindService() {
        try {
            Log.e("VideoCaptureActivity", "startAndBindService called");

            Intent startIntent = new Intent(me, VideoCaptureService.class);
            startIntent.setAction(AppConstants.ACTION.START_ACTION);
            startService(startIntent);

            if (videoCaptureServiceObj == null) {
                Intent intentBind = new Intent(me, VideoCaptureService.class);
                bindService(intentBind, mVideoServiceConnection, Context.BIND_AUTO_CREATE);
            }
            //  bindService(new Intent(this, LocationUpdatesService.class), mLocationServiceConnection, Context.BIND_AUTO_CREATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ServiceConnection mVideoServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            videoCaptureServiceObj = ((VideoCaptureService.MyBinder) service).getService();
            isVideoServicesBounded = true;
        }

        public void onServiceDisconnected(ComponentName className) {
            videoCaptureServiceObj = null;
            isVideoServicesBounded = false;
        }
    };

   /* // Monitors the state of the connection to the service.
    private final ServiceConnection mLocationServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationUpdatesService.LocalBinder binder = (LocationUpdatesService.LocalBinder) service;
            mLocationService = binder.getService();
            mLocationBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mLocationService = null;
            mLocationBound = false;
        }
    };*/

    PermissionUtils.permissionListener permissionListener = new PermissionUtils.permissionListener() {
        @Override
        public void onAllow(int requestCode) {

        }

        @Override
        public void onDeny(int requestCode) {
            showPermissionAllowDialog();
        }

        @Override
        public void onDenyNeverAskAgain(int requestCode) {
            showPermissionAllowDialog();
        }
    };

    private void showPermissionAllowDialog() {
        new MessageDialog(me).setMessage(getString(R.string.you_should_allow_this_permission_in_order_to_run_the_app)).cancelable(false).setPositiveButton(getString(R.string.yes), (dialog, which) -> {
            dialog.dismiss();
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setData(Uri.parse("package:" + getPackageName()));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            startActivity(intent);
        }).setNegativeButton(getString(R.string.no), (dialog, which) -> {
            dialog.dismiss();
        }).show();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.e("VideoCaptureActivity", "surfaceCreated called");
        if (!recordingStarted) startAndBindService();
        else {
            if (videoCaptureServiceObj == null) {
                Intent intentBind = new Intent(me, VideoCaptureService.class);
                bindService(intentBind, mVideoServiceConnection, Context.BIND_AUTO_CREATE);
            }
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.e("VideoCaptureActivity", "surfaceChanged called");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.e("VideoCaptureActivity", "surfaceDestroyed called");
    }

    private void handleStopEvent() {
        Log.e("VideoCaptureActivity", "handleStopEvent called");
        if (recordingStarted) {
        /*    new MessageDialog(me)
                    .setMessage(getString(R.string.do_you_want_to_stop))
                    .cancelable(false)
                    .setPositiveButton(getString(R.string.yes),
                            (dialog, which) -> {
                                dialog.dismiss();
                                //   mLocationService.removeLocationUpdates();
                                *//*if (videoCaptureServiceObj != null) {
                                    videoCaptureServiceObj.saveToDBAndStopRecording();
                                }*//*
                                unBindAndStopService(true);
                                super.onBackPressed();
                            })
                    .setNegativeButton(getString(R.string.no),
                            (dialog, which) -> {
                                dialog.dismiss();
                            }
                    ).show();
*/
            numberDialog
                    .setMessage(getString(R.string.recording_complete_alert))
                    .cancelable(true)
                    .setActionClickListener(this)
                    .show();
        } else {
            recordingStarted = false;
            unBindAndStopService(false, false);
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionUtils.onRequestPermissionsResult(this, requestCode, permissions, grantResults, permissionListener);
    }

    @Override
    public void onBackPressed() {
        handleStopEvent();
    }

    private void unBindAndStopService(boolean ifShowSuccessDialog, boolean ifNavigatetoAnotherScreen) {
        try {
            Log.e("VideoCaptureActivity", "unBindAndStopService called");
            if (videoCaptureServiceObj != null) {
                if (recordingStarted) {
                    videoCaptureServiceObj.saveToDBAndStopRecording(ifShowSuccessDialog, ifNavigatetoAnotherScreen);
                } else {
                    videoCaptureServiceObj.releaseCameraResource();
                    videoCaptureServiceObj.stopService();
                }
                if (mVideoServiceConnection != null) {
                    if (isVideoServicesBounded) {
                        unbindService(mVideoServiceConnection);
                        isVideoServicesBounded = false;
                    }
                }
                recordingStarted = false;
                /*if (mLocationBound) {
                    // Unbind from the service. This signals to the service that this activity is no longer
                    // in the foreground, and the service can respond by promoting itself to a foreground
                    // service.
                    unbindService(mLocationServiceConnection);
                    mLocationBound = false;
                }*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onStop() {
        Log.e("VideoCaptureActivity", "OnStop called");
        KeyguardManager myKM = (KeyguardManager) me.getSystemService(Context.KEYGUARD_SERVICE);
        if (myKM.inKeyguardRestrictedInputMode()) {
            //it is locked
        } else {
            //it is not locked

            boolean isLocked;

            PowerManager powerManager = (PowerManager) me.getSystemService(Context.POWER_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
                isLocked = !powerManager.isInteractive();
            } else {
                //noinspection deprecation
                isLocked = !powerManager.isScreenOn();
            }

            if (isLocked) {

            } else {
                unBindAndStopService(false, false);
                finish();
            }
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        Log.e("VideoCaptureActivity", "OnDestroy called");

        if (mVideoServiceConnection != null) {
            if (isVideoServicesBounded) {
                unbindService(mVideoServiceConnection);
                isVideoServicesBounded = false;
            }
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        /*if (mLocationBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mLocationServiceConnection);
            mLocationBound = false;
        }*/
        super.onDestroy();
    }


    @Override
    public void onItemClicked(String redirectTo) {
        switch (redirectTo) {
            case AppConstants.RECORD_NEXT:

                if (videoCaptureServiceObj != null) {
                    if (recordingStarted) {
                        videoCaptureServiceObj.recordNextVideo();
                    }
                }
                break;

            case AppConstants.GO_TO_ANALYSIS:
                unBindAndStopService(false, true);
                break;

            case AppConstants.GO_TO_HOME:
                unBindAndStopService(true, false);
                finish();
                break;
        }
    }
}