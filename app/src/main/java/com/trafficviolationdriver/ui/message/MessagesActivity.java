package com.trafficviolationdriver.ui.message;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.adapter.MessageAdapter;
import com.trafficviolationdriver.databinding.ActivityCommonRecyclerviewBinding;
import com.trafficviolationdriver.dialogs.MessageDialog;
import com.trafficviolationdriver.interfaces.OnRecyclerViewItemClicked;
import com.trafficviolationdriver.model.MessageModel;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.util.Logger;
import com.trafficviolationdriver.webservice.APIs;
import com.trafficviolationdriver.webservice.JSONCallback;
import com.trafficviolationdriver.webservice.Retrofit;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by viraj.patel on 13-Apr-19
 */
public class MessagesActivity extends BaseActivity implements OnRecyclerViewItemClicked<MessageModel> {

	ActivityCommonRecyclerviewBinding mBinding;

	MessageAdapter mAdapter;
	ArrayList<MessageModel> messageList = new ArrayList<>();
	private int pageNumber = 0;
	private boolean isLoading;
	private LinearLayoutManager mLayoutManager;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mBinding = DataBindingUtil.setContentView(this, R.layout.activity_common_recyclerview);
		initView();
		API_GetMessages();
	}

	private void initView() {
		setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.messages));
		mLayoutManager = (LinearLayoutManager) mBinding.rvWithdrawRequestList.getLayoutManager();
		mBinding.tvNoRecordFound.setText(getResources().getString(R.string.no_message_found));
		mBinding.rvWithdrawRequestList.addOnScrollListener(new RecyclerView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
				super.onScrollStateChanged(recyclerView, newState);
			}

			@Override
			public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
				super.onScrolled(recyclerView, dx, dy);

				int visibleItemCount = mLayoutManager.getChildCount();
				int totalItemCount = mLayoutManager.getItemCount();
				int lastVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

				if (!isLoading && (totalItemCount <= (lastVisibleItem + visibleItemCount)) && (pageNumber != -1)) {
					API_GetMessages();
					isLoading = true;
				}
			}
		});
	}

	private void setMessageAdapter() {
		if (mAdapter == null) {
			mAdapter = new MessageAdapter(this, messageList, this);
			mBinding.rvWithdrawRequestList.setAdapter(mAdapter);
		} else {
			mAdapter.updateMessageList(messageList);
		}
		mBinding.tvNoRecordFound.setVisibility(messageList.size() > 0 ? View.GONE : View.VISIBLE);
		mBinding.rvWithdrawRequestList.setVisibility(messageList.size() > 0 ? View.VISIBLE : View.GONE);
	}

	@Override
	public void onItemClicked(MessageModel mMessage) {
		if (mMessage != null) {
			startActivity(new Intent(this, MessageDetailActivity.class).putExtra(MessageDetailActivity.BUNDLE_DATA_MESSAGE, mMessage.getId()));
		}
	}

	private void API_GetMessages() {
		HashMap<String, String> params = new HashMap<>();
		params.put("pageNumber", String.valueOf(pageNumber));
		params.put("userId", String.valueOf(session.getUserDetail().getUserId()));
		try {
			Retrofit.with(MessagesActivity.this).
					setGetParameters(params).
					setAPI(APIs.API_GET_MESSAGES).
					setCallBackListener(new JSONCallback(MessagesActivity.this, showProgressBar()) {
						@Override
						protected void onSuccess(int statusCode, JSONObject jsonObject) {
							hideProgressBar();
							if (jsonObject.optJSONObject("data") != null) {
								try {
									NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
									notificationManager.cancelAll();
									Type modelType = new TypeToken<List<MessageModel>>() {
									}.getType();
									ArrayList<MessageModel> newMessageList = new Gson().fromJson(jsonObject.optJSONObject("data").getJSONArray("message").toString(), modelType);
									messageList.addAll(newMessageList);
									pageNumber = jsonObject.optJSONObject("data").optInt("nexPage");
								} catch (Exception e) {
									e.printStackTrace();
									isLoading = false;
								}
							} else {
								pageNumber = -1;
							}
							setMessageAdapter();
							isLoading = false;
						}

						@Override
						protected void onFailed(int statusCode, String message) {
							Logger.e(message);
							hideProgressBar();
							isLoading = false;

						}
					});
		} catch (Exception e) {
			e.printStackTrace();
			hideProgressBar();
			isLoading = false;
			new MessageDialog(MessagesActivity.this).setMessage(e.getMessage()).setPositiveButton(getString(R.string.retry), (dialog, which) -> {
				dialog.dismiss();
				API_GetMessages();
			}).setNegativeButton(getString(R.string.cancel), (dialog1, which) -> dialog1.dismiss()).show();
		}
	}
}
