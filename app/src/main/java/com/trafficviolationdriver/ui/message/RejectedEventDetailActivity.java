package com.trafficviolationdriver.ui.message;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.databinding.ActivityRejectedEventDetailBinding;
import com.trafficviolationdriver.dialogs.MessageDialog;
import com.trafficviolationdriver.model.RejectedEventDetailModel;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.ui.videocapture.VideoPlayActivity;
import com.trafficviolationdriver.util.Logger;
import com.trafficviolationdriver.webservice.APIs;
import com.trafficviolationdriver.webservice.JSONCallback;
import com.trafficviolationdriver.webservice.Retrofit;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

import static com.trafficviolationdriver.ui.videocapture.VideoPlayActivity.KEY_NUMBER_PLATE;

/**
 * Created by viraj.patel on 13-Apr-19
 */
public class RejectedEventDetailActivity extends BaseActivity implements View.OnClickListener {

    public static String BUNDLE_DATA_REJECTED_EVENT_ID = "rejected_event";
    public static String BUNDLE_DATA_REJECTED_EVENT_VEHICLE_NUMBER = "vehicle_number";
    ActivityRejectedEventDetailBinding mBinding;
    RejectedEventDetailModel mRejectedEventDetailModel;
    public static final String KEY_VIDEO_URL = "video_url";
    public static final String KEY_IMAGE_URL = "image_url";
    public static final String KEY_NUMBER_IMAGE_URL = "number_plate_url";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_rejected_event_detail);
        setClickEvents();
        handleBundleData(getIntent().getExtras());
    }

    private void handleBundleData(Bundle bundle) {

        if (bundle != null) {
            String eventId = bundle.getString(BUNDLE_DATA_REJECTED_EVENT_ID);
            if (eventId != null && eventId.length() > 0) {
                API_GetRejectedEvens(eventId);
            } else {
                showShortToast(getResources().getString(R.string.something_went_wrong));
                finish();
            }
            if (bundle.containsKey(BUNDLE_DATA_REJECTED_EVENT_VEHICLE_NUMBER)) {
                String vehicleNumber = bundle.getString(BUNDLE_DATA_REJECTED_EVENT_VEHICLE_NUMBER);
                setupToolBarWithBackArrow(mBinding.toolbar.toolbar, vehicleNumber);
            }
        } else {
            showShortToast(getResources().getString(R.string.something_went_wrong));
            finish();
        }
    }

    private void setClickEvents() {
        mBinding.cvVideo.setOnClickListener(this);
        mBinding.cvNumberPlate.setOnClickListener(this);
        mBinding.cvImage.setOnClickListener(this);
    }

    private void API_GetRejectedEvens(String id) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", String.valueOf(session.getUserDetail().getUserId()));
        params.put("eventId", id);
        try {
            Retrofit.with(RejectedEventDetailActivity.this).
                    setGetParameters(params).
                    setAPI(APIs.API_GET_REJECTED_DETAIL).
                    setCallBackListener(new JSONCallback(RejectedEventDetailActivity.this, showProgressBar()) {
                        @Override
                        protected void onSuccess(int statusCode, JSONObject jsonObject) {
                            hideProgressBar();

                            if (jsonObject.optJSONObject("data") != null) {
                                try {
                                    Type modelType = new TypeToken<RejectedEventDetailModel>() {
                                    }.getType();
                                    mRejectedEventDetailModel = new Gson().fromJson(jsonObject.optJSONObject("data").toString(), modelType);
                                    RejectedEventDetailModel mRejectedEventDetailModel = new Gson().fromJson(jsonObject.optJSONObject("data").toString(), modelType);
                                    setupToolBarWithBackArrow(mBinding.toolbar.toolbar, mRejectedEventDetailModel.getVehicalNumber());
                                    mBinding.setEventData(mRejectedEventDetailModel);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        protected void onFailed(int statusCode, String message) {
                            Logger.e(message);
                            hideProgressBar();
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
            hideProgressBar();
            new MessageDialog(RejectedEventDetailActivity.this).setMessage(e.getMessage()).setPositiveButton(getString(R.string.retry), (dialog, which) -> {
                dialog.dismiss();
                API_GetRejectedEvens(id);
            }).setNegativeButton(getString(R.string.cancel), (dialog1, which) -> dialog1.dismiss()).show();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cv_video:
                Intent intent = new Intent(this, VideoPlayActivity.class);
                intent.putExtra(KEY_VIDEO_URL, mRejectedEventDetailModel.getViolationVideo());
                startActivity(intent);
                break;
            case R.id.cv_number_plate:
                intent = new Intent(this, VideoPlayActivity.class);
                intent.putExtra(KEY_NUMBER_IMAGE_URL, mRejectedEventDetailModel.getViolationNumberPlate());
                intent.putExtra(KEY_NUMBER_PLATE, mRejectedEventDetailModel.getVehicalNumber());
                startActivity(intent);
                break;
            case R.id.cv_image:
                intent = new Intent(this, VideoPlayActivity.class);
                intent.putExtra(KEY_IMAGE_URL, mRejectedEventDetailModel.getViolationImage());
                startActivity(intent);
                break;
        }
    }

}
