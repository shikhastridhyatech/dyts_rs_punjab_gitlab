package com.trafficviolationdriver.ui.message;

import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.databinding.ActivityMessageDetailBinding;
import com.trafficviolationdriver.dialogs.MessageDialog;
import com.trafficviolationdriver.model.MessageDetailModel;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.util.Logger;
import com.trafficviolationdriver.util.TimeStamp;
import com.trafficviolationdriver.webservice.APIs;
import com.trafficviolationdriver.webservice.JSONCallback;
import com.trafficviolationdriver.webservice.Retrofit;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

/**
 * Created by viraj.patel on 13-Apr-19
 */
public class MessageDetailActivity extends BaseActivity {

    public static String BUNDLE_DATA_MESSAGE = "message";
    ActivityMessageDetailBinding mBinding;
    MessageDetailModel mMessageModel;
    String id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_message_detail);
        handleBundleData(getIntent().getExtras());
    }

    private void handleBundleData(Bundle bundle) {

        if (bundle != null) {
            id = bundle.getString(BUNDLE_DATA_MESSAGE);
            API_GetMessageDetail(id);

        } else {
            showShortToast(getResources().getString(R.string.something_went_wrong));
            finish();
        }
    }

    private void API_GetMessageDetail(String id) {
        HashMap<String, String> params = new HashMap<>();
        params.put("pageNumber", String.valueOf(1));
        params.put("userId", String.valueOf(session.getUserDetail().getUserId()));
        params.put("messageId", id);

        try {
            Retrofit.with(MessageDetailActivity.this).
                    setGetParameters(params).
                    setAPI(APIs.API_GET_MESSAGE_DETAIL).
                    setCallBackListener(new JSONCallback(MessageDetailActivity.this, showProgressBar()) {
                        @Override
                        protected void onSuccess(int statusCode, JSONObject jsonObject) {

                            hideProgressBar();
                            if (jsonObject.optJSONObject("data") != null) {
                                try {
                                    Type modelType = new TypeToken<MessageDetailModel>() {
                                    }.getType();
                                    mMessageModel = new Gson().fromJson(jsonObject.optJSONObject("data").toString(), modelType);
                                    Logger.e(mMessageModel.toString());
                                    setupToolBarWithBackArrow(mBinding.toolbar.toolbar, TimeStamp.getGTSDateFromTimestamp(mMessageModel.getDateTime()));
                                    mBinding.tvMessage.setText(mMessageModel.getMessage());

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        protected void onFailed(int statusCode, String message) {
                            Logger.e(message);
                            hideProgressBar();
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
            hideProgressBar();
            new MessageDialog(MessageDetailActivity.this).setMessage(e.getMessage()).setPositiveButton(getString(R.string.retry), (dialog, which) -> {
                dialog.dismiss();
                API_GetMessageDetail(id);
            }).setNegativeButton(getString(R.string.cancel), (dialog1, which) -> dialog1.dismiss()).show();
        }
    }


}
