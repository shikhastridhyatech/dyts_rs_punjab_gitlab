package com.trafficviolationdriver.ui.message;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.databinding.ActivityMessageDetailBinding;
import com.trafficviolationdriver.databinding.ActivityMessageDisplayBinding;
import com.trafficviolationdriver.dialogs.MessageDialog;
import com.trafficviolationdriver.model.MessageDetailModel;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.util.Logger;
import com.trafficviolationdriver.util.TimeStamp;
import com.trafficviolationdriver.webservice.APIs;
import com.trafficviolationdriver.webservice.JSONCallback;
import com.trafficviolationdriver.webservice.Retrofit;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

import static com.trafficviolationdriver.firebase.MyFirebaseMessagingService.NOTIFICATION_ID;

/**
 * Created by viraj.patel on 13-Apr-19
 */
public class DisplayMessageActivity extends BaseActivity implements View.OnClickListener {

	public static String BUNDLE_DATA_MESSAGE = "message";
	ActivityMessageDisplayBinding mBinding;
	String message;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mBinding = DataBindingUtil.setContentView(this, R.layout.activity_message_display);
		handleBundleData(getIntent().getExtras());
		mBinding.ivMenu.setOnClickListener(this);

	}

	private void handleBundleData(Bundle bundle) {
		if (bundle != null) {
			message = bundle.getString(BUNDLE_DATA_MESSAGE);
			mBinding.tvMessage.setText(message);
		} else {
			showShortToast(getResources().getString(R.string.something_went_wrong));
			finish();
		}
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.ivMenu:
				NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
				notificationManager.cancel(NOTIFICATION_ID);
				finish();
		}
	}
}
