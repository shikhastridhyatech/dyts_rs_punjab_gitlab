package com.trafficviolationdriver.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.trafficviolationdriver.R;
import com.trafficviolationdriver.databinding.ActivityForgotPasswordBinding;
import com.trafficviolationdriver.dialogs.MessageDialog;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.util.ValidationUtils;
import com.trafficviolationdriver.webservice.APIs;
import com.trafficviolationdriver.webservice.JSONCallback;
import com.trafficviolationdriver.webservice.ResponseUtils;
import com.trafficviolationdriver.webservice.Retrofit;

import org.json.JSONObject;

import java.util.HashMap;

import androidx.databinding.DataBindingUtil;

public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener {

    ActivityForgotPasswordBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(ForgotPasswordActivity.this, R.layout.activity_forgot_password);
        setClickEvents();
    }

    private void setClickEvents() {
        mBinding.tvSignIn.setOnClickListener(this);
        mBinding.btnResetPassword.setOnClickListener(this);
        mBinding.btnRequestToAdmin.setOnClickListener(this);
    }

    private boolean validate() {
        boolean value = true;
        try {
            if (mBinding.etEmail.getText().toString().isEmpty()) {
                showSnackBar(mBinding.etEmail, getString(R.string.please_enter_email_address));
                mBinding.etEmail.requestFocus();
                value = false;
            } else if (!ValidationUtils.isValidEmail(mBinding.etEmail.getText().toString())) {
                showSnackBar(mBinding.etEmail, getString(R.string.please_enter_valid_email_address));
                mBinding.etEmail.requestFocus();
                value = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_sign_in:
                finish();
                break;
            case R.id.btn_request_to_admin:
                if (validate())
                    API_forgotPassword(mBinding.etEmail.getText().toString(), 1);
                break;
            case R.id.btn_reset_password:
                if (validate())
                    API_forgotPassword(mBinding.etEmail.getText().toString(), 2);
                break;
        }
    }

    private void API_forgotPassword(String email, int requestType) {
        HashMap<String, String> params = new HashMap<>();
        params.put("email", email);
        try {
            Retrofit.with(this)
                    .setParameters(params)
                    .setAPI(ResponseUtils.getRequestAPIURL(requestType == 1 ? APIs.API_REQUEST_RESET_PASSWORD : APIs.API_FORGOT_PASSWORD, session))
                    .setCallBackListener(new JSONCallback(this, showProgressBar()) {
                        @Override
                        protected void onSuccess(int statusCode, JSONObject jsonObject) {
                            hideProgressBar();
                            if (jsonObject.optString("status_code") != null) {
                                try {
                                    showShortToast(jsonObject.optString("message"));
                                    Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            } else {
                                showShortToast(jsonObject.optString("message"));
                            }
                        }

                        @Override
                        protected void onFailed(int statusCode, String message) {
                            hideProgressBar();
                            showShortToast(message);
                        }
                    });
        } catch (Exception e) {
            hideProgressBar();
            new MessageDialog(this).setMessage(e.getMessage()).setPositiveButton(getString(R.string.retry), (dialog, which) -> {
                dialog.dismiss();
                API_forgotPassword(email, (requestType == 1 ? 1 : 2));
            }).setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.dismiss()).show();
        }
    }
}
