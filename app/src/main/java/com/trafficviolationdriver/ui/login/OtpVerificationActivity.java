package com.trafficviolationdriver.ui.login;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.databinding.ActivityOtpVerificationBinding;
import com.trafficviolationdriver.db.AppDatabase;
import com.trafficviolationdriver.db.dao.ConfigDao;
import com.trafficviolationdriver.db.entity.ConfigTableModel;
import com.trafficviolationdriver.dialogs.MessageDialog;
import com.trafficviolationdriver.model.LoginDataModel;
import com.trafficviolationdriver.model.UserDataModel;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.ui.home.DashBoardActivity;
import com.trafficviolationdriver.util.Logger;
import com.trafficviolationdriver.util.Utils;
import com.trafficviolationdriver.webservice.APIs;
import com.trafficviolationdriver.webservice.JSONCallback;
import com.trafficviolationdriver.webservice.Retrofit;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.trafficviolationdriver.util.AppConstants.IMEI;


public class OtpVerificationActivity extends BaseActivity implements View.OnClickListener, OnSmsCatchListener<String> {

    private static String TAG = OtpVerificationActivity.class.getSimpleName();
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    ActivityOtpVerificationBinding mBinding;
    String phoneNumber;
    SmsVerifyCatcher smsVerifyCatcher;
    LoginDataModel loginDataModel;
    String IMEI_Number_Holder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_otp_verification);
        smsVerifyCatcher = new SmsVerifyCatcher(this, this);
        handleBundleData(getIntent().getExtras());
        initView();
    }

    public String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{4}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }

    private void initView() {
        mBinding.toolbar.toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.transparent));
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "");
    }

    private void handleBundleData(Bundle bundle) {
        if (bundle != null) {
            phoneNumber = bundle.getString("phoneNumber");
            IMEI_Number_Holder = bundle.getString(IMEI);
            loginDataModel = bundle.getParcelable(LoginDataModel.class.getSimpleName());
            setClickEvents();
            if (!(hasAppPermissions(new String[]{Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS}))) {
                getSMSPermission();

            } else {
                if (getOTP().isEmpty()) {
                    setClickEvents();
                }
            }

        } else {
            showLongToast(getResources().getString(R.string.something_went_wrong));
            finish();
        }

        mBinding.etFour.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mBinding.etFour.getText().toString().length() == 1) {
                    Utils.hideSoftKeyboard(OtpVerificationActivity.this, mBinding.etFour);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public void onSmsCatch(String message) {
        Log.d("onSmsCatch", message);
        String code = parseCode(message);//Parse verification code
        Log.d("onSmsCatchCode", code);
        mBinding.etOne.setText(String.valueOf(code.charAt(0)));
        mBinding.etTwo.setText(String.valueOf(code.charAt(1)));
        mBinding.etThree.setText(String.valueOf(code.charAt(2)));
        mBinding.etFour.setText(String.valueOf(code.charAt(3)));
    }


    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }
    /*
     */

    /**
     * need for Android 6 real time permissions
     *//*
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }*/
    private void API_verifyOtp(String phoneNumber, String deviceId) {
        if (!getOTP().isEmpty()) {
            HashMap<String, String> params = new HashMap<>();
            params.put("phoneNumber", phoneNumber);
            params.put("otp", getOTP());
            params.put("IMEI", IMEI_Number_Holder);
            params.put("deviceToken", deviceId);
            params.put("deviceType", "A");

            try {
                Retrofit.with(this).setParameters(params).setAPI(APIs.API_VERIFY_OTP).setCallBackListener(new JSONCallback(this, showProgressBar()) {
                    @Override
                    protected void onSuccess(int statusCode, JSONObject jsonObject) {
                        hideProgressBar();
                        try {
                            Gson gson = new Gson();
                            final UserDataModel userInfo = gson.fromJson(jsonObject.optJSONObject("data").optJSONObject("user").toString(), new TypeToken<UserDataModel>() {
                            }.getType());
                            showShortToast(jsonObject.optString("message"));
                            session.storeUserDetail(userInfo);

                            ConfigDao mConfigDao = AppDatabase.getAppDatabase(OtpVerificationActivity.this).configDao();
                            ConfigTableModel mConfigModel = mConfigDao.getConfigData();
                            if (mConfigModel != null && mConfigModel.getRemainingMinutes() > 0) {
                                // Update config data in database
                                mConfigDao.updateConfigData(session.getUserDetail().getUserId(), userInfo.getTargetEventCount(), (userInfo.getRecordingQuotaMinutes() * 60 * 1000), mConfigModel.getRemainingMinutes(), userInfo.getDataDeleteTime());
                            } else {
                                // Clear Table
                                mConfigDao.deleteAll();
                                // Insert config data in database
                                mConfigDao.insertConfigData(new ConfigTableModel(session.getUserDetail().getUserId(), userInfo.getTargetEventCount(), (userInfo.getRecordingQuotaMinutes() * 60 * 1000), (userInfo.getRecordingQuotaMinutes() * 60 * 1000), userInfo.getDataDeleteTime()));
                            }

                            Intent intent = new Intent(OtpVerificationActivity.this, DashBoardActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    protected void onFailed(int statusCode, String message) {
                        hideProgressBar();
                        showLongToast(message);
                    }
                });
            } catch (Exception e) {
                hideProgressBar();
                new MessageDialog(this).setMessage(e.getMessage()).setPositiveButton(getString(R.string.retry), (dialog, which) -> {
                    dialog.dismiss();
                    API_verifyOtp(phoneNumber, deviceId);
                }).setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.dismiss()).show();
            }
        } else {
            showSnackBar(mBinding.etFour, getString(R.string.please_enter_otp), Snackbar.LENGTH_LONG);
        }
    }

    private String getOTP() {
        return mBinding.etOne.getText().toString() + mBinding.etTwo.getText().toString() + mBinding.etThree.getText().toString() + Objects.requireNonNull(mBinding.etFour.getText()).toString();
    }

    private void getSMSPermission() {
        requestAppPermissions(new String[]{Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS}, REQUEST_ID_MULTIPLE_PERMISSIONS, new BaseActivity.setPermissionListener() {
            @Override
            public void onPermissionGranted(int requestCode) {
                smsVerifyCatcher.onStart();
            }

            @Override
            public void onPermissionDenied(int requestCode) {

            }
        });
    }

    private void setClickEvents() {
        mBinding.etOne.addTextChangedListener(new GenericTextWatcher(mBinding.etOne));
        mBinding.etTwo.addTextChangedListener(new GenericTextWatcher(mBinding.etTwo));
        mBinding.etThree.addTextChangedListener(new GenericTextWatcher(mBinding.etThree));
        mBinding.etFour.addTextChangedListener(new GenericTextWatcher(mBinding.etFour));

        mBinding.etOne.setOnKeyListener(onKeyListener);
        mBinding.etTwo.setOnKeyListener(onKeyListener);
        mBinding.etThree.setOnKeyListener(onKeyListener);
        mBinding.etFour.setOnKeyListener(onKeyListener);

        mBinding.btnVerifiy.setOnClickListener(this);
        mBinding.txtResend.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_verifiy:
                validation();
                break;
            case R.id.txt_resend:
                Intent intent = new Intent(OtpVerificationActivity.this, LoginActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void validation() {
        try {
            if (mBinding.etOne.getText().toString().length() == 0) {
                showSnackBar(mBinding.etFour, getString(R.string.please_enter_otp), Snackbar.LENGTH_LONG);
            } else if (mBinding.etTwo.getText().toString().length() == 0) {
                showSnackBar(mBinding.etFour, getString(R.string.please_enter_otp), Snackbar.LENGTH_LONG);
            } else if (mBinding.etThree.getText().toString().length() == 0) {
                showSnackBar(mBinding.etFour, getString(R.string.please_enter_otp), Snackbar.LENGTH_LONG);
            } else if (mBinding.etFour.getText().toString().length() == 0) {
                showSnackBar(mBinding.etFour, getString(R.string.please_enter_otp), Snackbar.LENGTH_LONG);
            } else {

                // Get token
                FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
                    if (!task.isSuccessful() || task.getResult() == null) {
                        Log.w(TAG, "getInstanceId failed", task.getException());
                        API_verifyOtp(phoneNumber, "");
                        return;
                    }

                    // Get new Instance ID token
                    String token = task.getResult().getToken();

                    API_verifyOtp(phoneNumber, token);

                    // Log and toast
                    Logger.e(TAG, token);
                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {
                case R.id.et_one:
                    if (text.length() == 1) mBinding.etTwo.requestFocus();
                    break;
                case R.id.et_two:
                    if (text.length() == 1) mBinding.etThree.requestFocus();
                    break;
                case R.id.et_three:
                    if (text.length() == 1) mBinding.etFour.requestFocus();
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }
    }

    View.OnKeyListener onKeyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View view, int i, KeyEvent keyEvent) {
            if (i == KeyEvent.KEYCODE_DEL && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                switch (view.getId()) {
                    case R.id.et_one:
                        break;
                    case R.id.et_two:
                        mBinding.etOne.requestFocus();
                        break;
                    case R.id.et_three:
                        mBinding.etTwo.requestFocus();
                        break;
                    case R.id.et_four:
                        mBinding.etThree.requestFocus();
                        break;
                }
            }
            return false;
        }
    };
}

