package com.trafficviolationdriver.ui.login;

import android.os.Bundle;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.databinding.ActivityChangePasswordBinding;
import com.trafficviolationdriver.dialogs.MessageDialog;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.webservice.APIs;
import com.trafficviolationdriver.webservice.JSONCallback;
import com.trafficviolationdriver.webservice.ResponseUtils;
import com.trafficviolationdriver.webservice.Retrofit;

import org.json.JSONObject;

import java.util.HashMap;

import androidx.databinding.DataBindingUtil;

public class ChangePasswordActivity extends BaseActivity implements View.OnClickListener {

    ActivityChangePasswordBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        initView();
    }

    private void initView() {
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.change_password));
        mBinding.tvUpdatePassword.setOnClickListener(this);
    }

    public boolean validate() {
        boolean value = true;
        if (mBinding.etOldPassword.getText().toString().trim().isEmpty()) {
            showSnackBar(mBinding.etOldPassword, getResources().getString(R.string.please_enter_old_password), Snackbar.LENGTH_LONG);
            mBinding.etOldPassword.requestFocus();
            value = false;
        } else if (mBinding.etNewPassword.getText().toString().trim().isEmpty()) {
            showSnackBar(mBinding.etNewPassword, getResources().getString(R.string.please_enter_new_password), Snackbar.LENGTH_LONG);
            mBinding.etNewPassword.requestFocus();
            value = false;
        } else if (mBinding.etConfirmPassword.getText().toString().trim().isEmpty()) {
            showSnackBar(mBinding.etConfirmPassword, getString(R.string.please_reenter_password));
            mBinding.etConfirmPassword.requestFocus();
            value = false;
        } else if (!mBinding.etNewPassword.getText().toString().trim().equals(mBinding.etConfirmPassword.getText().toString().trim())) {
            showSnackBar(mBinding.etConfirmPassword, getString(R.string.password_mismatch));
            mBinding.etNewPassword.requestFocus();
            value = false;
        }
        return value;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_update_password:
                if (validate())
                    API_changePassword();
                break;
        }
    }

    private void API_changePassword() {
        try {
            HashMap<String, String> params = new HashMap<>();
            params.put("userId", String.valueOf(session.getUserDetail().getUserId()));
            params.put("oldPassword", mBinding.etOldPassword.getText().toString().trim());
            params.put("newPassword", mBinding.etNewPassword.getText().toString().trim());

            try {
                Retrofit.with(this).setParameters(params).setAPI(APIs.API_CHANGE_PASSWORD).setCallBackListener(new JSONCallback(this, showProgressBar()) {
                    @Override
                    protected void onSuccess(int statusCode, JSONObject jsonObject) {
                        hideProgressBar();
                        if (jsonObject.optString("message") != null) {
                            showShortToast(jsonObject.optString("message"));
                            finish();
                        } else {
                            showShortToast(getResources().getString(R.string.something_went_wrong));
                        }
                    }

                    @Override
                    protected void onFailed(int statusCode, String message) {
                        hideProgressBar();
                        showShortToast(message);
                    }
                });
            } catch (Exception e) {
                hideProgressBar();
                new MessageDialog(this).setMessage(e.getMessage()).setPositiveButton(getString(R.string.retry), (dialog, which) -> {
                    dialog.dismiss();
                    API_changePassword();
                }).setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.dismiss()).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
