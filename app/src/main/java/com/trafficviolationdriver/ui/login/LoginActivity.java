package com.trafficviolationdriver.ui.login;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.databinding.ActivityLoginBinding;
import com.trafficviolationdriver.dialogs.MessageDialog;
import com.trafficviolationdriver.model.LoginDataModel;
import com.trafficviolationdriver.model.UserDataModel;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.util.Logger;
import com.trafficviolationdriver.util.Utils;
import com.trafficviolationdriver.webservice.APIs;
import com.trafficviolationdriver.webservice.JSONCallback;
import com.trafficviolationdriver.webservice.Retrofit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.trafficviolationdriver.util.AppConstants.API_KEY;
import static com.trafficviolationdriver.util.AppConstants.IMEI;

public class LoginActivity extends BaseActivity {

    private static String TAG = LoginActivity.class.getSimpleName();
    private final static int REQUEST_PERMISSIONS_LOCATION = 12;

    ActivityLoginBinding mBinding;
    UserDataModel lastUserModel;
    TelephonyManager telephonyManager;
    String IMEI_Number_Holder;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location mLastKnownLocation;
    FusedLocationProviderClient mFusedLocationProviderClient;
    int retryCount = 0, imeiRetryCount = 0;
    String cityName = "";
    private LocationRequest locationRequest;

    private String[] permissions = new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    private LocationCallback locationCallback;

    @SuppressLint({"ClickableViewAccessibility", "MissingPermission", "HardwareIds"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(LoginActivity.this, R.layout.activity_login);
        lastUserModel = new UserDataModel();
        telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(LoginActivity.this);

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

		/*locationCallback = new LocationCallback() {
			@Override
			public void onLocationResult(LocationResult locationResult) {
				if (locationResult == null) {
					return;
				}
				for (Location location : locationResult.getLocations()) {
					if (location != null) {
						mFusedLocationProviderClient.removeLocationUpdates(locationCallback);
						Logger.e(String.format(Locale.US, "%s -- %s", location.getLatitude(), location.getLongitude()));
						API_GetCity(false, location);

					} else if (retryCount < 3) {
						mFusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
						retryCount++;
					} else {
						Logger.e(getResources().getString(R.string.not_able_to_get_location_please_try_again));
					}
				}
			}

		};*/

        if (!(hasAppPermissions(permissions))) {
            if (!(hasAppPermissions(new String[]{Manifest.permission.READ_PHONE_STATE})) && !(hasAppPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}))) {
                getAllPermission();
            } else if (!(hasAppPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}))) {
                getSMSPermission();
            } else if (!(hasAppPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}))) {
                getLocationPermission();
            }

        } else {
            if (checkGpsUnabled()) {
//				getCurrentLocation(false);
                getCustomCurrentLocation(false);

            } else {
                buildAlertMessageNoGps();
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                IMEI_Number_Holder = telephonyManager.getImei();
                Logger.d(TAG, "IMEI_Number==>" + IMEI_Number_Holder);

            } else {
                IMEI_Number_Holder = telephonyManager.getDeviceId();
                Logger.d(TAG, "IMEI_Number==>" + IMEI_Number_Holder);
            }
        }

        mBinding.etPhoneNumber.setSelection(mBinding.etPhoneNumber.getText().length());

        mBinding.btnSignIn.setOnClickListener(view -> {
            if (SystemClock.elapsedRealtime() - lastClickTime < 2000) {
                return;
            }
            lastClickTime = SystemClock.elapsedRealtime();
            validate();
        });

        mBinding.etPhoneNumber.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mBinding.etPhoneNumber.getText().toString().length() == 10) {
                    Utils.hideSoftKeyboard(LoginActivity.this, mBinding.etPhoneNumber);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void afterTextChanged(Editable s) {
            }
        });
    }

    @SuppressLint({"MissingPermission", "HardwareIds"})
    private void getIMEI_Number(boolean isCallValidation) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            IMEI_Number_Holder = telephonyManager.getImei();
            Logger.d(TAG, "IMEI_Number==>" + IMEI_Number_Holder);

        } else {
            IMEI_Number_Holder = telephonyManager.getDeviceId();
            Logger.d(TAG, "IMEI_Number==>" + IMEI_Number_Holder);
        }
        if (IMEI_Number_Holder.isEmpty() && imeiRetryCount < 3) {
            getIMEI_Number(isCallValidation);
            imeiRetryCount++;
        } else {
            if (isCallValidation) {
                validate();

            }
            Logger.d(TAG, "IMEI_Number Not Found");
        }
    }

    private boolean checkGpsUnabled() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.your_gps_permission_is_disabled))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();
            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void buildAlertMessageHighAlert() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.high_accuracy_gps_alert))             //todo : string pending in bhojpuri and oriya
                .setCancelable(false)
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();
            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void getAllPermission() {
        requestAppPermissions(permissions, REQUEST_ID_MULTIPLE_PERMISSIONS, new BaseActivity.setPermissionListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onPermissionGranted(int requestCode) {

                if (hasAppPermissions(new String[]{Manifest.permission.READ_PHONE_STATE})) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        IMEI_Number_Holder = telephonyManager.getImei();
                        Logger.d(TAG, "IMEI_Number==>" + IMEI_Number_Holder);
                    } else {
                        IMEI_Number_Holder = telephonyManager.getDeviceId();
                        Logger.d(TAG, "IMEI_Number==>" + IMEI_Number_Holder);
                    }
                } else if (hasAppPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})) {
//					getCurrentLocation(false);
                    getCustomCurrentLocation(false);
                } else {
                    getLocationPermission();
                }
            }

            @Override
            public void onPermissionDenied(int requestCode) {
                showSnackBar(mBinding.btnSignIn, getString(R.string.please_grant_permission), Snackbar.LENGTH_INDEFINITE, getString(R.string.settings), () -> {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    startActivity(intent);
                });
            }
        });
    }

    private void getSMSPermission() {
        requestAppPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_ID_MULTIPLE_PERMISSIONS, new BaseActivity.setPermissionListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onPermissionGranted(int requestCode) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    IMEI_Number_Holder = telephonyManager.getImei();
                    Logger.d(TAG, "IMEI_Number==>" + IMEI_Number_Holder);

                } else {
                    IMEI_Number_Holder = telephonyManager.getDeviceId();
                    Logger.d(TAG, "IMEI_Number==>" + IMEI_Number_Holder);
                }
            }

            @Override
            public void onPermissionDenied(int requestCode) {

                showSnackBar(mBinding.btnSignIn, getString(R.string.please_grant_permission), Snackbar.LENGTH_INDEFINITE, getString(R.string.settings), () -> {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    startActivity(intent);
                });
            }
        });
    }

    private void getLocationPermission() {
        requestAppPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_PERMISSIONS_LOCATION, new BaseActivity.setPermissionListener() {
            @Override
            public void onPermissionGranted(int requestCode) {
//				getCurrentLocation(false);
                getCustomCurrentLocation(false);
            }

            @Override
            public void onPermissionDenied(int requestCode) {
                showSnackBar(toolbar, getString(R.string.please_grant_permission), Snackbar.LENGTH_INDEFINITE, getString(R.string.settings), () -> {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    startActivity(intent);
                });
            }
        });
    }

    //Getting current location
	/*private void getCurrentLocation(boolean isCallAPI) {

		try {
			Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();

			locationResult.addOnCompleteListener(this, task -> {
				if (task.isSuccessful() && task.getResult() != null) {

					// Set the map's camera position to the current location of the device.
					mLastKnownLocation = task.getResult();

					if (mLastKnownLocation == null) {

						if (retryCount < 3) {
							getCurrentLocation(isCallAPI);
							retryCount++;
						} else {
							Logger.e(getResources().getString(R.string.not_able_to_get_location_please_try_again));
							new MessageDialog(this).setMessage(getString(R.string.not_able_to_get_location_please_try_again)).setPositiveButton(getString(R.string.retry), (dialog12, which) -> {
								dialog12.dismiss();
								mFusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
							}).setNegativeButton(getString(R.string.cancel), (dialog1, which) -> dialog1.dismiss()).show();
						}
					} else {
						Logger.e(getResources().getString(R.string.location_fetched, mLastKnownLocation.toString()));
						API_GetCity(isCallAPI, mLastKnownLocation);
					}
				} else {
					Logger.d(TAG, "Current location is null. Using defaults.");
					Logger.e(TAG, "Exception:Current location is null " + task.getException());
//					showShortToast(getString(R.string.not_able_to_get_location_please_try_again));
					new MessageDialog(this).setMessage(getString(R.string.not_able_to_get_location_please_try_again)).setPositiveButton(getString(R.string.retry), (dialog12, which) -> {
						dialog12.dismiss();
						mFusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
					}).setNegativeButton(getString(R.string.cancel), (dialog1, which) -> dialog1.dismiss()).show();
				}
			});
		} catch (SecurityException e) {
			Logger.e("Exception getCurrentLocation: %s", e.getMessage());
			new MessageDialog(this).setMessage(getString(R.string.not_able_to_get_location_please_try_again)).setPositiveButton(getString(R.string.retry), (dialog12, which) -> {
				dialog12.dismiss();
				getCurrentLocation(isCallAPI);
			}).setNegativeButton(getString(R.string.cancel), (dialog1, which) -> dialog1.dismiss()).show();
		}
	}    //Getting current location*/

    @SuppressLint("MissingPermission")
    private void getCustomCurrentLocation(boolean isCallAPI) {

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        mFusedLocationProviderClient.removeLocationUpdates(locationCallback);
//						Logger.e(String.format(Locale.US, "%s -- %s", location.getLatitude(), location.getLongitude()));
                        API_GetCity(isCallAPI, location);
                    } else if (retryCount < 3) {
                        getCustomCurrentLocation(isCallAPI);
                        retryCount++;
                    } else {
                        Logger.e(getResources().getString(R.string.not_able_to_get_location_please_try_again));
                        new MessageDialog(LoginActivity.this).setMessage(getString(R.string.not_able_to_get_location_please_try_again)).setPositiveButton(getString(R.string.retry), (dialog12, which) -> {
                            dialog12.dismiss();
                            getCustomCurrentLocation(isCallAPI);
                        }).setNegativeButton(getString(R.string.cancel), (dialog1, which) -> dialog1.dismiss()).show();
                    }
                }
            }

        };
        mFusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);

    }

    private void validate() {
        try {
            if (!checkGpsUnabled()) {
                buildAlertMessageNoGps();
            } else if (!checkGpsAccuracy()) {
                buildAlertMessageHighAlert();
            } else if (IMEI_Number_Holder == null && IMEI_Number_Holder.isEmpty()) {
                getIMEI_Number(true);
            } else if (mBinding.etPhoneNumber.getText().toString().isEmpty()) {
                showSnackBar(mBinding.etPhoneNumber, getString(R.string.please_enter_phone_number));
                mBinding.etPhoneNumber.requestFocus();
            }/* else if (!ValidationUtils.isValidPhoneNumber(mBinding.etPhoneNumber.getText().toString())) {
                showSnackBar(mBinding.etPhoneNumber, getString(R.string.please_enter_valid_phone_number));
                mBinding.etPhoneNumber.requestFocus();
            }*/ else if (cityName.equals("") && cityName.isEmpty()) {
                showProgressBar().show();
                if (checkGpsUnabled()) {
//					getCurrentLocation(true);
                    getCustomCurrentLocation(true);
                } else {
                    buildAlertMessageNoGps();
                }
            } else {
                API_signIn(mBinding.etPhoneNumber.getText().toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkGpsAccuracy() {
        try {
            if (Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE) == 3) {
                return true;
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    private void API_GetCity(boolean isCallAPI, Location mLastKnownLocation) {
        HashMap<String, String> params = new HashMap<>();
        params.put("latlng", mLastKnownLocation.getLatitude() + "," + mLastKnownLocation.getLongitude());
        params.put("sensor", String.valueOf(false));
        params.put("key", API_KEY);

        try {
            Retrofit.with(this).setUrl(APIs.LOCATION_URL).setCustomGetParameters(params).setCustomCallBackListener(new JSONCallback(this) {
                @Override
                protected void onSuccess(int statusCode, JSONObject jsonObject) {
                    try {
                        String Status = jsonObject.getString("status");
                        if (Status.equalsIgnoreCase("OK")) {
                            JSONArray Results = jsonObject.getJSONArray("results");
                            JSONObject zero = Results.getJSONObject(0);
                            JSONArray address_components = zero.getJSONArray("address_components");
                            for (int i = 0; i < address_components.length(); i++) {
                                JSONObject zero2 = address_components.getJSONObject(i);
                                String long_name = zero2.getString("long_name");
                                JSONArray mtypes = zero2.getJSONArray("types");
                                String Type = mtypes.getString(0);
                                if (Type.equalsIgnoreCase("locality")) {
                                    // Address2 = Address2 + long_name + ", ";
                                    cityName = long_name;
                                    Log.e(" CityName --->", cityName + "");

                                    if (cityName != null && !cityName.isEmpty() && isCallAPI) {
                                        Log.e(" CityName --->", cityName + "");
                                        API_signIn(mBinding.etPhoneNumber.getText().toString());

                                    } else if (cityName != null && !cityName.isEmpty()) {
                                        Log.e(" CityName --->", cityName + "");
                                    } else {
                                        Log.e("Getting Error CityName", cityName + "");
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
                }

                @Override
                protected void onFailed(int statusCode, String message) {
                    showLongToast(message);
                }
            });
        } catch (Exception e) {
            new MessageDialog(LoginActivity.this).setMessage(e.getMessage()).setPositiveButton(getString(R.string.retry), (dialog, which) -> {
                dialog.dismiss();
                API_GetCity(isCallAPI, mLastKnownLocation);
            }).setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.dismiss()).show();
        }
    }

    private void API_signIn(String phoneNumber) {
        HashMap<String, String> params = new HashMap<>();
        params.put("phoneNumber", phoneNumber);
        params.put("cityName", cityName.toLowerCase());
        params.put(IMEI, IMEI_Number_Holder);

        try {
            Retrofit.with(this).setParameters(params).setAPI(APIs.API_LOGIN).setCallBackListener(new JSONCallback(this, showProgressBar()) {
                @SuppressLint("StringFormatMatches")
                @Override
                protected void onSuccess(int statusCode, JSONObject jsonObject) {
                    hideProgressBar();
                    try {
                        Gson gson = new Gson();
                        final LoginDataModel loginInfo = gson.fromJson(jsonObject.optJSONObject("data").toString(), new TypeToken<LoginDataModel>() {
                        }.getType());
                        //if (loginInfo.getCity().toLowerCase().equals(cityName.toLowerCase())) {
                        Logger.e("Is User of Current Company:", "True");

                        if (loginInfo.isIsActive() && loginInfo.isIsIMEIVerified()) {
                            showShortToast(jsonObject.optString("message"));
                            Intent intent = new Intent(LoginActivity.this, OtpVerificationActivity.class);
                            intent.putExtra("phoneNumber", phoneNumber);
                            intent.putExtra(IMEI, IMEI_Number_Holder);
                            startActivity(intent);
                        } else if (!loginInfo.isIsActive()) {
                            new MessageDialog(LoginActivity.this).setMessage(getString(R.string.alert_login_from_inActive)).cancelable(false).setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                                dialog.dismiss();
                                mBinding.etPhoneNumber.getText().clear();
                            }).show();
                        } else if (!loginInfo.isIsIMEIVerified()) {
                            new MessageDialog(LoginActivity.this).setMessage(getString(R.string.alert_login_from_other_device)).cancelable(false).setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                                dialog.dismiss();
                                mBinding.etPhoneNumber.getText().clear();
                            }).show();
                        }

						/*} else {
							Logger.e("Is User of Current Company:", "False");
							new MessageDialog(LoginActivity.this).setMessage(getResources().getString(R.string.error_wrong_city, cityName, loginInfo.getCity())).cancelable(true).setPositiveButton(getString(R.string.ok), (dialog, which) -> {
								dialog.dismiss();
							}).show();
						}*/

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                protected void onFailed(int statusCode, String message) {
                    hideProgressBar();
                    showLongToast(message);
                }
            });
        } catch (Exception e) {
            hideProgressBar();
            new MessageDialog(this).setMessage(e.getMessage()).setPositiveButton(getString(R.string.retry), (dialog, which) -> {
                dialog.dismiss();
                API_signIn(phoneNumber);
            }).setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.dismiss()).show();
        }
    }
}

