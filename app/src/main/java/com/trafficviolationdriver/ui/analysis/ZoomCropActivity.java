package com.trafficviolationdriver.ui.analysis;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.databinding.ActivityZoomCropBinding;
import com.trafficviolationdriver.dialogs.VehicleNumberDialog;
import com.trafficviolationdriver.interfaces.OnBottomSheetClickListener;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.util.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.trafficviolationdriver.ui.analysis.VideoAnalysisActivity.EXTRA_DATA_CAPTURED_IMAGE;
import static com.trafficviolationdriver.ui.analysis.VideoAnalysisActivity.EXTRA_DATA_CAPTURED_VIDEO;
import static com.trafficviolationdriver.ui.analysis.VideoAnalysisActivity.EXTRA_DATA_CAPTURED_ZOOM_IMAGE;
import static com.trafficviolationdriver.ui.analysis.VideoAnalysisActivity.EXTRA_DATA_CHALLAN_CAPTURE_TIMESTAMP;
import static com.trafficviolationdriver.ui.analysis.VideoAnalysisActivity.EXTRA_DATA_EVENT_ID;
import static com.trafficviolationdriver.ui.analysis.VideoAnalysisActivity.EXTRA_DATA_NUMBER_PLATE;
import static com.trafficviolationdriver.ui.analysis.VideoAnalysisActivity.EXTRA_DATA_VIDEO_ID;

public class ZoomCropActivity extends BaseActivity implements View.OnClickListener {
    ActivityZoomCropBinding mBinding;
    private String cropImage = "", cropVideo = "", zoomImage = "", numberPlate = "";
    private long challanCaptureTime = 0;
    private int videoId = 0, eventId = 0;
    private boolean isEventCreated;
    List<String> months;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(ZoomCropActivity.this, R.layout.activity_zoom_crop);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.zoom_crop_title));
        mBinding.toolbar.toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.transparent));
        mBinding.toolbar.ivMenu.setOnClickListener(this);

        months = Arrays.asList(getApplicationContext().getResources().getStringArray(R.array.state_list));

        if (getIntent() != null) {
            cropImage = getIntent().getStringExtra(EXTRA_DATA_CAPTURED_IMAGE);
            cropVideo = getIntent().getStringExtra(EXTRA_DATA_CAPTURED_VIDEO);
            eventId = getIntent().getIntExtra(EXTRA_DATA_EVENT_ID, 0);
            videoId = getIntent().getIntExtra(EXTRA_DATA_VIDEO_ID, 0);
            challanCaptureTime = getIntent().getLongExtra(EXTRA_DATA_CHALLAN_CAPTURE_TIMESTAMP, 0);
//            numberPlate = getIntent().getStringExtra(EXTRA_DATA_NUMBER_PLATE);
            mBinding.photoView.setImageURI(Uri.parse(cropImage));
            Log.e("basic image", cropImage);
        }
        mBinding.ivCheck.setVisibility(View.GONE);
        mBinding.imgAddVehicle.setOnClickListener(view -> {
   /*         if (SystemClock.elapsedRealtime() - lastClickTime < 2000) {
                return;
            }*/
            mBinding.progressbar.setVisibility(View.VISIBLE);
            detectImg();
            lastClickTime = SystemClock.elapsedRealtime();
            //addVehicleNumber();
        });
        mBinding.imgCut.setOnClickListener(view -> {

            if (SystemClock.elapsedRealtime() - lastClickTime < 2000) {
                return;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            if (numberPlate.equals("")) {
                showLongToast(getResources().getString(R.string.please_enter_vehicle_number));
            } else if (!TextUtils.isEmpty(cropImage))
                cropImage();
            else
                showLongToast("File Not Found");
        });
    }

    private void detectImg() {
        FirebaseVisionImage image = null;
        try {

            image = FirebaseVisionImage.fromFilePath(this, Uri.fromFile(new File(cropImage)));
            FirebaseVisionTextRecognizer detector = FirebaseVision.getInstance().getOnDeviceTextRecognizer();
            detector.processImage(image).addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                @Override
                public void onSuccess(FirebaseVisionText firebaseVisionText) {
                    if (firebaseVisionText.getText().length() == 0) {
                        mBinding.progressbar.setVisibility(View.GONE);
                        addVehicleNumber();
                    } else {
                        processTxt(firebaseVisionText);
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void processTxt(FirebaseVisionText firebaseVisionText) {

        if (firebaseVisionText.getTextBlocks().size() == 0) {
            mBinding.progressbar.setVisibility(View.GONE);
            addVehicleNumber();
            return;
        }

        for (FirebaseVisionText.TextBlock block : firebaseVisionText.getTextBlocks()) {
            for (FirebaseVisionText.Line line : block.getLines()) {
                for (FirebaseVisionText.Element element : line.getElements()) {
                    numberPlate = element.getText();
                    Log.e("numberplate", numberPlate);
                    try {

                        if (numberPlate.length() == 10) {

                            for (int i = 0; i <= months.size() - 1; i++) {
                                if (numberPlate.substring(0, 2).equals(months.get(i))) {
                                    if (numberPlate.substring(2, 4).contains("O")) {
                                        numberPlate = numberPlate.replace("O", "0");
                                    }
                                    if (TextUtils.isDigitsOnly(numberPlate.substring(2, 4)) && (numberPlate.substring(4, 6)).matches("[a-zA-Z]+") && TextUtils.isDigitsOnly(numberPlate.substring(6, 10))) {
                                        Log.e(" if", "");
                                        mBinding.progressbar.setVisibility(View.GONE);
                                        addVehicleNumber();
                                        return;
                                    } else {
                                        mBinding.progressbar.setVisibility(View.GONE);
                                        addVehicleNumber();

                                    }
                                }
                            }
                        } else if (numberPlate.length() == 9) {

                            for (int i = 0; i <= months.size() - 1; i++) {
                                if (numberPlate.substring(0, 2).equals(months.get(i))) {
                                    if (numberPlate.substring(2, 4).contains("O")) {
                                        numberPlate.replace("O", "0");
                                    }
                                    if (TextUtils.isDigitsOnly(numberPlate.substring(2, 4)) && (numberPlate.substring(4, 5)).matches("[a-zA-Z]+") && TextUtils.isDigitsOnly(numberPlate.substring(6, 10))) {
                                        Log.e(" if", "");
                                        mBinding.progressbar.setVisibility(View.GONE);
                                        addVehicleNumber();
                                        return;
                                    } else {
                                        mBinding.progressbar.setVisibility(View.GONE);
                                        addVehicleNumber();

                                    }
                                }
                            }
                        } else if (numberPlate.length() >= 2) {
                            for (int i = 0; i < months.size() ; i++) {
                                if (numberPlate.substring(2, 4).contains("O")) {
                                    numberPlate.replace("O", "0");
                                }
                                if (numberPlate.substring(0, 2).equals(months.get(i))) {
                                    mBinding.progressbar.setVisibility(View.GONE);
                                    addVehicleNumber();
                                    return;
                                } else {
                                    mBinding.progressbar.setVisibility(View.GONE);
                                    addVehicleNumber();
                                    return;

                                }
                                    /*else {
                                        addVehicleNumber();
                                    }*/

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        //addVehicleNumber();
    }

    private void cropImage() {
        try {
            Bitmap bitmap = Bitmap.createBitmap(mBinding.photoView.getWidth(),
                    mBinding.photoView.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            mBinding.photoView.draw(canvas);
            //File zoomImageFile = Utils.createFileImage(bitmap, "Image_Zoom_");
            File zoomImageFile = null;


            zoomImageFile = Utils.createNewFile(this, false, false, true, eventId);

            FileOutputStream fos = new FileOutputStream(zoomImageFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();

            zoomImage = zoomImageFile.getAbsolutePath();
//        mBinding.photoView.setImageBitmap(bitmap);
            redirectDetail();
        } catch (FileNotFoundException e) {
            Log.d("FileNotFoundException", "File not found: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.d("FileNotFoundException", "Error accessing file: " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        Log.e("onBackPressed", String.valueOf(challanCaptureTime));
        Intent intent = new Intent(this, VideoAnalysisActivity.class);
        intent.putExtra(EXTRA_DATA_CHALLAN_CAPTURE_TIMESTAMP, challanCaptureTime);
        intent.putExtra(EXTRA_DATA_VIDEO_ID, videoId);
        startActivity(intent);
        finish();
    }

    private void addVehicleNumber() {
        VehicleNumberDialog vehicleNumberDialog = new VehicleNumberDialog(this, this);
        vehicleNumberDialog.setMessage(getString(R.string.please_enter_vehicle_number)).cancelable(false).setListener(new OnBottomSheetClickListener<String>() {
            @Override
            public void onItemClicked(String vehicleNumber, Dialog dialog) {
                dialog.dismiss();
                numberPlate = vehicleNumber;
                mBinding.ivCheck.setVisibility(View.VISIBLE);
            }

            @Override
            public void onItemCancel(Dialog dialog) {

            }
        }).show();

        vehicleNumberDialog.setVehicleNumber(numberPlate);
    }

    private void redirectDetail() {
        Intent intent = new Intent(this, CreateEventDetailActivity.class);
        intent.putExtra(EXTRA_DATA_CAPTURED_VIDEO, cropVideo);
        intent.putExtra(EXTRA_DATA_CAPTURED_IMAGE, cropImage);
        intent.putExtra(EXTRA_DATA_EVENT_ID, eventId);
        intent.putExtra(EXTRA_DATA_CHALLAN_CAPTURE_TIMESTAMP, challanCaptureTime);
        intent.putExtra(EXTRA_DATA_VIDEO_ID, videoId);
        intent.putExtra(EXTRA_DATA_CAPTURED_ZOOM_IMAGE, zoomImage);
        intent.putExtra(EXTRA_DATA_NUMBER_PLATE, numberPlate);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivMenu:
                onBackPressed();
                break;
        }
    }
}
