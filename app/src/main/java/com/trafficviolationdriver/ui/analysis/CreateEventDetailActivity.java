package com.trafficviolationdriver.ui.analysis;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.adapter.ViolationReasonsAdapter;
import com.trafficviolationdriver.databinding.ActivityCreateEventDetailBinding;
import com.trafficviolationdriver.db.AppDatabase;
import com.trafficviolationdriver.db.entity.DriverLocationTableModel;
import com.trafficviolationdriver.db.entity.EventTableModel;
import com.trafficviolationdriver.db.entity.VideoTableModel;
import com.trafficviolationdriver.dialogs.MessageDialog;
import com.trafficviolationdriver.interfaces.OnRecyclerViewItemClicked;
import com.trafficviolationdriver.model.ViolationReasonsModel;
import com.trafficviolationdriver.service.EventUploadService;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.ui.videocapture.VideoPlayActivity;
import com.trafficviolationdriver.util.AppConstants;
import com.trafficviolationdriver.util.Logger;
import com.trafficviolationdriver.util.TimeStamp;
import com.trafficviolationdriver.util.Utils;
import com.trafficviolationdriver.webservice.APIs;
import com.trafficviolationdriver.webservice.JSONCallback;
import com.trafficviolationdriver.webservice.Retrofit;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.trafficviolationdriver.ui.analysis.VideoAnalysisActivity.EXTRA_DATA_CAPTURED_IMAGE;
import static com.trafficviolationdriver.ui.analysis.VideoAnalysisActivity.EXTRA_DATA_CAPTURED_VIDEO;
import static com.trafficviolationdriver.ui.analysis.VideoAnalysisActivity.EXTRA_DATA_CAPTURED_ZOOM_IMAGE;
import static com.trafficviolationdriver.ui.analysis.VideoAnalysisActivity.EXTRA_DATA_CHALLAN_CAPTURE_TIMESTAMP;
import static com.trafficviolationdriver.ui.analysis.VideoAnalysisActivity.EXTRA_DATA_NUMBER_PLATE;
import static com.trafficviolationdriver.ui.analysis.VideoAnalysisActivity.EXTRA_DATA_VIDEO_ID;
import static com.trafficviolationdriver.ui.videocapture.VideoPlayActivity.KEY_IMAGE_URI;
import static com.trafficviolationdriver.ui.videocapture.VideoPlayActivity.KEY_NUMBER_IMAGE_URI;
import static com.trafficviolationdriver.ui.videocapture.VideoPlayActivity.KEY_NUMBER_PLATE;
import static com.trafficviolationdriver.ui.videocapture.VideoPlayActivity.KEY_VIDEO_URI;
import static com.trafficviolationdriver.util.TimeStamp.DateFormatGTSUploadEvent;

/**
 * Created by viraj.patel on 13-Apr-19
 */
public class CreateEventDetailActivity extends BaseActivity implements OnRecyclerViewItemClicked<ViolationReasonsModel>, View.OnClickListener {

	ActivityCreateEventDetailBinding mBinding;

	ViolationReasonsAdapter mAdapter;
	ArrayList<ViolationReasonsModel> mViolationReasonList = new ArrayList<>();
	private String cropVideo = "", cropImage = "", zoomImage = "", numberPlate = "";
	private long challanCaptureTime = 0;
	private int videoId = 0;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mBinding = DataBindingUtil.setContentView(this, R.layout.activity_create_event_detail);
		handleBundleData(getIntent().getExtras());
		API_GetViolationReasons();
		initView();
		setClickEvents();
	}

	private void initView() {
		mBinding.pullToRefresh.setOnRefreshListener(() -> {
			API_GetViolationReasons();
			mBinding.pullToRefresh.setRefreshing(false);
		});
	}

	private void setClickEvents() {
		mBinding.cvVideo.setOnClickListener(this);
		mBinding.cvNumberPlate.setOnClickListener(this);
		mBinding.cvImage.setOnClickListener(this);
	}


	private void handleBundleData(Bundle bundle) {
		if (bundle != null) {
			cropVideo = bundle.getString(EXTRA_DATA_CAPTURED_VIDEO);
			cropImage = bundle.getString(EXTRA_DATA_CAPTURED_IMAGE);
			challanCaptureTime = getIntent().getLongExtra(EXTRA_DATA_CHALLAN_CAPTURE_TIMESTAMP, 0);
			videoId = getIntent().getIntExtra(EXTRA_DATA_VIDEO_ID, 0);
			zoomImage = bundle.getString(EXTRA_DATA_CAPTURED_ZOOM_IMAGE);
			numberPlate = bundle.getString(EXTRA_DATA_NUMBER_PLATE);
			mBinding.ivVideo.setImageURI(Uri.parse(cropImage));
			mBinding.ivImage.setImageURI(Uri.parse(cropImage));
			mBinding.ivNumberPlate.setImageURI(Uri.parse(zoomImage));
			setupToolBarWithBackArrow(mBinding.toolbar.toolbar, numberPlate);
		} else {
			showShortToast(getResources().getString(R.string.something_went_wrong));
			finish();
		}
	}

	private void API_GetViolationReasons() {
		HashMap<String, String> params = new HashMap<>();
		params.put("userId", String.valueOf(session.getUserDetail().getUserId()));
		params.put("vehicleNo", numberPlate.replaceAll(" ", "-"));
		params.put("eventDate", TimeStamp.getDateFromTimestamp(TimeStamp.getCurrentUTC(), DateFormatGTSUploadEvent));
		try {
			Retrofit.with(CreateEventDetailActivity.this).setAPI(APIs.API_GET_VIOLATION_REASONS).setParameters(params).setCallBackListener(new JSONCallback(CreateEventDetailActivity.this, showProgressBar()) {
				@Override
				protected void onSuccess(int statusCode, JSONObject jsonObject) {
					hideProgressBar();
					if (jsonObject.optJSONObject("data") != null) {
						try {
							Type modelType = new TypeToken<List<ViolationReasonsModel>>() {
							}.getType();
							mViolationReasonList = new Gson().fromJson(jsonObject.optJSONObject("data").getJSONArray("reasons").toString(), modelType);
							setViolationReasonsAdapter();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}

				@Override
				protected void onFailed(int statusCode, String message) {
					Logger.e(message);
					hideProgressBar();

				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			hideProgressBar();
			new MessageDialog(CreateEventDetailActivity.this).setMessage(e.getMessage()).setPositiveButton(getString(R.string.retry), (dialog, which) -> {
				dialog.dismiss();
				API_GetViolationReasons();
			}).setNegativeButton(getString(R.string.cancel), (dialog1, which) -> dialog1.dismiss()).show();
		}
	}

	private void setViolationReasonsAdapter() {
		if (mAdapter == null) {
			mAdapter = new ViolationReasonsAdapter(this, mViolationReasonList, this);
			mBinding.rvViolationReasonsList.setAdapter(mAdapter);
		} else {
			mAdapter.updateViolationReasonsList(mViolationReasonList);
		}
		mBinding.tvNoRecordFound.setVisibility(mViolationReasonList.size() > 0 ? View.GONE : View.VISIBLE);
		mBinding.rvViolationReasonsList.setVisibility(mViolationReasonList.size() > 0 ? View.VISIBLE : View.GONE);
	}

	@Override
	public void onItemClicked(ViolationReasonsModel mMessage) {
		if (mMessage != null) {
			new MessageDialog(CreateEventDetailActivity.this).setMessage(getString(R.string.permission_create_event)).cancelable(true).setPositiveButton(getString(R.string.yes), (dialog, which) -> {
				dialog.dismiss();

				DriverLocationTableModel mLocationData = getCaptureTimeAndLocation(videoId);
				EventTableModel mEventModel = new EventTableModel(session.getUserDetail().getUserId(), videoId, challanCaptureTime, numberPlate, mMessage.getReasonId(), cropVideo, cropImage, zoomImage, mLocationData.getTimestamp(), mLocationData.getLatitude(), mLocationData.getLongitude());

				int eventId = (int) AppDatabase.getAppDatabase(CreateEventDetailActivity.this).eventDao().insertEventData(mEventModel);
				mEventModel.setId(eventId);
				Logger.d("mEventModel", mEventModel.toString());
				// Call API
//                API_CreateEventDetail(mEventModel);

				new MessageDialog(CreateEventDetailActivity.this)
						.setMessage(getString(R.string.events_will_uploaded_soon))
						.setPositiveButton(getString(R.string.ok)
								, (dialog1, which1) -> {
									dialog1.dismiss();

									AppDatabase appDatabase = AppDatabase.getAppDatabase(CreateEventDetailActivity.this);
									if (appDatabase != null && appDatabase.videoDao().getMyVideoCount(session.getUserDetail().getUserId()) > 0) {
										appDatabase.videoDao().updateTempChallanCaptureTime(videoId, challanCaptureTime);
									}

									if (!Utils.isServiceRunning(CreateEventDetailActivity.this, EventUploadService.class)) {
										//Start Foreground Service
										Intent mIntent = new Intent(CreateEventDetailActivity.this, EventUploadService.class);
										mIntent.setAction(AppConstants.ACTION.START_ACTION);
										mIntent.putExtra(EventUploadService.BUNDLE_DATA_LOCAL_EVENT_MODEL, mEventModel);
										startService(mIntent);
									}

									Intent intent = new Intent(CreateEventDetailActivity.this, VideoAnalysisActivity.class);
									intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
									intent.putExtra(VideoAnalysisActivity.EXTRA_DATA_VIDEO_ID, videoId);
									startActivity(intent);
									finish();
								}).show();

			}).show();
		}
	}

	private DriverLocationTableModel getCaptureTimeAndLocation(int videoId) {
		VideoTableModel mVideoTableModel = AppDatabase.getAppDatabase(this).videoDao().getVideoData(videoId);
		Logger.e("Capture Timestamp", String.valueOf(challanCaptureTime));
		Logger.e("endTimestamp", String.valueOf(mVideoTableModel.getEndTimestamp()));
		Logger.e("startTime", String.valueOf(mVideoTableModel.getEndTimestamp() - mVideoTableModel.getDuration()));

		long eventTimestamp = mVideoTableModel.getEndTimestamp() - mVideoTableModel.getDuration() + challanCaptureTime;
		Logger.e("Event Timestamp", String.valueOf(eventTimestamp));
		DriverLocationTableModel mLocationModel = AppDatabase.getAppDatabase(this).locationDao().getLocationFromTimestamp(videoId, eventTimestamp);
		if (mLocationModel != null) {
			Logger.e("LocationData", mLocationModel.toString());
			mLocationModel.setTimestamp(eventTimestamp);
		} else {
			mLocationModel = new DriverLocationTableModel();
			mLocationModel.setVideoId(videoId);
			mLocationModel.setDriverId(String.valueOf(session.getUserDetail().getUserId()));
			mLocationModel.setTimestamp(eventTimestamp);
		}
		return mLocationModel;
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
			case R.id.cv_video:
				Intent intent = new Intent(this, VideoPlayActivity.class);
				intent.putExtra(KEY_VIDEO_URI, cropVideo);
				startActivity(intent);
				break;
			case R.id.cv_number_plate:
				intent = new Intent(this, VideoPlayActivity.class);
				intent.putExtra(KEY_NUMBER_IMAGE_URI, zoomImage);
				intent.putExtra(KEY_NUMBER_PLATE, numberPlate);
				startActivity(intent);
				break;
			case R.id.cv_image:
				intent = new Intent(this, VideoPlayActivity.class);
				intent.putExtra(KEY_IMAGE_URI, cropImage);
				startActivity(intent);
				break;
		}
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, VideoAnalysisActivity.class);
		intent.putExtra(EXTRA_DATA_CHALLAN_CAPTURE_TIMESTAMP, challanCaptureTime);
		intent.putExtra(EXTRA_DATA_VIDEO_ID, videoId);
		startActivity(intent);
		finish();
//		super.onBackPressed();
	}
}
