package com.trafficviolationdriver.ui.analysis;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.databinding.ActivityVideoAnalysisBinding;
import com.trafficviolationdriver.db.AppDatabase;
import com.trafficviolationdriver.db.dao.VideoDao;
import com.trafficviolationdriver.db.entity.EventTableModel;
import com.trafficviolationdriver.db.entity.VideoTableModel;
import com.trafficviolationdriver.dialogs.MessageDialog;
import com.trafficviolationdriver.interfaces.OnTrimVideoListener;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.ui.base.ProgressDialog;
import com.trafficviolationdriver.util.Logger;
import com.trafficviolationdriver.util.TimeStamp;
import com.trafficviolationdriver.util.Utils;
import com.trafficviolationdriver.util.videotrimmer.TrimVideoUtils;
import com.trafficviolationdriver.videoplay.cropview.window.CropVideoView;
import com.trafficviolationdriver.videoplay.player.VideoPlayer;
import com.vincent.videocompressor.VideoCompress;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.trafficviolationdriver.util.AppConstants.VIDEO_CROP_ONE_SIDE_DURATION;

public class VideoAnalysisActivity extends BaseActivity implements VideoPlayer.OnProgressUpdateListener {
	ActivityVideoAnalysisBinding mBinding;
	private static final int STORAGE_REQUEST = 100;
	private VideoPlayer mVideoPlayer;
	private CropVideoView mCropVideoView;
	private String inputPath = "", outputPath = "", numberPlate = "", compressedVideoPath = "", finalVideoPath = "", from = "";
	private boolean isVideoPlaying = false;
	private FFmpeg ffmpeg;
	public Bitmap extractedImage;

	public static String EXTRA_DATA_CAPTURED_IMAGE = "capture_image";
	public static String EXTRA_DATA_CAPTURED_VIDEO = "capture_video";
	public static String EXTRA_DATA_EVENT_ID = "event_id";
	public static String EXTRA_DATA_CHALLAN_CAPTURE_TIMESTAMP = "challan_timestamp";
	public static String EXTRA_DATA_VIDEO_ID = "video_id";
	public static String EXTRA_DATA_NUMBER_PLATE = "number_plate";
	public static String EXTRA_DATA_CAPTURED_ZOOM_IMAGE = "capture_zoom_image";

	VideoTableModel mVideoTableModel;
	private int nextEventId = 0, videoId;
	private long pauseTime = 0, tempChallanCaptureTime = 0;
	private VideoDao videoDao;
	private boolean isEventCreated, isVideoPlayerInit;
	private ProgressDialog ffmpegProgressDialog;
	AppDatabase appDatabase;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		mBinding = DataBindingUtil.setContentView(this, R.layout.activity_video_analysis);
		initView();
		appDatabase = AppDatabase.getAppDatabase(this);
		if (appDatabase != null) {
			videoDao = appDatabase.videoDao();
		}
		handleBundleData(getIntent().getExtras());
		from = getIntent().hasExtra("From") ? getIntent().getStringExtra("From") : "";

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// ignore orientation/keyboard change
		super.onConfigurationChanged(newConfig);
	}

	private void initView() {
		mBinding.toolbar.toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.transparent));
		setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "");
	}
	private VideoPlayer.VideoPlayFinished videoPlayFinished = new VideoPlayer.VideoPlayFinished() {
		@Override
		public void onFinished() {
			if (videoDao == null) {
				AppDatabase appDatabase = AppDatabase.getAppDatabase(VideoAnalysisActivity.this);
				if (appDatabase != null) {
					videoDao = appDatabase.videoDao();
				} else
					return;
			}
			removeVideoAndCloseScreen();

			if (mVideoPlayer != null) mVideoPlayer.release();
		}

	/*	@Override
		public void onCancelled() {
			if (mVideoPlayer != null) mVideoPlayer.release();
			initAnalysis();
		}*/
	};

	private void initAnalysis() {
		if (TextUtils.isEmpty(inputPath)) {
			Toast.makeText(this, "input path must be valid and not null", Toast.LENGTH_SHORT).show();
			setResult(RESULT_CANCELED);
			finish();
		}
		findViews();

		requestStoragePermission();
	}

	private void getVideoFromDB() {

		mVideoTableModel = videoDao.getVideoData(videoId);
		if (videoDao != null && videoDao.getMyVideoCount(session.getUserDetail().getUserId()) > 0 && videoId != 0 && mVideoTableModel != null && mVideoTableModel.getDecryptedVideoPath() != null) {
			Log.e("Got Decrypted Video", mVideoTableModel.getDecryptedVideoPath());
			File file = new File(mVideoTableModel.getDecryptedVideoPath());
			inputPath = file.getAbsolutePath();
			tempChallanCaptureTime = mVideoTableModel.getTempCapturedTimeStamp();

			if (!isVideoPlayerInit) {
				initAnalysis();
				isVideoPlayerInit = true;
			} else {
				initPlayer(inputPath);
			}
		} else {
			new MessageDialog(this).setMessage(getString(R.string.no_video_available_for_analysis)).cancelable(false).setPositiveButton(getString(R.string.ok), (dialog, which) -> {
				dialog.dismiss();
				setResult(RESULT_CANCELED);
				finish();
			}).show();
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
		switch (requestCode) {
			case STORAGE_REQUEST: {
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					initPlayer(inputPath);
				} else {
					Toast.makeText(this, "You must grant a write storage permission to use this functionality", Toast.LENGTH_SHORT).show();
					setResult(RESULT_CANCELED);
					finish();
				}
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		Logger.d("onResume Video analysis");
		getVideoFromDB();

		appDatabase = AppDatabase.getAppDatabase(this);
		if (appDatabase != null && appDatabase.eventDao().getLastEventData() != null) {
			EventTableModel eventTableModel = appDatabase.eventDao().getLastEventData();
			if (eventTableModel != null) {
				nextEventId = eventTableModel.getId() + 1;
			}
		} else {
			nextEventId = nextEventId + 1;
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		if (isVideoPlaying) {
			if (mVideoPlayer != null) {
				mVideoPlayer.play(true);
			} else {
				mVideoPlayer = new VideoPlayer(this);
				mVideoPlayer.play(true);

			}

		}
	}

	@Override
	public void onStop() {
		if (mVideoPlayer != null && mVideoPlayer.isPlaying()) {
			mVideoPlayer.play(false);

		}
		super.onStop();
	}

	@Override
	public void onDestroy() {
		if (mVideoPlayer != null) {
			mVideoPlayer.release();
		}
		super.onDestroy();
	}
	@SuppressLint("LongLogTag")
	public void releaseResources() {
		// release everything we grabbed
		if (mVideoPlayer != null) {
			try {
				mVideoPlayer.play(false);
				mVideoPlayer.release();
				mVideoPlayer = null;
			} catch (Exception e) {
				Log.e("releaseResources(): message %s cause %s", e.getMessage(),  e.getCause());
				e.printStackTrace();
			}
		}
	}
	private VideoPlayer.VideoPlayCancelled videoPlayCancelled = new VideoPlayer.VideoPlayCancelled() {
		@Override
		public void onCancelled() {
		/*	if (mVideoPlayer != null) mVideoPlayer.release();
			initAnalysis();
			isVideoPlayerInit = true;
			getVideoFromDB();*/
			onBackPressed();
		}
	};

	@Override
	public void onFirstTimeUpdate(long duration, long currentPosition) {

	}

	@Override
	public void onProgressUpdate(long currentPosition, long duration, long bufferedPosition) {
		Logger.e("onProgressUpdate");
		if (mVideoPlayer != null) if (!mVideoPlayer.isPlaying()) {
			if (mVideoPlayer.isPlaying()) {
				playPause();
			}
		}
	}

	private void findViews() {
       /* Toolbar toolbar = findViewById(R.id.toolbar);
        setupToolBarWithBackArrow(toolbar, "");*/
		mCropVideoView = findViewById(R.id.cropVideoView);
		TextView txtFiveX = mCropVideoView.findViewById(R.id.txtFiveX);
		TextView txtOneX = mCropVideoView.findViewById(R.id.txtOneX);
		ImageView imgCut = mCropVideoView.findViewById(R.id.imgCut);
		ImageView ivPause = mCropVideoView.findViewById(R.id.exo_pause);
		txtFiveX.setOnClickListener(view -> mVideoPlayer.setPlayBackParams(5f));
		txtOneX.setOnClickListener(view -> mVideoPlayer.setPlayBackParams(1f));
//        imgCut.setOnClickListener(v -> handleCropStart());

		ivPause.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				from = isVideoPlaying ? "" : "Listing";
				if (isVideoPlaying) {
					pauseTime = mVideoPlayer.getPlayer().getCurrentPosition();
				}
				playPause();
				Logger.e("Play/Pause time: ", "==>" + mVideoPlayer.getPlayer().getCurrentPosition());
			}
		});

		imgCut.setOnClickListener(v -> {

			if (SystemClock.elapsedRealtime() - lastClickTime < 2000) {
				return;
			}
			lastClickTime = SystemClock.elapsedRealtime();

			mVideoPlayer.play(false);
			if (mVideoPlayer.getPlayer() != null) {
				tempChallanCaptureTime = mVideoPlayer.getPlayer().getCurrentPosition();
				//This method use Mp4Parser
				cutVideo();
				Logger.e("CaptureTime: ", "====>" + tempChallanCaptureTime);
			} else {
				new MessageDialog(this).setMessage(getString(R.string.something_went_wrong)).setPositiveButton(getString(R.string.ok), (dialog12, which) -> {
					getVideoFromDB();
					dialog12.dismiss();
				}).show();
			}

			//                            handleCropStart();



            /*new VehicleNumberDialog(this, this).setMessage(getString(R.string.please_enter_vehicle_number)).cancelable(false).setListener(new OnBottomSheetClickListener<String>() {
                @Override
                public void onItemClicked(String vehicleNumber, Dialog dialog) {
                    dialog.dismiss();
                    numberPlate = vehicleNumber;

                    //This method use FFMPEG
//                            handleCropStart();

                    //This method use Mp4Parser
                    cutVideo();
                }

                @Override
                public void onItemCancel(Dialog dialog) {

                }
            }).show();*/
		});
	}

	private void cutVideo() {

		showProgressBar().show();

		Uri source = Uri.parse(inputPath);
		outputPath = Utils.createNewFile(this, true, false, false, nextEventId).getAbsolutePath();

		long currentPosition = mVideoPlayer.getPlayer().getCurrentPosition();
		long duration = mVideoPlayer.getPlayer().getDuration();
		long startCrop = currentPosition - VIDEO_CROP_ONE_SIDE_DURATION < 0 ? 0 : currentPosition - VIDEO_CROP_ONE_SIDE_DURATION;
		long endCrop = currentPosition + VIDEO_CROP_ONE_SIDE_DURATION > duration ? duration : currentPosition + VIDEO_CROP_ONE_SIDE_DURATION;

		Logger.e("Pause Duration: ", String.valueOf(duration));
		Logger.e("EndTimestamp: ", String.valueOf(mVideoTableModel.getEndTimestamp()));
		Logger.e("StartTimestamp: ", String.valueOf(mVideoTableModel.getEndTimestamp() - mVideoTableModel.getDuration() + duration));

		try {
			TrimVideoUtils.startTrim(new File(inputPath), new File(outputPath), startCrop, endCrop, new OnTrimVideoListener() {

				@Override
				public void onTrimStarted() {
					Log.e("Event ", "onTrimStarted");
				}

				@Override
				public void getResult(Uri uri) {
					Log.e("Event ", "getResult");
					Log.e("uri ", String.valueOf(uri));
					mVideoPlayer.release();
					compressVideo();
				}

				@Override
				public void cancelAction() {
					Log.e("Event ", "cancelAction");
					hideProgressBar();
				}

				@Override
				public void onError(String message) {
					Log.e("Event ", "onError: " + message);
					hideProgressBar();
				}
			});
		} catch (Exception e) {
			//Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
			e.printStackTrace();
			hideProgressBar();
		}
	}

	private void handleCropStart() {
		mVideoPlayer.play(false);

		outputPath = Utils.createNewFile(this, true, false, false, nextEventId).getAbsolutePath();

		long currentPosition = mVideoPlayer.getPlayer().getCurrentPosition();
		long duration = mVideoPlayer.getPlayer().getDuration();
		long startCrop = currentPosition - VIDEO_CROP_ONE_SIDE_DURATION < 0 ? 0 : currentPosition - VIDEO_CROP_ONE_SIDE_DURATION;
		long endCrop = currentPosition + VIDEO_CROP_ONE_SIDE_DURATION > duration ? duration : currentPosition + VIDEO_CROP_ONE_SIDE_DURATION;

		Logger.e("Pause Duration: ", String.valueOf(duration));
		Logger.e("EndTimestamp: ", String.valueOf(mVideoTableModel.getEndTimestamp()));
		Logger.e("StartTimestamp: ", String.valueOf(mVideoTableModel.getEndTimestamp() - mVideoTableModel.getDuration() + duration));

		String[] complexCommand = {"-ss", "" + startCrop / 1000, "-y", "-i", inputPath, "-t", "" + (endCrop - startCrop) / 1000, "-s", "320x240", "-r", "15", "-c:v", "libx264", "-b:v", "2097152", "-b:a", "48000", "-ac", "2", "-ar", "22050", outputPath};

		setUpFFmpeg();
		execFFmpegBinary(complexCommand);
	}

	private void setUpFFmpeg() {
		ffmpeg = FFmpeg.getInstance(this);

		try {
			ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

				@Override
				public void onStart() {
					Log.d("Event ", "onStart");
				}

				@Override
				public void onFailure() {
					Log.d("Event ", "onFailure");
				}

				@Override
				public void onSuccess() {
					Log.d("Event ", "onSuccess");
				}

				@Override
				public void onFinish() {
					Log.d("Event ", "onFinish");
				}
			});
		} catch (com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException e) {
			// Handle if FFmpeg is not supported by device
			e.printStackTrace();
		}
	}

	private void execFFmpegBinary(String[] command) {
		ffmpegProgressDialog = new ProgressDialog(this, getString(R.string.preparing_video));
		try {

			ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {

				@Override
				public void onStart() {
					ffmpegProgressDialog.show();
					Log.d("Event ", "onStart");
					displayCroppingMessage();
				}

				@Override
				public void onProgress(String message) {
					Log.e("Event ", "onProgress - " + message);
				}

				@Override
				public void onFailure(String message) {
					Log.e("Event ", "onFailure - " + message);
					ffmpegProgressDialog.dismiss();
				}

				@Override
				public void onSuccess(String message) {
					Log.e("Event ", "onSuccess - " + message);
					ffmpegProgressDialog.dismiss();
					finalVideoPath = outputPath;
					cutImage();
				}

				@Override
				public void onFinish() {
					Log.e("Event ", "onFinish");
				}
			});
		} catch (FFmpegCommandAlreadyRunningException e) {
			// Handle if FFmpeg is already running
			e.printStackTrace();
		}
	}

	private void displayCroppingMessage() {
		new Handler().postDelayed(croppingRunnable, 5000);
	}

	private void displayFinalizingMessage() {
		new Handler().postDelayed(finalizingRunnable, 10000);
	}

	Runnable croppingRunnable = new Runnable() {
		@Override
		public void run() {
			if (ffmpegProgressDialog != null && ffmpegProgressDialog.isShowing()) {
				ffmpegProgressDialog.updateText(getString(R.string.cropping_video));
				displayFinalizingMessage();
			}
		}
	};

	Runnable finalizingRunnable = new Runnable() {
		@Override
		public void run() {
			if (ffmpegProgressDialog != null && ffmpegProgressDialog.isShowing()) {
				ffmpegProgressDialog.updateText(getString(R.string.finalizing_video));
			}
		}
	};

	private void compressVideo() {

		compressedVideoPath = Utils.createNewFile(VideoAnalysisActivity.this, true, true, false, false, nextEventId).getAbsolutePath();
		VideoCompress.compressVideoLow(outputPath, compressedVideoPath, new VideoCompress.CompressListener() {
			@Override
			public void onStart() {
				Logger.e("VideoCompress: ", "onStart");
			}

			@Override
			public void onSuccess() {
				Logger.e("VideoCompress: ", "onSuccess");
				finalVideoPath = compressedVideoPath;
				updateProgressDialogText(getString(R.string.please_wait));
				cutImage();
			}

			@Override
			public void onFail() {
				Logger.e("VideoCompress: ", "onFail");
			}

			@Override
			public void onProgress(float percent) {
				Logger.e("VideoCompress: ", "Progress: " + percent + "%");
				updateProgressDialogText(getString(R.string._percentage_completed, String.valueOf(Math.round(percent)).concat("%")));
//                tv_progress.setText(String.valueOf(percent) + "%");
			}
		});
	}

	private void handleBundleData(Bundle bundle) {
		if (bundle != null) {
			videoId = bundle.getInt(EXTRA_DATA_VIDEO_ID, 1);
			pauseTime = bundle.containsKey(EXTRA_DATA_CHALLAN_CAPTURE_TIMESTAMP) ? bundle.getLong(EXTRA_DATA_CHALLAN_CAPTURE_TIMESTAMP, 1) : 0;
			if (pauseTime != 0) {
				findViews();

				isVideoPlayerInit = true;
				Logger.e("Bundle Success", getResources().getString(R.string.something_went_wrong));

				getVideoFromDB();
			} else {
				Logger.e("Bundle", getResources().getString(R.string.something_went_wrong));

			}

//            if (videoId == 0) videoId = videoDao.getLastCapturedVideo().getId();
			setupToolBarWithBackArrow(mBinding.toolbar.toolbar, numberPlate);
		} else {
			Logger.d("bundle", getResources().getString(R.string.something_went_wrong));
		}
	}

	public Bitmap drawableToBitmap(Drawable drawable) {
		Bitmap bitmap = null;

		if (drawable instanceof BitmapDrawable) {
			BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
			if (bitmapDrawable.getBitmap() != null) {
				return bitmapDrawable.getBitmap();
			}
		}

		if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
			bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
		} else {
			bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
		}

		Canvas canvas = new Canvas(bitmap);
		drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
		drawable.draw(canvas);
		return bitmap;
	}

	private void playPause() {
		isVideoPlaying = !mVideoPlayer.isPlaying();
		if (mVideoPlayer.isPlaying()) {
			mVideoPlayer.play(!mVideoPlayer.isPlaying());
			return;
		}
		mVideoPlayer.play(!mVideoPlayer.isPlaying());
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mVideoPlayer != null) {
			mVideoPlayer.play(false);
			if (mVideoPlayer.getPlayer() != null) {
				pauseTime = mVideoPlayer.getPlayer().getCurrentPosition();
			}
			mVideoPlayer.release();
			mVideoPlayer = null;

		}
		isVideoPlayerInit = true;
		/*mVideoPlayer.play(false);
		if (mVideoPlayer.getPlayer() != null) {
			tempChallanCaptureTime = mVideoPlayer.getPlayer().getCurrentPosition();
		}*/
	}

	private void requestStoragePermission() {
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_REQUEST);
		} else {
			initPlayer(inputPath);
		}
	}

	private void cutImage() {

		// Method-1
		MediaMetadataRetriever media = new MediaMetadataRetriever();
		media.setDataSource(inputPath);
//        extractedImage = media.getImageAtIndex(20);
		extractedImage = media.getFrameAtTime(tempChallanCaptureTime * 1000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
		Logger.e("tempChallanCaptureTime: ", "=>" + String.valueOf(tempChallanCaptureTime));


		// Method-2
        /*FFmpegMediaMetadataRetriever mmr = new FFmpegMediaMetadataRetriever();
        mmr.setDataSource(inputPath);
        mmr.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_ALBUM);
        mmr.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_ARTIST);
        mmr.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);

        long duration = mmr.getMetadata().getLong("duration");
        Logger.e("duration: ", String.valueOf(duration));
        double frameRate = mmr.getMetadata().getDouble("framerate");
        Logger.e("frameRate: ", String.valueOf(frameRate));
        int numberOfFrame = (int) (duration / frameRate);
        Logger.e("numberOfFrame: ", String.valueOf(numberOfFrame));

        int frame = (int) ((tempChallanCaptureTime * numberOfFrame) / duration);

        Bitmap tempImage = mmr.getFrameAtTime(tempChallanCaptureTime * 1000, FFmpegMediaMetadataRetriever.OPTION_CLOSEST_SYNC);

        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(tempImage, tempImage.getWidth(), tempImage.getHeight(), true);
        extractedImage = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

        mmr.release();*/


		// File pictureFile = Utils.createFileImage(extractedImage, "Image_Crop_");
		File pictureFile = null;

		try {
			pictureFile = Utils.createNewFile(this, false, false, false, nextEventId);

			FileOutputStream fos = new FileOutputStream(pictureFile);
			extractedImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
			fos.close();
		} catch (FileNotFoundException e) {
			Log.d("FileNotFoundException", "File not found: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Log.d("FileNotFoundException", "Error accessing file: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		hideProgressBar();
		if (mVideoPlayer != null) {
			mVideoPlayer.release();
		}
		Intent intent = new Intent(this, ZoomCropActivity.class);
		assert pictureFile != null;
		intent.putExtra(EXTRA_DATA_CAPTURED_IMAGE, pictureFile.getAbsolutePath());
		Log.d("EXTRA_DATA_POS", "tempChallanCaptureTime: " + tempChallanCaptureTime);
//		intent.putExtra(EXTRA_DATA_POS, tempChallanCaptureTime);
//        intent.putExtra(EXTRA_DATA_CAPTURED_VIDEO, outputPath);
//        intent.putExtra(EXTRA_DATA_CAPTURED_VIDEO, compressedVideoPath);
		intent.putExtra(EXTRA_DATA_CAPTURED_VIDEO, finalVideoPath);
		intent.putExtra(EXTRA_DATA_EVENT_ID, nextEventId);
		intent.putExtra(EXTRA_DATA_CHALLAN_CAPTURE_TIMESTAMP, tempChallanCaptureTime);
		intent.putExtra(EXTRA_DATA_VIDEO_ID, videoId);
		intent.putExtra(EXTRA_DATA_NUMBER_PLATE, numberPlate);
		startActivity(intent);
		finish();
	}

	private void initPlayer(String uri) {
		if (!new File(uri).exists()) {
			removeVideoAndCloseScreen();

            /*Toast.makeText(this, "File doesn't exists", Toast.LENGTH_SHORT).show();
            setResult(RESULT_CANCELED);
            finish();
            return;*/
		}

		mVideoPlayer = new VideoPlayer(this);
		mCropVideoView.setPlayer(mVideoPlayer.getPlayer());
		mVideoPlayer.initMediaSource(this, uri);
		mVideoPlayer.setUpdateListener(this);
		mVideoPlayer.setPlayBackParams(1f); //set 1X or 5X parameter
		mVideoPlayer.setLastChalanTime(tempChallanCaptureTime); //set last challan time
		mVideoPlayer.seekTo((pauseTime > tempChallanCaptureTime) ? pauseTime : tempChallanCaptureTime); //seek to last challan time
		mVideoPlayer.setVideoFinishedListener(videoPlayFinished); //set last chalan time
		mVideoPlayer.setVideoCancelledListener(videoPlayCancelled); //set last chalan time

		if (from.equalsIgnoreCase("Listing")) {
			playPause();
		}
	}

	private void removeVideoAndCloseScreen() {
		try {
			if (appDatabase.configDao() != null && appDatabase.configDao().getConfigData() != null) {

				VideoTableModel mVideoModel = appDatabase.videoDao().getVideoData(videoId);
				long challanCaptureTime = mVideoModel.getChallanCaptureTime();
				long totalDuration = mVideoModel.getDuration();
				long remainingTime = totalDuration - challanCaptureTime;

				if (remainingTime > 0) {
					appDatabase.configDao().updateRemainingQuota(session.getUserDetail().getUserId(), appDatabase.configDao().getConfigData().getRemainingMinutes() + remainingTime);
					Logger.e("VideoCaptureService", "now recordingQuotaMillis in minutes ==> " + TimeStamp.getMinutesFromMilliseconds(appDatabase.configDao().getConfigData().getRemainingMinutes()));
				}
			}
			Utils.deleteVideo(videoDao.getVideoData(videoId).getDecryptedVideoPath());
			Utils.deleteVideo(videoDao.getVideoData(videoId).getEncryptedVideoPath());
			Utils.deleteVideo(videoDao.getVideoData(videoId).getVideoThumbnail());
			videoDao.removeVideo(videoId);
			onBackPressed();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		Logger.e("onNewIntent", "Came from Upload Event");
        isEventCreated = true;
        getVideoFromDB();
	}

	@Override
	public void onBackPressed() {
		releaseResources();
		Intent mIntent = new Intent();
		setResult(isEventCreated ? RESULT_OK : RESULT_CANCELED, mIntent);
		super.onBackPressed();
	}
}