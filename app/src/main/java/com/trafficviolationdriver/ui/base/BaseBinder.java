package com.trafficviolationdriver.ui.base;

import android.content.res.Resources;
import android.net.Uri;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.trafficviolationdriver.R;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.databinding.BindingAdapter;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.trafficviolationdriver.webservice.APIs.BASE_IMAGE_PATH;

public class BaseBinder {

    @BindingAdapter({"app:setLanguageIcon"})
    public static void setFlagIcon(AppCompatImageView iv, String image) {

        if (image != null && image.length() > 0 && !image.equals("null")) {
            Resources resources = iv.getContext().getResources();
            final int resourceId = resources.getIdentifier(image, "drawable",
                    iv.getContext().getPackageName());
            iv.setImageResource(resourceId);
        } else {
            iv.setImageResource(R.mipmap.ic_launcher);
        }
    }

    @BindingAdapter({"app:setImageUri", "app:setImageUrl"})
    public static void setImage(AppCompatImageView iv, Uri imageUri, String Url) {

        if (Url == null || Url.length() == 0) {
            setImageUri(iv, imageUri);
        } else {
            setImageUrl(iv, Url);
        }
    }
    @BindingAdapter({"app:setImageUri"})
    public static void setImageUri(AppCompatImageView iv, Uri imageUri) {

        if (imageUri != null)
         iv.setImageURI(imageUri);
        else
            iv.setImageResource(R.drawable.shape_circle_grey);
    }

    @BindingAdapter({"app:setImageUrl"})
    public static void setImageUrl(AppCompatImageView iv, String imageUrl) {

        if (imageUrl != null && imageUrl.length() > 0 && !imageUrl.equals("null"))
            Glide.with(iv.getContext())
                    .load(BASE_IMAGE_PATH + imageUrl)
                    .apply(new RequestOptions()
                            .error(R.drawable.shape_circle_grey)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .centerCrop())
                    .into(iv);
        else
            iv.setImageResource(R.drawable.shape_circle_grey);
    }

    @BindingAdapter({"app:setSimpleImage"})
    public static void setSimpleImage(AppCompatImageView iv, String image) {

        if (image != null && image.length() > 0 && !image.equals("null"))
            Glide.with(iv.getContext())
                    .load(BASE_IMAGE_PATH + image)
                    .apply(new RequestOptions()
                            .placeholder(R.color.transparent)
                            .error(R.color.grey)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .centerCrop())
                    .into(iv);
        else
            iv.setImageResource(R.drawable.shape_circle_grey);    }
}
