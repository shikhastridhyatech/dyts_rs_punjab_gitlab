package com.trafficviolationdriver.ui.home;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.broadcastreceivers.NotificationBroadcastReceiver;
import com.trafficviolationdriver.databinding.ActivityDashBoardBinding;
import com.trafficviolationdriver.db.AppDatabase;
import com.trafficviolationdriver.db.entity.ConfigTableModel;
import com.trafficviolationdriver.dialogs.MessageDialog;
import com.trafficviolationdriver.interfaces.OnNotificationDataReceivedListener;
import com.trafficviolationdriver.model.DashBoardModel;
import com.trafficviolationdriver.model.DashboardEventModel;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.ui.common.LanguageSelectionActivity;
import com.trafficviolationdriver.ui.message.MessagesActivity;
import com.trafficviolationdriver.ui.message.RejectedEventsActivity;
import com.trafficviolationdriver.ui.videocapture.VideoCaptureActivity;
import com.trafficviolationdriver.ui.videocapture.VideoListingActivity;
import com.trafficviolationdriver.util.FileUtil;
import com.trafficviolationdriver.util.LocationUtil;
import com.trafficviolationdriver.util.Logger;
import com.trafficviolationdriver.util.PermissionUtils;
import com.trafficviolationdriver.util.TimeStamp;
import com.trafficviolationdriver.webservice.APIs;
import com.trafficviolationdriver.webservice.JSONCallback;
import com.trafficviolationdriver.webservice.Retrofit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import static com.trafficviolationdriver.util.AppConstants.INTENT_FILTER_MESSAGE_RECEIVED;
import static com.trafficviolationdriver.util.MemoryUtil.getAvailableInternalMemorySize;
import static com.trafficviolationdriver.util.MemoryUtil.getMemoryInfo;
import static com.trafficviolationdriver.util.NetworkUtil.isConnectedFast;


public class DashBoardActivity extends BaseActivity implements View.OnClickListener, OnNotificationDataReceivedListener<Intent> {

    private static final String TAG = DashBoardActivity.class.getSimpleName();
    private static final int ANALYSIS_RESULT_CODE = 9;
    private final static int REQUEST_PERMISSIONS_LOCATION = 12;
    ActivityDashBoardBinding mBinding;
    boolean doubleBackToExitPressedOnce = false;
    ActionBarDrawerToggle toggle;
    DashBoardModel mDashBoardModel;
    View headerView;
    TabLayout.Tab firstTab;
    ImageView ivMenu;
    NotificationBroadcastReceiver mNotificationReceiver;

    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location mLastKnownLocation;
    FusedLocationProviderClient mFusedLocationProviderClient;
    int retryCount = 0;
    private boolean isConnectedFast,isMemoryAvailable,isInternalMemoryAvailable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(DashBoardActivity.this, R.layout.activity_dash_board);
        toggle = new ActionBarDrawerToggle(this, mBinding.drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        isConnectedFast = isConnectedFast(getApplicationContext());
        isMemoryAvailable=getMemoryInfo(getApplicationContext());
        mBinding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        setupToolBarWithMenu(mBinding.toolbar.toolbar, getString(R.string.traffic_police));

        ivMenu = mBinding.toolbar.toolbar.findViewById(R.id.ivMenu);
        ivMenu.setOnClickListener(this);

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        if (!(hasAppPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}))) {
            getLocationPermission();
        } else {
            getCurrentLocation();
        }

        // Create Periodic Worker to check and upload driver's location data on server
        LocationUtil.setLocationDataUploadWorker(this);
        FileUtil.setFirstTimeCleanDirectoryWorker(this);

        setInitView();
        setUpTabLayout();
        setUpNavigationLayout();
        setClickEvents();

        API_getDriverProfile(String.valueOf(session.getUserDetail().getUserId()));

        ArrayList<String> data = (ArrayList<String>) AppDatabase.getAppDatabase(this).locationDao().getUploadDataArray();
        Logger.e("DriverLocationData: ", data.toString());

        mNotificationReceiver = new NotificationBroadcastReceiver(this, this, INTENT_FILTER_MESSAGE_RECEIVED);

    }

    @SuppressLint("StringFormatInvalid")
    @Override
    protected void onResume() {
        super.onResume();
        ConfigTableModel mConfigModel = AppDatabase.getAppDatabase(DashBoardActivity.this).configDao().getConfigData();
        Logger.e(mConfigModel.toString());
        //mBinding.layoutDashboard.tvEventTarget.setText(getResources().getString(R.string._out_of_minutes, TimeStamp.getMinutesFromMilliseconds(mConfigModel.getRemainingMinutes())(mConfigModel.getRemainingMinutes() / 60000), (mConfigModel.getQuotaMinutes() / 60000)));
        mBinding.layoutDashboard.tvEventTarget.setText(getResources().getString(R.string._out_of_minutes, TimeStamp.getMinutesFromMilliseconds(mConfigModel.getRemainingMinutes()), TimeStamp.getMinutesFromMilliseconds(mConfigModel.getQuotaMinutes())));
    }

    private void setInitView() {
        headerView = mBinding.nvDashboard.getHeaderView(0);
        AppCompatTextView mUserName = headerView.findViewById(R.id.tv_user_name);
        AppCompatTextView mUserEmail = headerView.findViewById(R.id.tv_user_email);

        if (session.isLoggedIn()) {
            mUserName.setText(session.getUserDetail().getFirstName().concat(" ").concat(session.getUserDetail().getLastName()));
            mUserEmail.setText(session.getUserDetail().getPhoneNumber());
        }
        mBinding.toolbar.ivNotification.setVisibility(View.VISIBLE);
        mBinding.pullToRefresh.setOnRefreshListener(() -> {
            firstTab.select();
            API_getDriverProfile(String.valueOf(session.getUserDetail().getUserId()));
            mBinding.pullToRefresh.setRefreshing(false);
        });
    }

    private void setNotificationData(int notificationCount) {
        if (notificationCount > 0) {
            mBinding.toolbar.tvNotificationCount.setText(String.valueOf(notificationCount));
            mBinding.toolbar.tvNotificationCount.setVisibility(View.VISIBLE);
        } else {
            mBinding.toolbar.tvNotificationCount.setVisibility(View.GONE);
        }
    }

    private void setClickEvents() {
        mBinding.layoutDashboard.viewRejectedEvents.setOnClickListener(this);
        mBinding.layoutDashboard.btnAnalysis.setOnClickListener(this);
        mBinding.toolbar.ivNotification.setOnClickListener(this);
    }

    private void setUpTabLayout() {
        firstTab = mBinding.layoutDashboard.tabLayout.newTab();
        firstTab.setText(getString(R.string.today));
        mBinding.layoutDashboard.tabLayout.addTab(firstTab, 0, true);

        TabLayout.Tab secondTab = mBinding.layoutDashboard.tabLayout.newTab();
        secondTab.setText(getString(R.string.last_7_days));
        mBinding.layoutDashboard.tabLayout.addTab(secondTab, 1, false);

        TabLayout.Tab thirdTab = mBinding.layoutDashboard.tabLayout.newTab();
        thirdTab.setText(getString(R.string.last_30_day));
        mBinding.layoutDashboard.tabLayout.addTab(thirdTab, 2, false);

        setTabListener();
    }

    private void setTabListener() {

        mBinding.layoutDashboard.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (mDashBoardModel != null) {
                    switch (tab.getPosition()) {
                        case 0:
                            updateRejectedEventView(true);
                            mBinding.layoutDashboard.viewRejectedEvents.setEnabled(true);
                            setDriverProfile(mDashBoardModel.getToday());
                            break;
                        case 1:
                            updateRejectedEventView(false);
                            setDriverProfile(mDashBoardModel.getLast7days());
                            break;
                        case 2:
                            updateRejectedEventView(false);
                            setDriverProfile(mDashBoardModel.getLast30days());
                            break;
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void updateRejectedEventView(boolean isEnable) {
        if (isEnable) {
            mBinding.layoutDashboard.viewRejectedEvents.setEnabled(true);
            mBinding.layoutDashboard.tvRejectedEvents.setAlpha(1f);
            mBinding.layoutDashboard.staticTvRejectedEvents.setAlpha(1f);
            mBinding.layoutDashboard.ivRejectedEvents.setAlpha(1f);
        } else {

            mBinding.layoutDashboard.viewRejectedEvents.setEnabled(false);
            mBinding.layoutDashboard.tvRejectedEvents.setAlpha(0.5f);
            mBinding.layoutDashboard.staticTvRejectedEvents.setAlpha(0.5f);
            mBinding.layoutDashboard.ivRejectedEvents.setAlpha(0.5f);
        }
    }

    private void setUpNavigationLayout() {
        mBinding.toolbar.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleDrawer();
            }
        });

        mBinding.nvDashboard.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                closeNavigationDrawer();
                switch (id) {
                    case R.id.navigation_home:
                        return true;
                    case R.id.navigation_profile:
                        startActivity(new Intent(DashBoardActivity.this, ProfileActivity.class));
                        return true;
                    case R.id.navigation_language:
                        startActivity(new Intent(DashBoardActivity.this, LanguageSelectionActivity.class));
                        return true;
                    case R.id.navigation_logout:
                        new MessageDialog(DashBoardActivity.this).setMessage(getString(R.string.alert_logout)).cancelable(true).setPositiveButton(getString(R.string.yes), (dialog, which) -> {
                            dialog.dismiss();
                            API_Logout(String.valueOf(session.getUserDetail().getUserId()));
                        }).setNegativeButton(getString(R.string.no), (dialog, which) -> dialog.dismiss()).show();
                        return true;
                }
                return false;
            }
        });
    }

    public void toggleDrawer() {
        if (mBinding.drawerLayout.isDrawerOpen(GravityCompat.START))
            mBinding.drawerLayout.closeDrawer(GravityCompat.START);
        else mBinding.drawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void closeNavigationDrawer() {
        boolean drawerIsOpen = mBinding.drawerLayout.isDrawerOpen(GravityCompat.START);
        if (drawerIsOpen) {
            mBinding.drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    private void API_Logout(String userId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", userId);
        try {
            Retrofit.with(this).setParameters(params).setAPI(APIs.API_LOGOUT).setCallBackListener(new JSONCallback(this) {
                @Override
                protected void onSuccess(int statusCode, JSONObject jsonObject) {
                    Logger.e(TAG, jsonObject.toString());
                    session.logoutUser(DashBoardActivity.this);
                }

                @Override
                protected void onFailed(int statusCode, String message) {
                    Logger.e(TAG, message);
                    session.logoutUser(DashBoardActivity.this);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            session.logoutUser(DashBoardActivity.this);
        }
    }

    private void API_getDriverProfile(String userId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", userId);
        try {
            Retrofit.with(this).setAPI(APIs.API_GET_DASHBOARD).setParameters(params).setCallBackListener(new JSONCallback(this, showProgressBar()) {
                @Override
                protected void onSuccess(int statusCode, JSONObject jsonObject) throws JSONException {
                    hideProgressBar();
                    if (jsonObject.optJSONObject("data") != null && jsonObject.optJSONObject("data").length() > 0) {

                        Type modelType = new TypeToken<DashBoardModel>() {
                        }.getType();
                        mDashBoardModel = new Gson().fromJson(jsonObject.optJSONObject("data").toString(), modelType);
                        Logger.e(mDashBoardModel.toString());
                        ConfigTableModel mConfigModel = AppDatabase.getAppDatabase(DashBoardActivity.this).configDao().getConfigData();
                        Logger.e(mConfigModel.toString());
                        mBinding.layoutDashboard.tvLastLogin.setText(TimeStamp.getGTSDateFromTimestamp((mDashBoardModel.getLastLogin())));
                        mBinding.layoutDashboard.tvEventTarget.setText(getResources().getString(R.string._out_of_minutes, TimeStamp.getMinutesFromMilliseconds(mConfigModel.getRemainingMinutes()), TimeStamp.getMinutesFromMilliseconds(mConfigModel.getQuotaMinutes())));

                        mBinding.layoutDashboard.tvRemainingEventTime.setText(getResources().getString(R.string._out_of_events, String.valueOf(mDashBoardModel.getRemainingEvents()), String.valueOf(mDashBoardModel.getTargetEventCount())));


                        mBinding.layoutDashboard.tvUserName.setText(String.format("%s %s", session.getUserDetail().getFirstName(), session.getUserDetail().getLastName()));
                        mBinding.layoutDashboard.tvUserCity.setText(session.getUserDetail().getCity());

                        setDriverProfile(mDashBoardModel.getToday());
                        setNotificationData(mDashBoardModel.getNotificationCount());
                    }
                }

                @Override
                protected void onFailed(int statusCode, String message) {
                    Logger.e(message);
                    hideProgressBar();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            hideProgressBar();
            new MessageDialog(this).setMessage(e.getMessage()).setPositiveButton(getString(R.string.retry), (dialog12, which) -> {
                dialog12.dismiss();
                API_getDriverProfile(String.valueOf(session.getUserDetail().getUserId()));
            }).setNegativeButton(getString(R.string.cancel), (dialog1, which) -> dialog1.dismiss()).show();
        }
    }

    private void setDriverProfile(DashboardEventModel mDashBoardEventModel) {
        mBinding.layoutDashboard.tvApprovedEvents.setText(String.valueOf(mDashBoardEventModel.getApprovedEvents()));
        mBinding.layoutDashboard.tvPendingEvents.setText(String.valueOf(mDashBoardEventModel.getPendingEvents()));
        mBinding.layoutDashboard.tvRejectedEvents.setText(String.valueOf(mDashBoardEventModel.getRejectedEvents()));
        mBinding.layoutDashboard.tvTotalEvents.setText(String.valueOf(mDashBoardEventModel.getToalEvents()));
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.prompt_exit), Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    @Override
    protected void onDestroy() {
        AppDatabase.destroyInstance();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationReceiver);
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.view_rejected_events:
                startActivity(new Intent(DashBoardActivity.this, RejectedEventsActivity.class));
                break;
            case R.id.btn_analysis:
                checkVideoInDB();
                break;
            case R.id.iv_notification:
                startActivity(new Intent(DashBoardActivity.this, MessagesActivity.class));
                setNotificationData(0);
                break;
            case R.id.ivMenu:
                mBinding.drawerLayout.openDrawer(GravityCompat.START);
                break;
            case R.id.tv_start_recording:
                //TODO Change to Remove Restrict user with low internet and ram
                isConnectedFast = isConnectedFast(getApplicationContext());
                isMemoryAvailable=getMemoryInfo(getApplicationContext());
                isInternalMemoryAvailable=getAvailableInternalMemorySize();

                if (isConnectedFast && isMemoryAvailable &&isInternalMemoryAvailable) {
                    PermissionUtils.isCheck(this, new String[]{PermissionUtils.CAMERA, PermissionUtils.READ_EXTERNAL, PermissionUtils.WRITE_EXTERNAL, PermissionUtils.RECORD_AUDIO, PermissionUtils.LOCATION, PermissionUtils.ACCESS_LOCATION}, PermissionUtils.CAMERA_CODE, permissionListener);
                } else if(!isConnectedFast){
                    showSnackBar(mBinding.drawerLayout, getResources().getString(R.string.poor_bandwidth));
                }else if(!isMemoryAvailable){
                    showSnackBar(mBinding.drawerLayout, getResources().getString(R.string.poor_memory));
                }else if(!isInternalMemoryAvailable){
                    showSnackBar(mBinding.drawerLayout, getResources().getString(R.string.poor_internal_storage));
                }else
                    showSnackBar(mBinding.drawerLayout, getResources().getString(R.string.poor_memory_bandwidth));

                break;
        }
    }

    private void checkVideoInDB() {
        AppDatabase appDatabase = AppDatabase.getAppDatabase(this);
        if (appDatabase.videoDao().getMyVideoCount(session.getUserDetail().getUserId()) > 0) {
            startActivityForResult(new Intent(DashBoardActivity.this, VideoListingActivity.class), ANALYSIS_RESULT_CODE);
        } else {
            new MessageDialog(this).setMessage(getString(R.string.no_video_available_for_analysis)).cancelable(false).setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                dialog.dismiss();
            }).show();
        }
    }

    PermissionUtils.permissionListener permissionListener = new PermissionUtils.permissionListener() {
        @Override
        public void onAllow(int requestCode) {
            checkGpsUnabled();
        }

        @Override
        public void onDeny(int requestCode) {
            showPermissionAllowDialog();
        }

        @Override
        public void onDenyNeverAskAgain(int requestCode) {
            showPermissionAllowDialog();
        }
    };

    private void checkGpsUnabled() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            ConfigTableModel mConfigModel = AppDatabase.getAppDatabase(DashBoardActivity.this).configDao().getConfigData();
            if (mConfigModel != null) {
                if ((mConfigModel.getRemainingMinutes() / 1000) <= 0) {
                    new MessageDialog(this)
                            .setMessage(getString(R.string.your_event_target_quota_is_completed))
                            .cancelable(false)
                            .setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                                dialog.dismiss();
                            }).show();
                } else {
                    startActivity(new Intent(DashBoardActivity.this, VideoCaptureActivity.class));
                }
            }
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.your_gps_permission_is_disabled)).setCancelable(false).setPositiveButton(getString(R.string.yes), (dialog, id) -> startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))).setNegativeButton(getString(R.string.no), (dialog, id) -> dialog.cancel());
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void showPermissionAllowDialog() {
        new MessageDialog(DashBoardActivity.this).setMessage(getString(R.string.you_should_allow_this_permission_in_order_to_run_the_app)).cancelable(false).setPositiveButton(getString(R.string.yes), (dialog, which) -> {
            dialog.dismiss();
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setData(Uri.parse("package:" + getPackageName()));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            startActivity(intent);
        }).setNegativeButton(getString(R.string.no), (dialog, which) -> {
            dialog.dismiss();
        }).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionUtils.onRequestPermissionsResult(this, requestCode, permissions, grantResults, permissionListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == ANALYSIS_RESULT_CODE) {
            firstTab.select();
            API_getDriverProfile(String.valueOf(session.getUserDetail().getUserId()));
        }
    }

    @Override
    public void onDataReceived(Intent data) {
        /*if (data != null) {
            Logger.e(data.getStringExtra("message"));
        }*/

        firstTab.select();
        API_getDriverProfile(String.valueOf(session.getUserDetail().getUserId()));
    }


    private void getLocationPermission() {
        requestAppPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_PERMISSIONS_LOCATION, new BaseActivity.setPermissionListener() {
            @Override
            public void onPermissionGranted(int requestCode) {
                getCurrentLocation();
            }

            @Override
            public void onPermissionDenied(int requestCode) {
                showSnackBar(toolbar, getString(R.string.please_grant_permission), Snackbar.LENGTH_INDEFINITE, getString(R.string.settings), () -> {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    startActivity(intent);
                });
            }
        });
    }

    //Getting current location
    private void getCurrentLocation() {

        try {
            Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
            locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>() {
                @SuppressLint("StringFormatMatches")
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    if (task.isSuccessful()) {
                        // Set the map's camera position to the current location of the device.
                        mLastKnownLocation = task.getResult();
                        if (mLastKnownLocation == null) {
                            if (retryCount < 3) {
                                getCurrentLocation();
                                retryCount++;
                            } else {
                                Logger.e(getResources().getString(R.string.not_able_to_get_location_please_try_again));
                            }
                        } else {
                            Logger.e(getResources().getString(R.string.location_fetched, mLastKnownLocation.toString()));
                            API_GetCity(mLastKnownLocation);
                        }
                    } else {
                        Logger.d(TAG, "Current location is null. Using defaults.");
                        Logger.e(TAG, "Exception: %s" + task.getException());
                    }
                }
            });
        } catch (SecurityException e) {
            Logger.e("Exception: %s", e.getMessage());
        }
    }

    private void API_GetCity(Location mLastKnownLocation) {
        HashMap<String, String> params = new HashMap<>();
        params.put("latlng", mLastKnownLocation.getLatitude() + "," + mLastKnownLocation.getLongitude());
        params.put("sensor", String.valueOf(false));
        params.put("key", "AIzaSyBtWnExOjgjgPQRyVXZ1E0AvvSj75Y5Ods");
        try {
            Retrofit.with(this).setUrl(APIs.LOCATION_URL).setCustomGetParameters(params).setCustomCallBackListener(new JSONCallback(this) {
                @SuppressLint("StringFormatMatches")
                @Override
                protected void onSuccess(int statusCode, JSONObject jsonObject) {
                    try {
                        String Status = jsonObject.getString("status");
                        if (Status.equalsIgnoreCase("OK")) {
                            JSONArray Results = jsonObject.getJSONArray("results");
                            JSONObject zero = Results.getJSONObject(0);
                            JSONArray address_components = zero.getJSONArray("address_components");
                            /*for (int i = 0; i < address_components.length(); i++) {
                                JSONObject zero2 = address_components.getJSONObject(i);
                                String cityName = zero2.getString("long_name");
                                JSONArray mtypes = zero2.getJSONArray("types");
                                String Type = mtypes.getString(0);
                                if (Type.equalsIgnoreCase("locality")) {

                                    Log.e(" CityName --->", cityName + "");

                                    if (session.getUserDetail().getCity().toLowerCase().equals(cityName.toLowerCase())) {
                                        Logger.e("Is User of Current Company:", "True");

                                    } else {
                                        Logger.e("Is User of Current Company:", "False");
                                        new MessageDialog(DashBoardActivity.this)
                                                .setMessage(getString(R.string.error_wrong_city, cityName, session.getUserDetail().getCity())).cancelable(true).setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                                            dialog.dismiss();
                                            API_Logout(String.valueOf(session.getUserDetail().getUserId()));
                                        }).show();
                                    }
                                }
                            }*/
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                protected void onFailed(int statusCode, String message) {
                    showLongToast(message);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //  TODO The following two intent-filters are the key to set homescreen

/*
	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			switch (event.getKeyCode()) {
				case KeyEvent.KEYCODE_BACK:
					return false;
				case KeyEvent.KEYCODE_HOME:
					return false;
			}
		} else if (event.getAction() == KeyEvent.ACTION_UP) {
			switch (event.getKeyCode()) {
				case KeyEvent.KEYCODE_BACK:
					if (!event.isCanceled()) {
						// Do BACK behavior.
						return false;
					}
					return false;
				case KeyEvent.KEYCODE_HOME:
					if (!event.isCanceled()) {
						// Do HOME behavior.
						return true;
					}
					return false;
				default:
					return false;


			}
		}

		return super.dispatchKeyEvent(event);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_HOME) {
			Log.i("TAG", "Press Home");
			System.exit(0);
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}
*/

}