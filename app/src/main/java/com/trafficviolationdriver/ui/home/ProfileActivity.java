package com.trafficviolationdriver.ui.home;

import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.databinding.ActivityProfileBinding;
import com.trafficviolationdriver.dialogs.MessageDialog;
import com.trafficviolationdriver.model.UserDataModel;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.webservice.APIs;
import com.trafficviolationdriver.webservice.JSONCallback;
import com.trafficviolationdriver.webservice.Retrofit;

import org.json.JSONObject;

import java.util.HashMap;

import androidx.databinding.DataBindingUtil;

public class ProfileActivity extends BaseActivity {

    ActivityProfileBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_profile);
        initView();
    }

    private void initView() {
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.profile));
        mBinding.setUserdata(session.getUserDetail());
        mBinding.executePendingBindings();
    }

}



