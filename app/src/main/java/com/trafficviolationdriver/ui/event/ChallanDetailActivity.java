package com.trafficviolationdriver.ui.event;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.adapter.ViolationReasonsAdapter;
import com.trafficviolationdriver.databinding.ActivityChallanDetailBinding;
import com.trafficviolationdriver.dialogs.MessageDialog;
import com.trafficviolationdriver.interfaces.OnRecyclerViewItemClicked;
import com.trafficviolationdriver.model.ViolationReasonsModel;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.ui.message.RejectedEventDetailActivity;
import com.trafficviolationdriver.util.Logger;
import com.trafficviolationdriver.webservice.APIs;
import com.trafficviolationdriver.webservice.JSONCallback;
import com.trafficviolationdriver.webservice.ResponseUtils;
import com.trafficviolationdriver.webservice.Retrofit;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

public class ChallanDetailActivity extends BaseActivity implements OnRecyclerViewItemClicked<ViolationReasonsModel> {

    ActivityChallanDetailBinding mBinding;

    ViolationReasonsAdapter mAdapter;
    ArrayList<ViolationReasonsModel> mViolationReasonsList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_challan_detail);

        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.rejected_events));
        API_GetViolationReasons();
    }

    private void setViolationReasonsAdapter() {
        if (mAdapter == null) {
            mAdapter = new ViolationReasonsAdapter(this, mViolationReasonsList, this);
            mBinding.rvViolationReasonsList.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }
        mBinding.tvNoRecordFound.setVisibility(mViolationReasonsList.size() > 0 ? View.GONE : View.VISIBLE);
        mBinding.rvViolationReasonsList.setVisibility(mViolationReasonsList.size() > 0 ? View.VISIBLE : View.GONE);
    }


    @Override
    public void onItemClicked(ViolationReasonsModel mReasons) {
        if (mReasons != null) {
            startActivity(new Intent(this, RejectedEventDetailActivity.class).putExtra(RejectedEventDetailActivity.BUNDLE_DATA_REJECTED_EVENT_ID, mReasons));
        }
    }



    private void API_GetViolationReasons() {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", String.valueOf(session.getUserDetail().getUserId()));

        try {
            Retrofit.with(ChallanDetailActivity.this).
                    setAPI(ResponseUtils.getRequestAPIURL(APIs.API_GET_VIOLATION_REASONS, session)).
                    setParameters(params).
                    setCallBackListener(new JSONCallback(ChallanDetailActivity.this, showProgressBar()) {
                        @Override
                        protected void onSuccess(int statusCode, JSONObject jsonObject) {

                            hideProgressBar();
                            if (jsonObject.optJSONObject("data") != null) {
                                try {
                                    Type modelType = new TypeToken<List<ViolationReasonsModel>>() {
                                    }.getType();
                                    mViolationReasonsList = new Gson().fromJson(jsonObject.optJSONObject("data").getJSONArray("reasons").toString(), modelType);
                                    setViolationReasonsAdapter();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        protected void onFailed(int statusCode, String message) {
                            Logger.e(message);
                            hideProgressBar();
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
            hideProgressBar();
            new MessageDialog(ChallanDetailActivity.this).setMessage(e.getMessage()).setPositiveButton(getString(R.string.retry), (dialog, which) -> {
                dialog.dismiss();
                API_GetViolationReasons();
            }).setNegativeButton(getString(R.string.cancel), (dialog1, which) -> dialog1.dismiss()).show();
        }
    }



}
