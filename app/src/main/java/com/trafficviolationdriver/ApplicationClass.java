package com.trafficviolationdriver;

import android.content.Context;
import android.os.StrictMode;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.FirebaseApp;
import com.trafficviolationdriver.util.AppConstants;
import com.trafficviolationdriver.util.SessionManager;
import com.trafficviolationdriver.util.Utils;

import io.fabric.sdk.android.Fabric;

public class ApplicationClass extends MultiDexApplication implements LifecycleObserver {

    private static ApplicationClass context;
    public static boolean isAppRunning = false;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;

        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

        Fabric.with(this, new Crashlytics());
        FirebaseApp.initializeApp(this);

        SessionManager session = new SessionManager(context);
        Utils.setLocale(this, session.getDataByKey(SessionManager.KEY_LANGUAGE, AppConstants.EN));

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    public static Context getAppContext() {
        return context;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onAppBackgrounded() {
        //App in background
        isAppRunning = true;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onAppForegrounded() {
        // App in foreground
        isAppRunning = true;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onAppDestroyed() {
        // App is destroy
        isAppRunning = false;
    }
}
