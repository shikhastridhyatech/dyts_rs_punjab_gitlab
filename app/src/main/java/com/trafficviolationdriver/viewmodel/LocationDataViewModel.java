package com.trafficviolationdriver.viewmodel;

import android.app.Application;

import com.trafficviolationdriver.db.AppDatabase;
import com.trafficviolationdriver.util.AppConstants;
import com.trafficviolationdriver.util.Logger;
import com.trafficviolationdriver.util.SessionManager;
import com.trafficviolationdriver.worker.LocationDataWorker;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

public class LocationDataViewModel extends AndroidViewModel {

    private WorkManager mWorkManager;
    private AppDatabase appDatabase;
    private MutableLiveData<UUID> uuidLiveData = new MutableLiveData<>();

    public LocationDataViewModel(@NonNull Application application) {
        super(application);
        mWorkManager = WorkManager.getInstance();

        appDatabase = AppDatabase.getAppDatabase(this.getApplication());
    }

    /*public void setStatusToPresentForFirstDay() {
        appDatabase.daysDao().getVideoById(1, DayStatusEnum.PRESENT.getCode());
    }*/

    public UUID setDayIncrementWorker() {

        PeriodicWorkRequest.Builder dayWorkBuilder =
                new PeriodicWorkRequest.Builder(LocationDataWorker.class, 15, TimeUnit.MINUTES, 5,
                        TimeUnit.MINUTES);

        // Add Tag to workBuilder
        dayWorkBuilder.addTag(AppConstants.LOCATION_DATA_UPLOAD_WORK_NAME);
        // Create the actual work object:
        PeriodicWorkRequest dayWork = dayWorkBuilder.build();
        // Then enqueue the recurring task:
        mWorkManager.enqueue(dayWork);

        // Set UUID in session for future use
        UUID workerId = dayWork.getId();
        SessionManager session = new SessionManager(this.getApplication());
        session.storeDataByKey(SessionManager.KEY_WORKER_UUID_LOCATION, workerId.toString());
        Logger.e("Save Worker UUID", workerId.toString());

        return workerId;
    }

    /**
     * Cancel work using the work's unique id
     */
    public void cancelWorkByUUID() {
        SessionManager session = new SessionManager(this.getApplication());
        String workerUuid = session.getDataByKey(SessionManager.KEY_WORKER_UUID_LOCATION);
        Logger.e("Canceling Worker UUID", workerUuid);
        if (workerUuid != null && workerUuid.length() > 0) {
            mWorkManager.cancelWorkById(UUID.fromString(workerUuid));
            mWorkManager.pruneWork();
        }
        session.storeDataByKey(SessionManager.KEY_WORKER_UUID_LOCATION, "");
    }

    /**
     * Cancel work using the work's unique name
     */
    void cancelWorkByTag(String workerUdi) {
        mWorkManager.cancelAllWorkByTag(AppConstants.LOCATION_DATA_UPLOAD_WORK_NAME);
    }
}
