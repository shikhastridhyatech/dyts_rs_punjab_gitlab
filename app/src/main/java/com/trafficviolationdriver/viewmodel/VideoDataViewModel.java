package com.trafficviolationdriver.viewmodel;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.trafficviolationdriver.db.AppDatabase;
import com.trafficviolationdriver.db.entity.VideoTableModel;
import com.trafficviolationdriver.util.SessionManager;

import java.util.List;

public class VideoDataViewModel extends AndroidViewModel {

    private LiveData<List<VideoTableModel>> myVideoList;
    private AppDatabase appDatabase;

    public VideoDataViewModel(@NonNull Application application) {
        super(application);

        SessionManager session = new SessionManager(this.getApplication());
        appDatabase = AppDatabase.getAppDatabase(this.getApplication());
        myVideoList = appDatabase.videoDao().getAllRecordedVideos(session.getUserDetail().getUserId());
    }

    public LiveData<List<VideoTableModel>> getVideoList() {
        return myVideoList;
    }

    public void getVideoById(int dayId) {
        new updateStatusAsyncTask(appDatabase, dayId).execute();
    }

    private static class updateStatusAsyncTask extends AsyncTask<Void, Void, Void> {

        private AppDatabase db;
        private int videoId;

        updateStatusAsyncTask(AppDatabase appDatabase, int videoId) {
            db = appDatabase;
            this.videoId = videoId;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            db.videoDao().getVideoData(videoId);
            return null;
        }
    }

    public VideoTableModel getIngredientsOfShoppingList(int videoId) {
        return appDatabase.videoDao().getVideoData(videoId);
    }

}
