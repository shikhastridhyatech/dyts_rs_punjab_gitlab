package com.trafficviolationdriver.service;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.media.CamcorderProfile;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.db.AppDatabase;
import com.trafficviolationdriver.db.entity.DriverLocationTableModel;
import com.trafficviolationdriver.db.entity.VideoTableModel;
import com.trafficviolationdriver.dialogs.MyServiceAlertDialog;
import com.trafficviolationdriver.dialogs.VideoEncodeDialog;
import com.trafficviolationdriver.ui.videocapture.VideoCaptureActivity;
import com.trafficviolationdriver.ui.videocapture.VideoListingActivity;
import com.trafficviolationdriver.util.AppConstants;
import com.trafficviolationdriver.util.FileUtil;
import com.trafficviolationdriver.util.LocationUtil;
import com.trafficviolationdriver.util.Logger;
import com.trafficviolationdriver.util.SessionManager;
import com.trafficviolationdriver.util.TimeStamp;
import com.trafficviolationdriver.util.Utils;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class VideoCaptureService extends Service {

	private static final String TAG = VideoCaptureService.class.getSimpleName();
	private final static String FOREGROUND_CHANNEL_ID = "foreground_channel_id";
	private NotificationManager mNotificationManager;
	private IBinder mBinder = new MyBinder();
	public static Camera mServiceCamera;
	private boolean mRecordingStatus, recordingStarted, isBounded;
	private MediaRecorder mMediaRecorder;
	private File originalRecordedFile;
	public SessionManager session;
	private RemoteViews remoteViews;
	private long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L, recordingQuotaMillis;
	private Handler handler;
	private int seconds, minutes, hours;
	private String timerText = "";
	int videoId;
	/*private SensorManager sensorMan;
	private Sensor gyroscopeSensor;*/
	MediaPlayer mediaPlayer;

	/**
	 * Used to check whether the bound activity has really gone away and not unbound as part of an
	 * orientation change. We create a foreground service notification only if the former takes
	 * place.
	 */
	/////////////////////////////private boolean mChangingConfiguration = false;


	public static final String PACKAGE_NAME = "com.trafficviolationdriver";

	/**
	 * The name of the channel for notifications.
	 */
	public static final String CHANNEL_ID = "default_notification_channel";

	public static final String ACTION_BROADCAST = PACKAGE_NAME + ".broadcast";

	public static final String EXTRA_LOCATION = PACKAGE_NAME + ".location";
	public static final String EXTRA_STARTED_FROM_NOTIFICATION = PACKAGE_NAME + ".started_from_notification";

	/**
	 * The desired interval for location updates. Inexact. Updates may be more or less frequent.
	 */
	private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

	/**
	 * The fastest rate for active location updates. Updates will never be more frequent
	 * than this value.
	 */
	private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

	/**
	 * The identifier for the notification displayed for the foreground service.
	 */
	private static final int NOTIFICATION_ID = 1028450;

	/**
	 * Contains parameters used by {@link com.google.android.gms.location.FusedLocationProviderApi}.
	 */
	private LocationRequest mLocationRequest;

	/**
	 * Provides access to the Fused Location Provider API.
	 */
	private FusedLocationProviderClient mFusedLocationClient;

	/**
	 * Callback for changes in location.
	 */
	private LocationCallback mLocationCallback;

	private Handler mServiceHandler;

	/**
	 * The current location.
	 */
	private Location mLocation;

	String channelId = "", channelName = "", channelDescription = "";
	private int notificationPriority;

	DriverLocationTableModel mLocationModel;
	private AppDatabase appDatabase;

	public VideoCaptureService() {
	}


	@Override
	public void onCreate() {
		super.onCreate();

		session = new SessionManager(this);
		handler = new Handler();
		appDatabase = AppDatabase.getAppDatabase(this);
		if (appDatabase.configDao() != null && appDatabase.configDao().getConfigData() != null) {
			recordingQuotaMillis = appDatabase.configDao().getConfigData().getRemainingMinutes();
		}
		timerText = "" + String.format(Locale.ENGLISH, "%02d", hours) + ":" + String.format(Locale.ENGLISH, "%02d", minutes) + ":" + String.format(Locale.ENGLISH, "%02d", seconds);


		mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.beep);
		mediaPlayer.setLooping(false);

		/*sensorMan = (SensorManager) getSystemService(SENSOR_SERVICE);
		gyroscopeSensor = sensorMan.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);*/


		mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		// Android O requires a Notification Channel.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			CharSequence name = getString(R.string.app_name);
			// Create the channel for the notification
			NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);

			// Set the Notification Channel for the Notification Manager.
			mNotificationManager.createNotificationChannel(mChannel);
		}

		mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

		mLocationCallback = new LocationCallback() {
			@Override
			public void onLocationResult(LocationResult locationResult) {
				super.onLocationResult(locationResult);
				onNewLocation(locationResult.getLastLocation());
			}
		};

		createLocationRequest();
		getLastLocation();

		HandlerThread handlerThread = new HandlerThread(TAG);
		handlerThread.start();
		mServiceHandler = new Handler(handlerThread.getLooper());

		mLocationModel = new DriverLocationTableModel();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		try {
			Log.e(TAG, "in onStartCommand intent.getAction() = " + intent.getAction());
			if (intent == null) {
				stopRecording();
				stopService();
				return START_NOT_STICKY;
			}

			boolean startedFromNotification = intent.getBooleanExtra(EXTRA_STARTED_FROM_NOTIFICATION, false);

			// We got here because the user decided to remove location updates from the notification.
			if (startedFromNotification) {
				removeLocationUpdates();
				stopSelf();
			}

			// Tells the system to not try to recreate the service after it has been killed.
			switch (intent.getAction()) {
				case AppConstants.ACTION.START_ACTION:
					Log.e(TAG, "Received user starts foreground intent");
					isBounded = true;
					connect();
					break;

				case "START_RECORD":
					isBounded = true;
					startForeground(AppConstants.NOTIFICATION_ID_FOREGROUND_SERVICE, prepareNotification());
					if (isBounded) {
						LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(VideoCaptureActivity.RECORDING_START_STOP).putExtra("ServiceState", true));
					} else {
						initMediaRecorder();
						startRecording();
					}
					break;

				case "STOP_RECORD":
					Log.e(TAG, "Received STOP_RECORD event");
					if (isBounded) {
						LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(VideoCaptureActivity.RECORDING_START_STOP).putExtra("ServiceState", false));
					} else {
						saveToDBAndStopRecording(false, false);
					}
					Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
					sendBroadcast(it);
					isBounded = false;
					break;

				default:
					isBounded = false;
					stopRecording();
					stopService();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return START_STICKY;
	}

	@Override
	public void onTaskRemoved(Intent rootIntent) {
		super.onTaskRemoved(rootIntent);
	}

	private void connect() {
		initMediaRecorder();
		startForeground(AppConstants.NOTIFICATION_ID_FOREGROUND_SERVICE, prepareNotification());
	}

	public void startForegroundService() {
		startForeground(AppConstants.NOTIFICATION_ID_FOREGROUND_SERVICE, prepareNotification());
	}

	@Override
	public IBinder onBind(Intent intent) {
		Log.e(TAG, "in onBind");
		if (mBinder == null) {
			mBinder = new MyBinder();
		}
		isBounded = true;
	/////////////////	mChangingConfiguration = false;
		return mBinder;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		Log.e(TAG, "Last client unbound from service");

		// Called when the last client (MainActivity in case of this sample) unbinds from this
		// service. If this method is called due to a configuration change in MainActivity, we
		// do nothing. Otherwise, we make this service a foreground service.
	/* ////////////////////////////////////	if (!mChangingConfiguration) {
			// Log.e(TAG, "Starting foreground service");
          *//*  if(recordingStarted)
            startForeground(AppConstants.NOTIFICATION_ID_FOREGROUND_SERVICE, prepareNotification());*//*
		}*/
		isBounded = false;
		return true; // Ensures onRebind() is called when a client re-binds.
	}

	@Override
	public void onRebind(Intent intent) {
		Log.e(TAG, "in onRebind()");
	////////////////////////////////////////////////	mChangingConfiguration = false;
		isBounded = true;
		super.onRebind(intent);
	}

	/*
	//////////////////////////////
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mChangingConfiguration = true;
	}
*/

	public void initMediaRecorder() {
		try {
			mServiceCamera = Camera.open(findBackFacingCamera());

			mMediaRecorder = new MediaRecorder();
			////
			mServiceCamera.setDisplayOrientation(90);
		//	mServiceCamera.setDisplayOrientation(0);

			mServiceCamera.startPreview();
			if (isBounded) mServiceCamera.setPreviewDisplay(VideoCaptureActivity.mHolder);

			Camera.Parameters params = mServiceCamera.getParameters();
			///////////
			params.setRotation(180);

			List<String> focusModes = params.getSupportedFocusModes();

			String CAF_PICTURE = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE, CAF_VIDEO = Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO, supportedMode = focusModes.contains(CAF_PICTURE) ? CAF_PICTURE : focusModes.contains(CAF_VIDEO) ? CAF_VIDEO : "";

			if (!supportedMode.equals("")) {
				params.setFocusMode(supportedMode);
				mServiceCamera.setParameters(params);
			}
			mServiceCamera.setParameters(params);

			mServiceCamera.unlock();
			mMediaRecorder.setCamera(mServiceCamera);
//////////////////
			mMediaRecorder.setOrientationHint(90);
		//mMediaRecorder.setOrientationHint(0);

			mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

			mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);

			CamcorderProfile cpHigh = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
			cpHigh.fileFormat = MediaRecorder.OutputFormat.MPEG_4;


			mMediaRecorder.setProfile(cpHigh);

			if (isBounded)
				mMediaRecorder.setPreviewDisplay(VideoCaptureActivity.mHolder.getSurface());

			originalRecordedFile = Utils.createNewFile(this, true, true, false, session.getUserDetail().getUserId());
/*
            boolean r = newFile.setReadable(false);
            boolean e = newFile.setExecutable(false);
            boolean w = newFile.setWritable(false);
            Log.e("VideoCaptureService", "setReadable ==> " + r);
            Log.e("VideoCaptureService", "setExecutable ==> " + e);
            Log.e("VideoCaptureService", "setWritable ==> " + w);*/

			if (originalRecordedFile != null) {
				Log.e("FilePath", originalRecordedFile.getAbsolutePath());
				mMediaRecorder.setOutputFile(originalRecordedFile.getAbsolutePath());
			}

			mMediaRecorder.setOnErrorListener(new MediaRecorder.OnErrorListener() {
				@Override
				public void onError(MediaRecorder mr, int what, int extra) {
					Log.e("MediaRecorder Error", "Error: " + what + ", " + extra);
				}
			});
			mMediaRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
				@Override
				public void onInfo(MediaRecorder mr, int what, int extra) {
					Log.e("MediaRecorder Info", "Error: " + what + ", " + extra);
				}
			});
			mMediaRecorder.prepare();

		} catch (IllegalStateException e) {
			handleException(e);
		} catch (IOException e) {
			handleException(e);
		} catch (RuntimeException e) {
			handleException(e);
		} catch (Exception e) {
			handleException(e);
		}
	}

	private void handleException(Exception e) {
		Log.e(TAG, e.getMessage());
		e.printStackTrace();
		if (originalRecordedFile != null) {
			originalRecordedFile.delete();
			originalRecordedFile = null;
		}

		if (isBounded) {
			LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(VideoCaptureActivity.RECORDING_START_STOP).putExtra("FinishActivity", true));

		}
		stopRecording();

		Intent intent = new Intent(this, MyServiceAlertDialog.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.putExtra("Message", getString(R.string.something_went_wrong));
		startActivity(intent);

		stopService();
	}

	public void startRecording() {
		try {
			if (mMediaRecorder != null) {
				requestLocationUpdates();
				mMediaRecorder.start();
				recordingStarted = true;

				/*if (gyroscopeSensor != null) {
					Logger.e("Your device has gyroscope Sensor");
					sensorMan.registerListener(gyroListener, gyroscopeSensor,
							SensorManager.SENSOR_DELAY_UI);
				} else {
					Logger.e("Your device does not support gyroscope Sensor");
				}*/


				if (mNotificationManager != null && serviceIsRunningInForeground(this)) {
					mNotificationManager.notify(AppConstants.NOTIFICATION_ID_FOREGROUND_SERVICE, prepareNotification());
					Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
					sendBroadcast(it);
				}
               /* if (remoteViews != null) {
                    remoteViews.setTextViewText+(R.id.tvState, getString(R.string.video_recording_started));
                }
*/
				isBounded = true;
				mRecordingStatus = true;
				StartTime = SystemClock.uptimeMillis();
				handler.postDelayed(runnable, 0);

				new CountDownTimer(recordingQuotaMillis, 1000) {

					public void onTick(long millisUntilFinished) {
					}

					public void onFinish() {
						saveToDBAndStopRecording(false, false);
						if (isBounded) {
							LocalBroadcastManager
									.getInstance(VideoCaptureService.this)
									.sendBroadcast(
											new Intent(VideoCaptureActivity.RECORDING_START_STOP)
													.putExtra("FinishActivity", true));
						}
						Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
						sendBroadcast(it);
						isBounded = false;
					}
				}.start();
			}
		} catch (IllegalStateException e) {
			handleException(e);
		} catch (Exception e) {
			handleException(e);
		}
	}

	public Runnable runnable = new Runnable() {

		public void run() {

			MillisecondTime = SystemClock.uptimeMillis() - StartTime;

			UpdateTime = TimeBuff + MillisecondTime;

			seconds = (int) (UpdateTime / 1000);

			minutes = seconds / 60;
			if (minutes >= 60) {
				hours = minutes / 60;
				minutes %= 60;
			}

			seconds = seconds % 60;

            /*double elapsedSeconds = (int) (MillisecondTime / 1000);

            hours = (int) (elapsedSeconds / 3600);
            minutes = (int) ((elapsedSeconds % 3600) / 60);
            seconds = (int) (elapsedSeconds % 60);*/



           /* seconds = (int) (MillisecondTime / 1000) % 60 ;
            minutes = (int) ((MillisecondTime / (1000*60)) % 60);
            hours = (int) ((MillisecondTime / (1000*60*60)) % 24);*/

			//   MilliSeconds = (int) (UpdateTime % 1000);

			timerText = "" + String.format(Locale.ENGLISH, "%02d", hours) + ":" + String.format(Locale.ENGLISH, "%02d", minutes) + ":" + String.format(Locale.ENGLISH, "%02d", seconds);
			if (isBounded)
				LocalBroadcastManager.getInstance(VideoCaptureService.this).sendBroadcast(new Intent(VideoCaptureActivity.RECORDING_START_STOP).putExtra("UpdateTimer", timerText));

			if (mNotificationManager != null) {
				mNotificationManager.notify(AppConstants.NOTIFICATION_ID_FOREGROUND_SERVICE, prepareNotification());
			}
			handler.postDelayed(this, 0);
		}
	};

	public SensorEventListener gyroListener = new SensorEventListener() {
		public void onAccuracyChanged(Sensor sensor, int acc) {
		}

		public void onSensorChanged(SensorEvent event) {
			float x = event.values[0];
			float y = event.values[1];
			float z = event.values[2];

           /* Logger.e("X : " + (int) x + " rad/s");
            Logger.e("Y : " + (int) y + " rad/s");
            Logger.e("Z : " + (int) z + " rad/s");*/

       /*     if (event.values[2] > 0.5f) { // anticlockwise
                getWindow().getDecorView().setBackgroundColor(Color.BLUE);
                Logger.e("anticlockwise rotate");
            } else if (event.values[2] < -0.5f) { // clockwise
                getWindow().getDecorView().setBackgroundColor(Color.YELLOW);
                Logger.e("clockwise rotate");

            }*/
			float[] rotationMatrix = new float[16];
			SensorManager.getRotationMatrixFromVector(
					rotationMatrix, event.values);

			// Remap coordinate system
			float[] remappedRotationMatrix = new float[16];
			SensorManager.remapCoordinateSystem(rotationMatrix,
					SensorManager.AXIS_X,
					SensorManager.AXIS_Z,
					remappedRotationMatrix);

			// Convert to orientations
			float[] orientations = new float[3];
			SensorManager.getOrientation(remappedRotationMatrix, orientations);

			for (int i = 0; i < 3; i++) {
				orientations[i] = (float) (Math.toDegrees(orientations[i]));
			}

			if (orientations[2] > 25) {
				try {
					mediaPlayer.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
				Logger.e("clockwise value", String.valueOf(orientations[2]));
			} else if (orientations[2] < -25) {
				try {
					mediaPlayer.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
				Logger.e("anticlockwise value", String.valueOf(orientations[2]));
			} else if (Math.abs(orientations[2]) < 10) {
				try {
					mediaPlayer.pause();
//                    mediaPlayer.seekTo(0);
				} catch (Exception e) {
					e.printStackTrace();
				}
				Logger.e("portrait value", String.valueOf(orientations[2]));
			}

			if (orientations[1] > 30 && orientations[1] < 60) {
				try {
					mediaPlayer.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
				Logger.e("clockwise value", String.valueOf(orientations[1]));
			} else if (orientations[1] < -10) {
				try {
					mediaPlayer.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
				Logger.e("anticlockwise value", String.valueOf(orientations[1]));
			} else if (Math.abs(orientations[1]) < 10) {
				try {
					mediaPlayer.pause();
//                    mediaPlayer.seekTo(0);
				} catch (Exception e) {
					e.printStackTrace();
				}
				Logger.e("portrait value", String.valueOf(orientations[1]));
			}

            /*if(orientations[0] > 45) {
                Logger.e("clockwise value", String.valueOf(orientations[0]));
                getWindow().getDecorView().setBackgroundColor(Color.YELLOW);
            } else if(orientations[0] < -45) {
                Logger.e("anticlockwise value", String.valueOf(orientations[0]));
                getWindow().getDecorView().setBackgroundColor(Color.BLUE);
            } else if(Math.abs(orientations[0]) < 10) {
                Logger.e("portrait value", String.valueOf(orientations[0]));
                getWindow().getDecorView().setBackgroundColor(Color.WHITE);
            }*/

			// TYPE_GYROSCOPE
            /*if (event.values[2] > 0.5f) { // anticlockwise
                Logger.e("anticlockwise value", String.valueOf(event.values[2]));
                getWindow().getDecorView().setBackgroundColor(Color.BLUE);
            } else if (event.values[2] < -0.5f) { // clockwise
                Logger.e("clockwise value", String.valueOf(event.values[2]));
                getWindow().getDecorView().setBackgroundColor(Color.YELLOW);
            } else if (event.values[2] < -0.0f) { // portrait
                Logger.e("portrait value", String.valueOf(event.values[2]));
                getWindow().getDecorView().setBackgroundColor(Color.WHITE);
            }
            if (event.values[1] > 0.5f) { // anticlockwise
                Logger.e("anticlockwise value", String.valueOf(event.values[1]));
                getWindow().getDecorView().setBackgroundColor(Color.BLUE);
            } else if (event.values[1] < -0.5f) { // clockwise
                Logger.e("clockwise value", String.valueOf(event.values[1]));
                getWindow().getDecorView().setBackgroundColor(Color.YELLOW);
            } else if (event.values[1] < -0.0f) { // portrait
                Logger.e("portrait value", String.valueOf(event.values[1]));
                getWindow().getDecorView().setBackgroundColor(Color.WHITE);
            }
            if (event.values[0] > 0.5f) { // anticlockwise
                Logger.e("anticlockwise value", String.valueOf(event.values[0]));
                getWindow().getDecorView().setBackgroundColor(Color.BLUE);
            } else if (event.values[0] < -0.5f) { // clockwise
                Logger.e("clockwise value", String.valueOf(event.values[0]));
                getWindow().getDecorView().setBackgroundColor(Color.YELLOW);
            } else if (event.values[0] < -0.0f) { // portrait
                Logger.e("portrait value", String.valueOf(event.values[0]));
                getWindow().getDecorView().setBackgroundColor(Color.WHITE);
            }*/
		}
	};

	private Notification prepareNotification() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && mNotificationManager.getNotificationChannel(FOREGROUND_CHANNEL_ID) == null) {
			CharSequence name = getString(R.string.text_name_notification);
			int importance = NotificationManager.IMPORTANCE_DEFAULT;
			NotificationChannel channel = new NotificationChannel(FOREGROUND_CHANNEL_ID, name, importance);
			channel.enableVibration(false);
			mNotificationManager.createNotificationChannel(channel);
		}

		remoteViews = new RemoteViews(getPackageName(), R.layout.notification);

		Intent stopIntent = new Intent(this, VideoCaptureService.class);
		stopIntent.setAction("STOP_RECORD");
		PendingIntent pendingStopIntent = PendingIntent.getService(this, 0, stopIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		Intent startIntent = new Intent(this, VideoCaptureService.class);
		startIntent.setAction("START_RECORD");
		PendingIntent pendingStartIntent = PendingIntent.getService(this, 0, startIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		remoteViews.setViewVisibility(R.id.tvVideoTimer, View.VISIBLE);

		if (recordingStarted) {
			remoteViews.setViewVisibility(R.id.ivStart, View.GONE);
			remoteViews.setViewVisibility(R.id.ivStop, View.VISIBLE);
			remoteViews.setTextViewText(R.id.tvState, getString(R.string.video_recording_started));
		} else {
			remoteViews.setViewVisibility(R.id.ivStart, View.VISIBLE);
			remoteViews.setViewVisibility(R.id.ivStop, View.GONE);
			remoteViews.setTextViewText(R.id.tvState, getString(R.string.start_recording));
		}
		remoteViews.setOnClickPendingIntent(R.id.ivStart, pendingStartIntent);
		remoteViews.setOnClickPendingIntent(R.id.ivStop, pendingStopIntent);

		if (remoteViews != null) {
			remoteViews.setTextViewText(R.id.tvVideoTimer, timerText);
		}

		NotificationCompat.Builder notificationBuilder;
		notificationBuilder = new NotificationCompat.Builder(this, FOREGROUND_CHANNEL_ID);

		notificationBuilder
				.setContent(remoteViews)
				.setSmallIcon(R.mipmap.ic_launcher)
				.setCategory(NotificationCompat.CATEGORY_SERVICE)
				.setOnlyAlertOnce(true)
				.setOngoing(true)
				.setAutoCancel(true);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			notificationBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
		}
		return notificationBuilder.build();
	}

	private int findBackFacingCamera() {
		try {
			int numberOfCameras = Camera.getNumberOfCameras();
			for (int i = 0; i < numberOfCameras; i++) {
				Camera.CameraInfo info = new Camera.CameraInfo();
				Camera.getCameraInfo(i, info);
				if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
					return i;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	public class MyBinder extends Binder {
		public VideoCaptureService getService() {
			return VideoCaptureService.this;
		}
	}

	/**
	 * Makes a request for location updates. Note that in this sample we merely log the
	 *
	 */
	public void requestLocationUpdates() {
		Log.e(TAG, "Requesting location updates");
		LocationUtil.setRequestingLocationUpdates(this, true);
		//startService(new Intent(getApplicationContext(), LocationUpdatesService.class));
		try {
			mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
		} catch (SecurityException unlikely) {
			LocationUtil.setRequestingLocationUpdates(this, false);
			Log.e(TAG, "Lost location permission. Could not request updates. " + unlikely);
		}
	}

	/**
	 * Removes location updates. Note that in this sample we merely log the

	 */
	public void removeLocationUpdates() {
		Log.e(TAG, "Removing location updates");
		try {
			mFusedLocationClient.removeLocationUpdates(mLocationCallback);
			LocationUtil.setRequestingLocationUpdates(this, false);
			stopSelf();
		} catch (SecurityException unlikely) {
			LocationUtil.setRequestingLocationUpdates(this, true);
			Log.e(TAG, "Lost location permission. Could not remove updates. " + unlikely);
		}
	}

	private void getLastLocation() {
		try {
			mFusedLocationClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
				@Override
				public void onComplete(@NonNull Task<Location> task) {
					if (task.isSuccessful() && task.getResult() != null) {
						mLocation = task.getResult();
					} else {
						Log.e(TAG, "Failed to get location.");
					}
				}
			});
		} catch (SecurityException unlikely) {
			Log.e(TAG, "Lost location permission." + unlikely);
		}
	}

	private void onNewLocation(Location location) {
		Log.e(TAG, "New location: " + location);

		mLocation = location;

		mLocationModel = new DriverLocationTableModel(-1, String.valueOf(session.getUserDetail().getUserId()), mLocation.getLatitude(), mLocation.getLongitude(), new Date().getTime());

		// Insert entry into local database
		appDatabase.locationDao().insert(mLocationModel);
	}

	/**
	 * Sets the location request parameters.
	 */
	private void createLocationRequest() {
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
		mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	}

	public boolean serviceIsRunningInForeground(Context context) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (getClass().getName().equals(service.service.getClassName())) {
				if (service.foreground) {
					return true;
				}
			}
		}
		return false;
	}

	public void saveToDBAndStopRecording(boolean ifShow, boolean ifNavigate) {
		try {
			if (mRecordingStatus) {

				recordingStarted = false;

				handler.removeCallbacks(runnable);
				removeLocationUpdates();

				MillisecondTime = 0L;
				StartTime = 0L;
				TimeBuff = 0L;
				UpdateTime = 0L;
				seconds = 0;
				minutes = 0;

				if (mNotificationManager != null && serviceIsRunningInForeground(this)) {
					mNotificationManager.notify(AppConstants.NOTIFICATION_ID_FOREGROUND_SERVICE, prepareNotification());
					Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
					sendBroadcast(it);
				}
			}

			stopRecording();
			if (ifNavigate) {
				Intent intent = new Intent(this, VideoListingActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
			if (ifShow) {
				Intent intent = new Intent(this, MyServiceAlertDialog.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.putExtra("Message", getString(R.string.video_saved_successfully));
				startActivity(intent);
			}

			stopService();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void recordNextVideo() {
		try {
			saveToDBAndStopRecording(false, false);
			initMediaRecorder();
			startRecording();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveVideoFileToDB() {
		Log.e("VideoCaptureService", "saveVideoFileToDB called");
		try {
			long endTimeMillis = System.currentTimeMillis();
			long duration = getVideoDuration();

			String videoName = TimeStamp.getVideoStartEndTime(endTimeMillis - duration, endTimeMillis);
			VideoTableModel videoTableModel = new VideoTableModel();
			videoTableModel.setDriverId(String.valueOf(session.getUserDetail().getUserId()));
			videoTableModel.setEndTimestamp(endTimeMillis);
			videoTableModel.setDuration(duration);
			videoTableModel.setVideoName(videoName);

			File newFile = null, thumbnailFile = null;
			if (originalRecordedFile != null) {
				thumbnailFile = new File(Utils.getVideoFolder(this), videoName.concat("_thumb"));
				boolean isThumbImageCreated = FileUtil.storeImage(originalRecordedFile.getAbsolutePath(), thumbnailFile);
				Logger.e("isThumbImageCreated: ", String.valueOf(isThumbImageCreated));
				videoTableModel.setVideoThumbnail(isThumbImageCreated ? thumbnailFile.getAbsolutePath() : "");
				newFile = new File(Utils.getVideoFolder(this), videoName.concat("_temp"));
				boolean b = originalRecordedFile.renameTo(newFile);
				if (b)
					videoTableModel.setVideoPath(newFile.getAbsolutePath());
				else
					videoTableModel.setVideoPath("");
			} else
				videoTableModel.setVideoPath("");

			videoId = (int) appDatabase.videoDao().insert(videoTableModel);
			appDatabase.locationDao().updateLocationVideoId(videoId);

			Log.e("VideoCaptureService", "duration in minutes ==> " + TimeStamp.getMinutesFromMilliseconds(duration));

			if (appDatabase.configDao() != null && appDatabase.configDao().getConfigData() != null) {
				recordingQuotaMillis -= duration;
				Log.e("VideoCaptureService", "now recordingQuotaMillis in minutes ==> " + TimeStamp.getMinutesFromMilliseconds(recordingQuotaMillis));
				appDatabase.configDao().updateRemainingQuota(session.getUserDetail().getUserId(), recordingQuotaMillis);
			}

			Log.e("DB", "Video Saved to DB");
			if (newFile != null && newFile.exists()) {
				Log.e("newFile", "newFile");

				File encryptedFilePath = new File(Utils.getVideoFolder(this), videoName);


				//1. Start Foreground service to do Encryption
				//Start Foreground Service
				Intent mIntent = new Intent(this, VideoEncryptionService.class);
				mIntent.setAction(AppConstants.ACTION.START_ACTION);
				mIntent.putExtra(VideoEncryptionService.KEY_INPUT_PATH, newFile.getAbsolutePath());
				mIntent.putExtra(VideoEncryptionService.KEY_OUTPUT_PATH, encryptedFilePath.getAbsolutePath());
				mIntent.putExtra(VideoEncryptionService.KEY_VIDEO_ID, videoId);

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
					startForegroundService(mIntent);
				} else {
					startService(mIntent);
				}
//                startService(mIntent);
/*
				//2. Open new Activity and do Encryption process
				Intent intent = new Intent(this, VideoEncodeDialog.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.putExtra(VideoEncryptionService.KEY_INPUT_PATH, newFile.getAbsolutePath());
				intent.putExtra(VideoEncryptionService.KEY_OUTPUT_PATH, encryptedFilePath.getAbsolutePath());
				intent.putExtra(VideoEncryptionService.KEY_VIDEO_ID, videoId);
				startActivity(intent);*/
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private long getVideoDuration() {
		Log.e("VideoCaptureService", "getVideoDuration called");
		try {
			MediaMetadataRetriever retriever = new MediaMetadataRetriever();
			retriever.setDataSource(this, Uri.fromFile(new File(originalRecordedFile.getAbsolutePath())));
			String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
			Logger.e("time: ", "=> " + time);
			long timeInMillisec = Long.parseLong(time);
			Log.e("mp duration", "" + timeInMillisec);

			@SuppressLint("DefaultLocale") String timeString = String.format("%d min, %d sec",
					TimeUnit.MILLISECONDS.toMinutes(timeInMillisec),
					TimeUnit.MILLISECONDS.toSeconds(timeInMillisec) -
							TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeInMillisec)), Locale.ENGLISH);
			Logger.e("timeString: ", "=> " + timeString);
			retriever.release();
			return timeInMillisec;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public void stopRecording() {
		try {
			Log.e("in", "Stop Recording");
			if (mMediaRecorder != null) {
				if (mRecordingStatus) {
					mMediaRecorder.stop();
					mMediaRecorder.reset();
					/*if(gyroscopeSensor != null) {
						sensorMan.unregisterListener(gyroListener);
					}*/
					saveVideoFileToDB();
				}
				mMediaRecorder.release();
				mMediaRecorder = null;
				mRecordingStatus = false;
			}
			releaseCameraResource();

		} catch (IllegalStateException e) {
			e.printStackTrace();
			releaseCameraResource();
		} catch (RuntimeException e) {
			e.printStackTrace();
			releaseCameraResource();
		} catch (Exception e) {
			e.printStackTrace();
			releaseCameraResource();
		} finally {
			try {
				if (mMediaRecorder != null) {
					mMediaRecorder.release();
					mMediaRecorder = null;
				}
			} catch (Exception e) {
				e.printStackTrace();
				releaseCameraResource();
			}
		}
	}

	public void releaseCameraResource() {
		Log.e("VideoCaptureService", "releaseCameraResource called");
		try {
			if (mServiceCamera != null) {
				mServiceCamera.stopPreview();
				// mServiceCamera.unlock();
				mServiceCamera.release();
				mServiceCamera = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void stopService() {
		Log.e("VideoCaptureService", "stopService called");
		try {
			// stopForeground(true);
			this.stopForeground(false);
			mNotificationManager.cancel(AppConstants.NOTIFICATION_ID_FOREGROUND_SERVICE);
			// encryptRecordedFileAndSaveToDB();
			stopSelf();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroy() {
		Log.e("VideoCaptureService", "OnDestroy called");
		try {
			mServiceHandler.removeCallbacksAndMessages(null);
			stopRecording();
			stopService();
			super.onDestroy();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}