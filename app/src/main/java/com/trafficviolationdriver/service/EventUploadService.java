package com.trafficviolationdriver.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.db.AppDatabase;
import com.trafficviolationdriver.db.dao.EventDao;
import com.trafficviolationdriver.db.entity.EventTableModel;
import com.trafficviolationdriver.model.EventDataModel;
import com.trafficviolationdriver.model.FileModel;
import com.trafficviolationdriver.ui.home.DashBoardActivity;
import com.trafficviolationdriver.util.AppConstants;
import com.trafficviolationdriver.util.Logger;
import com.trafficviolationdriver.util.SessionManager;
import com.trafficviolationdriver.util.TimeStamp;
import com.trafficviolationdriver.webservice.APIs;
import com.trafficviolationdriver.webservice.JSONCallbackMultipart;
import com.trafficviolationdriver.webservice.ProgressRequestBody;
import com.trafficviolationdriver.webservice.Retrofit;
import com.trafficviolationdriver.worker.EventUploadWorker;

import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.trafficviolationdriver.util.TimeStamp.DateFormatGTSUploadEvent;
import static com.trafficviolationdriver.util.Utils.deleteEventData;

/**
 * Created by Shikha Shah on 16-May-19
 */
public class EventUploadService extends Service implements ProgressRequestBody.UploadCallbacks {

	private static final String TAG = EventUploadService.class.getSimpleName();
	private final static String FOREGROUND_CHANNEL_ID = "create_event_channel_id";
	private final static String FOREGROUND_CHANNEL_ID_EVENT_FAILED = "event_failed_channel_id";

	public final static String BUNDLE_DATA_LOCAL_EVENT_MODEL = "local_event_model";

	public SessionManager session;
	private Context mContext;

	private NotificationManager mNotificationManager;
	private NotificationCompat.Builder notificationBuilder;
	int notificationID = 101, eventFailedNotificationID = 401, remainingEventCount = 0;
	private String contentText = "";
	private boolean progressBarVisibility = true;

	private EventDao eventDao;
	private RemoteViews remoteViews;
	private Notification mNotification;
	private EventTableModel mLocalEventModel;

	/**
	 * Used to check whether the bound activity has really gone away and not unbound as part of an
	 * orientation change. We create a foreground service notification only if the former takes
	 * place.
	 */
	private boolean isSetOngoing = true;
	private int progress;
	private Bundle mBundle;

	@Override
	public void onCreate() {
		super.onCreate();
		session = new SessionManager(this);
		mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		// Android O requires a Notification Channel.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			CharSequence name = getString(R.string.app_name);
			// Create the channel for the notification
			NotificationChannel mChannel = new NotificationChannel(FOREGROUND_CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
			NotificationChannel mChannelEventFailed = new NotificationChannel(FOREGROUND_CHANNEL_ID_EVENT_FAILED, name, NotificationManager.IMPORTANCE_DEFAULT);
			// Set the Notification Channel for the Notification Manager.
			mNotificationManager.createNotificationChannel(mChannel);
			mNotificationManager.createNotificationChannel(mChannelEventFailed);
		}
		mContext = this;
		contentText = getString(R.string.uploading_event_data);
		eventDao = AppDatabase.getAppDatabase(this).eventDao();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		try {
			mBundle = intent.getExtras();
			if (mBundle == null) {
				return START_NOT_STICKY;
			}

			switch (Objects.requireNonNull(intent.getAction())) {
				case AppConstants.ACTION.START_ACTION:
					progressBarVisibility = true;
					callEventUploadApi();
					break;

				case AppConstants.ACTION.STOP_ACTION:
					Logger.e(TAG, "in STOP_ACTION");
					stopSelf();
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return START_NOT_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	private Notification prepareNotification() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && mNotificationManager.getNotificationChannel(FOREGROUND_CHANNEL_ID) == null) {
			CharSequence name = getString(R.string.text_name_notification);
			int importance = NotificationManager.IMPORTANCE_LOW;
			NotificationChannel channel = new NotificationChannel(FOREGROUND_CHANNEL_ID, name, importance);
			channel.enableVibration(false);
			mNotificationManager.createNotificationChannel(channel);
		}

		Intent mIntent = new Intent(EventUploadService.this, EventUploadService.class);
		mIntent.setAction(AppConstants.ACTION.STOP_ACTION);
		PendingIntent mPendingIntent = PendingIntent.getService(EventUploadService.this, 0, mIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		remoteViews = new RemoteViews(getPackageName(), R.layout.notification_event_upload);

		remoteViews.setProgressBar(R.id.notifProgressBar, 100, progress, false);
		remoteViews.setTextViewText(R.id.tvState, contentText);
		remoteViews.setViewVisibility(R.id.notifProgressBar, progressBarVisibility ? View.VISIBLE : View.GONE);

		notificationBuilder = new NotificationCompat.Builder(this, FOREGROUND_CHANNEL_ID);
		notificationBuilder
				.setContent(remoteViews)
				.setSmallIcon(R.mipmap.ic_launcher)
				.setCategory(NotificationCompat.CATEGORY_SERVICE)
				.setDeleteIntent(mPendingIntent)
				.setOnlyAlertOnce(true)
				.setColor(ContextCompat.getColor(EventUploadService.this, R.color.colorPrimary))
				.setOngoing(isSetOngoing);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			notificationBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
		}
		mNotification = notificationBuilder.build();
		return mNotification;
	}

	@Override
	public void onProgressUpdate(int percentage) {
		Logger.e("Event Upload Progress: ", String.valueOf(percentage));
		try {
			if (notificationBuilder != null) {
				progress = percentage;
				progressBarVisibility = true;
				contentText = getString(R.string.uploading_event_data) + " " + progress + getString(R.string.percentage);

				remoteViews.setProgressBar(R.id.notifProgressBar, 100, progress, false);
				remoteViews.setTextViewText(R.id.tvState, contentText);

				mNotificationManager.notify(notificationID, mNotification);
			}
		} catch (Exception e) {
			e.printStackTrace();

			isSetOngoing = false;
			contentText = getString(R.string.failed_to_upload_event);
			progress = 0;
			notificationBuilder.setAutoCancel(true);
			remoteViews.setProgressBar(R.id.notifProgressBar, 100, progress, false);
			remoteViews.setTextViewText(R.id.tvState, contentText);
			mNotificationManager.notify(notificationID, mNotification);

			stopForeground(true);
			stopSelf();

			showEventFailedNotification(contentText);
		}
	}

	private void callEventUploadApi() {
		progress = 0;
		contentText = getString(R.string.uploading_event_data) + " " + progress + getString(R.string.percentage);
		isSetOngoing = true;
		startForeground(notificationID, prepareNotification());

		mLocalEventModel = mBundle.getParcelable(BUNDLE_DATA_LOCAL_EVENT_MODEL);

		assert mLocalEventModel != null;
		API_UploadEventDetail();
	}

	private void API_UploadEventDetail() {

		HashMap<String, String> params = new HashMap<>();
		params.put("userId", String.valueOf(session.getUserDetail().getUserId()));
		params.put("vehicleNo", mLocalEventModel.getVehicleNo());
		params.put("reasonId", String.valueOf(mLocalEventModel.getReasonId()));
		params.put("violationDateTime", TimeStamp.getDateFromTimestamp(mLocalEventModel.getDateTime(), DateFormatGTSUploadEvent));
		params.put("violationLatitude", String.valueOf(mLocalEventModel.getLatitude()));
		params.put("violationLongitude", String.valueOf(mLocalEventModel.getLongitude()));

		HashMap<FileModel, File> fileParams = new HashMap<>();
		FileModel mFileModel;

		// Set Captured Image
		File mFile = new File(mLocalEventModel.getImagePath());
		if (mFile.exists()) {
			Logger.e("Image File Exist: ", mLocalEventModel.getImagePath());
			mFileModel = new FileModel();
			mFileModel.setName(mFile.getName());
			mFileModel.setKey("violationImage");
			mFileModel.setPath(mLocalEventModel.getImagePath());
			mFileModel.setType(FileModel.MediaType.MEDIA_TYPE_IMAGE);
			fileParams.put(mFileModel, mFile.getAbsoluteFile());
		} else {
			Logger.e("Image File Not Exist: ", mLocalEventModel.getImagePath());
		}

		// Set Captured Video
		mFile = new File(mLocalEventModel.getCroppedVideoPath());
		if (mFile.exists()) {
			Logger.e("Video File Exist: ", mLocalEventModel.getCroppedVideoPath());
			mFileModel = new FileModel();
			mFileModel.setName(mFile.getName());
			mFileModel.setKey("violationVideo");
			mFileModel.setPath(mLocalEventModel.getCroppedVideoPath());
			mFileModel.setType(FileModel.MediaType.MEDIA_TYPE_VIDEO);
			fileParams.put(mFileModel, mFile.getAbsoluteFile());
		} else {
			Logger.e("Video File Not Exist: ", mLocalEventModel.getCroppedVideoPath());
		}

		// Set Number Plate Image
		mFile = new File(mLocalEventModel.getNumberplatePath());
		if (mFile.exists()) {
			Logger.e("NumberPlateImage File Exist: ", mLocalEventModel.getNumberplatePath());
			mFileModel = new FileModel();
			mFileModel.setName(mFile.getName());
			mFileModel.setKey("violationNumberPlateImage");
			mFileModel.setPath(mLocalEventModel.getNumberplatePath());
			mFileModel.setType(FileModel.MediaType.MEDIA_TYPE_IMAGE);
			fileParams.put(mFileModel, mFile.getAbsoluteFile());
		} else {
			Logger.e("NumberPlateImage File Not Exist: ", mLocalEventModel.getNumberplatePath());
		}
		Logger.e(String.valueOf(fileParams.size()));

		try {
			Retrofit.with(mContext)
					.setMediaFileUploadListener(EventUploadService.this)
					.setFileParameters(params, fileParams)
					.setAPI(APIs.API_UPLOAD_EVENT)
					.setCallBackListenerMultipart(new JSONCallbackMultipart(mContext) {

						@Override
						protected void onSuccess(int statusCode, JSONObject jsonObject, String tag) {
							try {
								int video_Id = mLocalEventModel.getVideoId(), event_id = mLocalEventModel.getId();
								long temp_event_created_time = mLocalEventModel.getChallanCaptureTime();
								Logger.e("Received Tag: ", "==> " + tag);
								if (!tag.isEmpty()) {
									String[] videoChallanData = tag.split(",");
									video_Id = Integer.parseInt(videoChallanData[0]);
									temp_event_created_time = Long.parseLong(videoChallanData[1]);
									event_id = Integer.parseInt(videoChallanData[2]);
									Logger.e("video_Id: ", "==> " + video_Id);
									Logger.e("challan_capture_time: ", "==> " + temp_event_created_time);
									Logger.e("event_id: ", "==> " + event_id);
								}

								if (jsonObject.optJSONObject("data") != null && jsonObject.optJSONObject("data").optJSONObject("event") != null) {
									try {
										Type modelType = new TypeToken<EventDataModel>() {
										}.getType();
										EventDataModel mEventModel = new Gson().fromJson(jsonObject.optJSONObject("data").optJSONObject("event").toString(), modelType);
										Logger.e(mEventModel.toString());

										AppDatabase appDatabase = AppDatabase.getAppDatabase(mContext);
										if (appDatabase != null && appDatabase.videoDao().getMyVideoCount(session.getUserDetail().getUserId()) > 0) {
											appDatabase.eventDao().updateEventSyncStatus(event_id);
											appDatabase.eventDao().removeAllEventsExceptLast();

											if (appDatabase.configDao() != null && appDatabase.configDao().getConfigData() != null) {
												long challanCaptureTime = appDatabase.videoDao().getVideoData(video_Id).getChallanCaptureTime();

												if (challanCaptureTime < temp_event_created_time) {
													appDatabase.videoDao().updateChallanCaptureTime(video_Id, temp_event_created_time);
													appDatabase.configDao().updateRemainingQuota(session.getUserDetail().getUserId(), appDatabase.configDao().getConfigData().getRemainingMinutes() + (temp_event_created_time - challanCaptureTime));
													Log.e("VideoCaptureService", "now recordingQuotaMillis in minutes ==> " + TimeStamp.getMinutesFromMilliseconds(appDatabase.configDao().getConfigData().getRemainingMinutes() + +(temp_event_created_time - challanCaptureTime)));
												}
											}
											deleteEventData(mContext, event_id);
										}

										// Send Broadcast to Dashboard Screen to update Event Count
										LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent(AppConstants.INTENT_FILTER_MESSAGE_RECEIVED));

										contentText = jsonObject.optJSONObject("data").optJSONObject("event").optString("remainingEvents") + " " + getString(R.string.events_remaining_out_of) + " " + appDatabase.configDao().getConfigData().getTargetEvents();
										checkAndUploadNextEvent(contentText);


									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							} catch (NumberFormatException e) {
								e.printStackTrace();
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						@Override
						protected void onFailed(int statusCode, String message) {
							Logger.e(message);

							isSetOngoing = false;
							//contentText = getString(R.string.failed_to_connect_with_server);
							contentText = message != null ? message : getString(R.string.failed_to_connect_with_server);
							progress = 0;
							notificationBuilder.setAutoCancel(true);
							remoteViews.setProgressBar(R.id.notifProgressBar, 100, progress, false);
							remoteViews.setTextViewText(R.id.tvState, contentText);
							mNotificationManager.notify(notificationID, mNotification);

							stopForeground(true);
							stopSelf();

							showEventFailedNotification(contentText);
							setEventUploadWorker();
						}
					}, String.valueOf(mLocalEventModel.getVideoId()).concat(",").concat(String.valueOf(mLocalEventModel.getChallanCaptureTime())).concat(",").concat(String.valueOf(mLocalEventModel.getId())));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void checkAndUploadNextEvent(String remainingEventsMessage) {

		int driverId = new SessionManager(mContext).getUserDetail().getUserId();
		int remainingEvents = eventDao.getRemainingEventCount(driverId);
		if (remainingEvents > 0) {
			this.remainingEventCount = remainingEvents;
			Logger.e("Remaining Videos: ", String.valueOf(remainingEvents));

			mLocalEventModel = eventDao.getRemainingEvent(driverId);
			if (mLocalEventModel != null) {

				File videoFile = new File(mLocalEventModel.getCroppedVideoPath());
				File imageFile = new File(mLocalEventModel.getImagePath());
				File numberPlateFile = new File(mLocalEventModel.getNumberplatePath());

				if (videoFile.exists() && imageFile.exists() && numberPlateFile.exists()) {
					progress = 0;
					progressBarVisibility = true;
					if (remainingEventCount > 0) {
						contentText = getString(R.string.uploading_event_data) + " " + progress + getString(R.string.percentage) + "(" + getResources().getString(R.string.remaining_events) + ": " + remainingEventCount + ")";
					} else {
						contentText = getString(R.string.uploading_event_data) + " " + progress + getString(R.string.percentage);
					}
					isSetOngoing = true;

					remoteViews.setProgressBar(R.id.notifProgressBar, 100, progress, false);
					remoteViews.setTextViewText(R.id.tvState, contentText);
					remoteViews.setViewVisibility(R.id.notifProgressBar, progressBarVisibility ? View.VISIBLE : View.GONE);
					mNotificationManager.notify(notificationID, mNotification);

					API_UploadEventDetail();
				} else {
					checkAndUploadNextEvent(remainingEventsMessage);
				}

			} else {
				stopForeground(true);
				stopSelf();
			}
		} else {

			isSetOngoing = false;

			progress = 100;
			progressBarVisibility = false;
			notificationBuilder.setAutoCancel(true);

			remoteViews.setProgressBar(R.id.notifProgressBar, 100, progress, false);
			remoteViews.setTextViewText(R.id.tvState, remainingEventsMessage);
			remoteViews.setViewVisibility(R.id.notifProgressBar, progressBarVisibility ? View.VISIBLE : View.GONE);
			mNotificationManager.notify(notificationID, mNotification);

			stopForeground(true);
			stopSelf();
		}
	}

	private void setEventUploadWorker() {

		String workerUuid = session.getDataByKey(SessionManager.KEY_WORKER_UUID_UPLOAD_EVENT);
		if (workerUuid.equals("")) {
			// Create a Constraints object that defines when the task should run
			Constraints constraints = new Constraints.Builder()
					.setRequiredNetworkType(NetworkType.CONNECTED)
					.build();

			OneTimeWorkRequest.Builder eventUploadWorkBuilder =
					new OneTimeWorkRequest.Builder(EventUploadWorker.class)
							.setInitialDelay(1, TimeUnit.MINUTES);
			// Todo: Change duration from 1 minute to 5 or 15 minute in final production mode -Viraj

			// Add Internet connectivity constraint
			eventUploadWorkBuilder.setConstraints(constraints);
			// Add Tag to workBuilder
			eventUploadWorkBuilder.addTag(AppConstants.EVENT_DATA_UPLOAD_WORK_NAME);
			// Create the actual work object:
			OneTimeWorkRequest firstDayWork = eventUploadWorkBuilder.build();
			// Then enqueue the recurring task:
			WorkManager.getInstance().enqueue(firstDayWork);

			// Set UUID in session for future use
			UUID workerId = firstDayWork.getId();
			session.storeDataByKey(SessionManager.KEY_WORKER_UUID_UPLOAD_EVENT, workerId.toString());
		}
	}

	private void showEventFailedNotification(String message) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && mNotificationManager.getNotificationChannel(FOREGROUND_CHANNEL_ID_EVENT_FAILED) == null) {
			CharSequence name = getString(R.string.text_name_notification);
			int importance = NotificationManager.IMPORTANCE_LOW;
			NotificationChannel channel = new NotificationChannel(FOREGROUND_CHANNEL_ID_EVENT_FAILED, name, importance);
			channel.enableVibration(false);
			mNotificationManager.createNotificationChannel(channel);
		}

		Intent mIntent = new Intent(EventUploadService.this, DashBoardActivity.class);
		PendingIntent mPendingIntent = PendingIntent.getService(EventUploadService.this, (int) System.currentTimeMillis(), mIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_CANCEL_CURRENT);

		remoteViews = new RemoteViews(getPackageName(), R.layout.notification_event_upload);

		remoteViews.setProgressBar(R.id.notifProgressBar, 100, progress, false);
		remoteViews.setTextViewText(R.id.tvState, message);
		remoteViews.setViewVisibility(R.id.notifProgressBar, View.GONE);

		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, FOREGROUND_CHANNEL_ID_EVENT_FAILED);
		notificationBuilder
				.setContent(remoteViews)
				.setSmallIcon(R.mipmap.ic_launcher)
				.setCategory(NotificationCompat.CATEGORY_SERVICE)
				.setContentIntent(mPendingIntent)
				.setAutoCancel(true)
				.setColor(ContextCompat.getColor(EventUploadService.this, R.color.colorPrimary));

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			notificationBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
		}
		mNotificationManager.notify(eventFailedNotificationID, notificationBuilder.build());
	}
}