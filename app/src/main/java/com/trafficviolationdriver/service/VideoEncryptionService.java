package com.trafficviolationdriver.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.trafficviolationdriver.R;
import com.trafficviolationdriver.db.AppDatabase;
import com.trafficviolationdriver.db.dao.VideoDao;
import com.trafficviolationdriver.db.entity.VideoTableModel;
import com.trafficviolationdriver.interfaces.OnVideoEncryptionDecryptionListener;
import com.trafficviolationdriver.util.AppConstants;
import com.trafficviolationdriver.util.Logger;
import com.trafficviolationdriver.util.SecureVideoUtil;
import com.trafficviolationdriver.util.SessionManager;
import com.trafficviolationdriver.util.Utils;

import java.io.File;
import java.util.Objects;

/**
 * Created by viraj.patel on 30-May-19
 */
public class VideoEncryptionService extends Service implements OnVideoEncryptionDecryptionListener {

    private static final String TAG = VideoEncryptionService.class.getSimpleName();
    private final static String FOREGROUND_CHANNEL_ID = "video_encryption_channel_id";
    private String FOREGROUND_VIDEO_CHANNEL_ID;

    public static final String KEY_INPUT_PATH = "inputPath";
    public static final String KEY_OUTPUT_PATH = "outputPath";
    public static final String KEY_VIDEO_ID = "videoId";

    private Context mContext;

    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder notificationBuilder;
    int notificationID = 4005;
    private String contentText = "";
    private boolean progressBarVisibility = true;

    /**
     * Used to check whether the bound activity has really gone away and not unbound as part of an
     * orientation change. We create a foreground service notification only if the former takes
     * place.
     */
    private boolean isSetOngoing = true;
    private int progress;
    private Bundle mBundle;

    private String inputPath, encryptedFilePath;
    private int videoId;

    VideoDao videoDao;
    RemoteViews remoteViews;
    private Notification mNotification;

    @Override
    public void onCreate() {
        super.onCreate();

        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // Android O requires a Notification Channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            // Create the channel for the notification
            NotificationChannel mChannel = new NotificationChannel(FOREGROUND_CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            // Set the Notification Channel for the Notification Manager.
            mNotificationManager.createNotificationChannel(mChannel);
        }
        mContext = this;
        contentText = getString(R.string.preparing_video);
        videoDao = AppDatabase.getAppDatabase(this).videoDao();
    }

    @Override
    public synchronized int onStartCommand(Intent intent, int flags, int startId) {
        try {
            mBundle = intent.getExtras();
            if (mBundle == null) {
                return START_NOT_STICKY;
            }

            switch (Objects.requireNonNull(intent.getAction())) {
                case AppConstants.ACTION.START_ACTION:

                    inputPath = mBundle.getString(KEY_INPUT_PATH);
                    encryptedFilePath = mBundle.getString(KEY_OUTPUT_PATH);
                    videoId = mBundle.getInt(KEY_VIDEO_ID);

                    progress = 0;
                    progressBarVisibility = true;
                    contentText = getString(R.string.processing_video) + " " + progress + getString(R.string.percentage);
                    isSetOngoing = true;

                    startForeground(notificationID, prepareNotification());

                    SecureVideoUtil.getInstance().doEncryption(inputPath, encryptedFilePath, this);

                    break;

                case AppConstants.ACTION.STOP_ACTION:
                    Logger.e(TAG, "in STOP_ACTION");
                    stopSelf();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private Notification prepareNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && mNotificationManager.getNotificationChannel(FOREGROUND_CHANNEL_ID) == null) {
            CharSequence name = getString(R.string.text_name_notification);
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel channel = new NotificationChannel(FOREGROUND_CHANNEL_ID, name, importance);
            channel.enableVibration(false);
            mNotificationManager.createNotificationChannel(channel);
        }

        Intent mIntent = new Intent(VideoEncryptionService.this, VideoEncryptionService.class);
        mIntent.setAction(AppConstants.ACTION.STOP_ACTION);
        PendingIntent mPendingIntent = PendingIntent.getService(VideoEncryptionService.this, 0, mIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        remoteViews = new RemoteViews(getPackageName(), R.layout.notification_event_upload);

        remoteViews.setProgressBar(R.id.notifProgressBar, 100, progress, false);
        remoteViews.setTextViewText(R.id.tvState, contentText);
        remoteViews.setViewVisibility(R.id.notifProgressBar, progressBarVisibility ? View.VISIBLE : View.GONE);

        notificationBuilder = new NotificationCompat.Builder(this, FOREGROUND_CHANNEL_ID);
        notificationBuilder
                .setContent(remoteViews)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .setDeleteIntent(mPendingIntent)
                .setOnlyAlertOnce(true)
                .setColor(ContextCompat.getColor(VideoEncryptionService.this, R.color.colorPrimary))
                .setOngoing(isSetOngoing);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        }
        mNotification = notificationBuilder.build();
        return mNotification;
    }

    @Override
    public void onVideoOperationSucceed() {
        Logger.e("Video Encryption Progress: ", "Finished");

        try {
            VideoTableModel videoTableModel = videoDao.getVideoData(videoId);
            if (encryptedFilePath != null) {
                videoTableModel.setEncryptedVideoPath(encryptedFilePath);
            }
            videoTableModel.setEncrypted(true);
            videoTableModel.setEncryptedPercentage(100);
            videoTableModel.setVideoPath("");
            videoDao.updateVideoData(videoTableModel);
            Utils.deleteVideo(inputPath);

            isSetOngoing = false;
            progress = 100;
            progressBarVisibility = false;
            notificationBuilder.setAutoCancel(true);

            if (notificationBuilder != null) {
                progress = 0;
                remoteViews.setProgressBar(R.id.notifProgressBar, 100, progress, false);
                remoteViews.setTextViewText(R.id.tvState, contentText);
                remoteViews.setViewVisibility(R.id.notifProgressBar, progressBarVisibility ? View.VISIBLE : View.GONE);
                mNotificationManager.notify(notificationID, mNotification);
            }

            int driverId = new SessionManager(this).getUserDetail().getUserId();
            int remainingVideos = videoDao.getRemainingEncryptionVideos(driverId);
            if (remainingVideos > 0) {

                Logger.e("Remaining Videos: ", String.valueOf(remainingVideos));

                VideoTableModel mVideoModel = videoDao.getRemainingVideoDetail(driverId);
                if (mVideoModel != null) {

                    inputPath = mVideoModel.getVideoPath();
                    encryptedFilePath = new File(Utils.getVideoFolder(this), mVideoModel.getVideoName()).getAbsolutePath();
                    videoId = mVideoModel.getId();

                    progress = 0;
                    progressBarVisibility = true;
                    contentText = getString(R.string.processing_video) + " " + progress + getString(R.string.percentage);
                    isSetOngoing = true;

                    SecureVideoUtil.getInstance().doEncryption(inputPath, encryptedFilePath, this);
                } else {
                    stopForeground(true);
                    stopSelf();
                }
            } else {
                stopForeground(true);
                stopSelf();
            }
        } catch (Exception e) {
            Logger.e("Video Encryption Exception: ", e.toString());

        }
    }

    @Override
    public void onProgressUpdate(int progress) {
        Logger.e("Video Encryption Progress: ", String.valueOf(progress));
        if (notificationBuilder != null) {
            progressBarVisibility = true;
            this.progress = progress;
            contentText = getString(R.string.processing_video) + " " + progress + getString(R.string.percentage);

            remoteViews.setProgressBar(R.id.notifProgressBar, 100, progress, false);
            remoteViews.setTextViewText(R.id.tvState, contentText);

            mNotificationManager.notify(notificationID, mNotification);
        }
        videoDao.updateVideoEncryptionProgress(videoId, progress);
    }

    @Override
    public void onVideoOperationFailed(String message) {
        Logger.e("Video Encryption: ", "onError");
        isSetOngoing = false;
        contentText = getString(R.string.failed_to_process_video);
        progress = 0;
        notificationBuilder.setAutoCancel(true);
        remoteViews.setProgressBar(R.id.notifProgressBar, 100, progress, false);
        remoteViews.setTextViewText(R.id.tvState, contentText);

        mNotificationManager.notify(notificationID, mNotification);
        stopForeground(false);
    }
}
