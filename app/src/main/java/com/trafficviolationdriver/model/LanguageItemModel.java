package com.trafficviolationdriver.model;

/**
 * Created by viraj.patel on 18-Apr-19
 */
public class LanguageItemModel {
    private String title = "",local="";

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public LanguageItemModel(String title, String local) {
		this.title = title;
		this.local = local;
	}
}
