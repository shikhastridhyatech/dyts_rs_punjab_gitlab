package com.trafficviolationdriver.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ${RICHA} on 19-04-2019.
 */
public class EventDataModel implements Parcelable {

    /**
     * violationImage : imagesviolationImages/violation_1555647520.jpeg
     * violationNumberPlateImage : imagesviolationNumberPlateImages/violation_number_plate_1555647520.jpg
     * violationVideo : VideosviolationVideos/violation_1555647520.mp4
     * violationDateTime : 1518205560
     * vehicleNo : GJ-01-PG-3698
     * violationReason : Wrong side vehicle drive
     * uploadedBy : XYZ ABC
     * violationLatitude : 56.65454854
     * violationLongitude : 41.56454848
     * eventStatus : Pending
     */

    @SerializedName("violationImage")
    private String violationImage;
    @SerializedName("violationNumberPlateImage")
    private String violationNumberPlateImage;
    @SerializedName("violationVideo")
    private String violationVideo;
    @SerializedName("violationDateTime")
    private int violationDateTime;
    @SerializedName("vehicleNo")
    private String vehicleNo;
    @SerializedName("violationReason")
    private String violationReason;
    @SerializedName("uploadedBy")
    private String uploadedBy;
    @SerializedName("violationLatitude")
    private String violationLatitude;
    @SerializedName("violationLongitude")
    private String violationLongitude;
    @SerializedName("eventStatus")
    private String eventStatus;

    public String getViolationImage() {
        return violationImage;
    }

    public void setViolationImage(String violationImage) {
        this.violationImage = violationImage;
    }

    public String getViolationNumberPlateImage() {
        return violationNumberPlateImage;
    }

    public void setViolationNumberPlateImage(String violationNumberPlateImage) {
        this.violationNumberPlateImage = violationNumberPlateImage;
    }

    public String getViolationVideo() {
        return violationVideo;
    }

    public void setViolationVideo(String violationVideo) {
        this.violationVideo = violationVideo;
    }

    public int getViolationDateTime() {
        return violationDateTime;
    }

    public void setViolationDateTime(int violationDateTime) {
        this.violationDateTime = violationDateTime;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getViolationReason() {
        return violationReason;
    }

    public void setViolationReason(String violationReason) {
        this.violationReason = violationReason;
    }

    public String getUploadedBy() {
        return uploadedBy;
    }

    public void setUploadedBy(String uploadedBy) {
        this.uploadedBy = uploadedBy;
    }

    public String getViolationLatitude() {
        return violationLatitude;
    }

    public void setViolationLatitude(String violationLatitude) {
        this.violationLatitude = violationLatitude;
    }

    public String getViolationLongitude() {
        return violationLongitude;
    }

    public void setViolationLongitude(String violationLongitude) {
        this.violationLongitude = violationLongitude;
    }

    public String getEventStatus() {
        return eventStatus;
    }

    public void setEventStatus(String eventStatus) {
        this.eventStatus = eventStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.violationImage);
        dest.writeString(this.violationNumberPlateImage);
        dest.writeString(this.violationVideo);
        dest.writeInt(this.violationDateTime);
        dest.writeString(this.vehicleNo);
        dest.writeString(this.violationReason);
        dest.writeString(this.uploadedBy);
        dest.writeString(this.violationLatitude);
        dest.writeString(this.violationLongitude);
        dest.writeString(this.eventStatus);
    }

    public EventDataModel() {
    }

    protected EventDataModel(Parcel in) {
        this.violationImage = in.readString();
        this.violationNumberPlateImage = in.readString();
        this.violationVideo = in.readString();
        this.violationDateTime = in.readInt();
        this.vehicleNo = in.readString();
        this.violationReason = in.readString();
        this.uploadedBy = in.readString();
        this.violationLatitude = in.readString();
        this.violationLongitude = in.readString();
        this.eventStatus = in.readString();
    }

    public static final Parcelable.Creator<EventDataModel> CREATOR = new Parcelable.Creator<EventDataModel>() {
        @Override
        public EventDataModel createFromParcel(Parcel source) {
            return new EventDataModel(source);
        }

        @Override
        public EventDataModel[] newArray(int size) {
            return new EventDataModel[size];
        }
    };

    @Override
    public String toString() {
        return "EventDataModel{" +
                "violationImage='" + violationImage + '\'' +
                ", violationNumberPlateImage='" + violationNumberPlateImage + '\'' +
                ", violationVideo='" + violationVideo + '\'' +
                ", violationDateTime=" + violationDateTime +
                ", vehicleNo='" + vehicleNo + '\'' +
                ", violationReason='" + violationReason + '\'' +
                ", uploadedBy='" + uploadedBy + '\'' +
                ", violationLatitude='" + violationLatitude + '\'' +
                ", violationLongitude='" + violationLongitude + '\'' +
                ", eventStatus='" + eventStatus + '\'' +
                '}';
    }
}
