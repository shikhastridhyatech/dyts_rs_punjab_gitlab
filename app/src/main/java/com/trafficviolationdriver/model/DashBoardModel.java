package com.trafficviolationdriver.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ${RICHA} on 16-04-2019.
 */
public class DashBoardModel {

    /**
     * notificationCount : 12
     * lastLogin : null
     * today : {"totalEvents":0,"pendingEvents":0,"approvedEvents":0,"rejectedEvents":0}
     * last7days : {"totalEvents":4,"pendingEvents":3,"approvedEvents":1,"rejectedEvents":0}
     * last30days : {"totalEvents":5,"pendingEvents":4,"approvedEvents":1,"rejectedEvents":0}
     */


    @SerializedName("notificationCount")
    private int notificationCount;
    @SerializedName("lastLogin")
    private int lastLogin;
    @SerializedName("today")
    private DashboardEventModel today;
    @SerializedName("last7days")
    private DashboardEventModel last7days;
    @SerializedName("last30days")
    private DashboardEventModel last30days;
    /**
     * dataDeleteTime : 13:00:00
     * recordingQuotaMinutes : 85
     * targetEventCount : 0
     * remainingEvents : 0
     */

    @SerializedName("dataDeleteTime")
    private String dataDeleteTime;
    @SerializedName("recordingQuotaMinutes")
    private String recordingQuotaMinutes;
    @SerializedName("targetEventCount")
    private int targetEventCount;
    @SerializedName("remainingEvents")
    private int remainingEvents;


    public int getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(int notificationCount) {
        this.notificationCount = notificationCount;
    }

    public long getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(int lastLogin) {
        this.lastLogin = lastLogin;
    }

    public DashboardEventModel getToday() {
        return today;
    }

    public void setToday(DashboardEventModel today) {
        this.today = today;
    }

    public DashboardEventModel getLast7days() {
        return last7days;
    }

    public void setLast7days(DashboardEventModel last7days) {
        this.last7days = last7days;
    }

    public DashboardEventModel getLast30days() {
        return last30days;
    }

    public void setLast30days(DashboardEventModel last30days) {
        this.last30days = last30days;
    }

    @Override
    public String toString() {
        return "DashBoardModel{" + "notificationCount=" + notificationCount + ", lastLogin=" + lastLogin + ", today=" + today + ", last7days=" + last7days + ", last30days=" + last30days + '}';
    }

    public String getDataDeleteTime() {
        return dataDeleteTime;
    }

    public void setDataDeleteTime(String dataDeleteTime) {
        this.dataDeleteTime = dataDeleteTime;
    }

    public String getRecordingQuotaMinutes() {
        return recordingQuotaMinutes;
    }

    public void setRecordingQuotaMinutes(String recordingQuotaMinutes) {
        this.recordingQuotaMinutes = recordingQuotaMinutes;
    }

    public int getTargetEventCount() {
        return targetEventCount;
    }

    public void setTargetEventCount(int targetEventCount) {
        this.targetEventCount = targetEventCount;
    }

    public int getRemainingEvents() {
        return remainingEvents;
    }

    public void setRemainingEvents(int remainingEvents) {
        this.remainingEvents = remainingEvents;
    }
}

