package com.trafficviolationdriver.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ${RICHA} on 08-04-2019.
 */
public class UserDataModel implements Parcelable {


    @SerializedName("isActive")
    private boolean isActive;
    @SerializedName("isIMEIVerified")
    private boolean isIMEIVerified;
    @SerializedName("userId")
    private Integer userId;
    @SerializedName("email")
    private String email;
    @SerializedName("phoneNumber")
    private String phoneNumber;
    @SerializedName("city")
    private String city;
    @SerializedName("cityId")
    private String cityId;
    @SerializedName("firstName")
    private String firstName;
    @SerializedName("lastName")
    private String lastName;
    @SerializedName("imageUrl")
    private String imageUrl;
    @SerializedName("token")
    private String token;
    @SerializedName("middleName")
    private String middleName;
    @SerializedName("panNumber")
    private String panNumber;
    @SerializedName("drivingLicence")
    private String drivingLicence;
    @SerializedName("AadharNumber")
    private String AadharNumber;
    @SerializedName("voterNumber")
    private String voterNumber;
    @SerializedName("secondaryPhoneNumber")
    private String secondaryPhoneNumber;
    @SerializedName("address")
    private String address;
    @SerializedName("bloodGroup")
    private String bloodGroup;
    @SerializedName("disease")
    private String disease;
    @SerializedName("allergy")
    private String allergy;
    @SerializedName("pfAccountNumber")
    private String pfAccountNumber;
    @SerializedName("bankAccount")
    private String bankAccount;
    @SerializedName("bankName")
    private String bankName;
    @SerializedName("bankBranch")
    private String bankBranch;
    @SerializedName("bankIFSC")
    private String bankIFSC;
    @SerializedName("IMEI")
    private String IMEI;
    @SerializedName("joiningDate")
    private int joiningDate;
    @SerializedName("dataDeleteTime")
    private String dataDeleteTime;
    @SerializedName("recordingQuotaMinutes")
    private int recordingQuotaMinutes;
    @SerializedName("targetEventCount")
    private int targetEventCount;

    public boolean isIsActive() {
        return isActive;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isIsIMEIVerified() {
        return isIMEIVerified;
    }

    public void setIsIMEIVerified(boolean isIMEIVerified) {
        this.isIMEIVerified = isIMEIVerified;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public String getDrivingLicence() {
        return drivingLicence;
    }

    public void setDrivingLicence(String drivingLicence) {
        this.drivingLicence = drivingLicence;
    }

    public String getAadharNumber() {
        return AadharNumber;
    }

    public void setAadharNumber(String AadharNumber) {
        this.AadharNumber = AadharNumber;
    }

    public String getVoterNumber() {
        return voterNumber;
    }

    public void setVoterNumber(String voterNumber) {
        this.voterNumber = voterNumber;
    }

    public String getSecondaryPhoneNumber() {
        return secondaryPhoneNumber;
    }

    public void setSecondaryPhoneNumber(String secondaryPhoneNumber) {
        this.secondaryPhoneNumber = secondaryPhoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public String getAllergy() {
        return allergy;
    }

    public void setAllergy(String allergy) {
        this.allergy = allergy;
    }

    public String getPfAccountNumber() {
        return pfAccountNumber;
    }

    public void setPfAccountNumber(String pfAccountNumber) {
        this.pfAccountNumber = pfAccountNumber;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getBankIFSC() {
        return bankIFSC;
    }

    public void setBankIFSC(String bankIFSC) {
        this.bankIFSC = bankIFSC;
    }

    public String getIMEI() {
        return IMEI;
    }

    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    public int getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(int joiningDate) {
        this.joiningDate = joiningDate;
    }

    public String getDataDeleteTime() {
        return dataDeleteTime;
    }

    public void setDataDeleteTime(String dataDeleteTime) {
        this.dataDeleteTime = dataDeleteTime;
    }

    public int getRecordingQuotaMinutes() {
        return recordingQuotaMinutes;
    }

    public void setRecordingQuotaMinutes(int recordingQuotaMinutes) {
        this.recordingQuotaMinutes = recordingQuotaMinutes;
    }

    public int getTargetEventCount() {
        return targetEventCount;
    }

    public void setTargetEventCount(int targetEventCount) {
        this.targetEventCount = targetEventCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.isActive ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isIMEIVerified ? (byte) 1 : (byte) 0);
        dest.writeInt(this.userId);
        dest.writeString(this.email);
        dest.writeString(this.phoneNumber);
        dest.writeString(this.city);
        dest.writeString(this.cityId);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.imageUrl);
        dest.writeString(this.token);
        dest.writeString(this.middleName);
        dest.writeString(this.panNumber);
        dest.writeString(this.drivingLicence);
        dest.writeString(this.AadharNumber);
        dest.writeString(this.voterNumber);
        dest.writeString(this.secondaryPhoneNumber);
        dest.writeString(this.address);
        dest.writeString(this.bloodGroup);
        dest.writeString(this.disease);
        dest.writeString(this.allergy);
        dest.writeString(this.pfAccountNumber);
        dest.writeString(this.bankAccount);
        dest.writeString(this.bankName);
        dest.writeString(this.bankBranch);
        dest.writeString(this.bankIFSC);
        dest.writeString(this.IMEI);
        dest.writeInt(this.joiningDate);
        dest.writeString(this.dataDeleteTime);
        dest.writeInt(this.recordingQuotaMinutes);
        dest.writeInt(this.targetEventCount);
    }

    public UserDataModel() {
    }

    protected UserDataModel(Parcel in) {
        this.isActive = in.readByte() != 0;
        this.isIMEIVerified = in.readByte() != 0;
        this.userId = in.readInt();
        this.email = in.readString();
        this.phoneNumber = in.readString();
        this.city = in.readString();
        this.cityId = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.imageUrl = in.readString();
        this.token = in.readString();
        this.middleName = in.readString();
        this.panNumber = in.readString();
        this.drivingLicence = in.readString();
        this.AadharNumber = in.readString();
        this.voterNumber = in.readString();
        this.secondaryPhoneNumber = in.readString();
        this.address = in.readString();
        this.bloodGroup = in.readString();
        this.disease = in.readString();
        this.allergy = in.readString();
        this.pfAccountNumber = in.readString();
        this.bankAccount = in.readString();
        this.bankName = in.readString();
        this.bankBranch = in.readString();
        this.bankIFSC = in.readString();
        this.IMEI = in.readString();
        this.joiningDate = in.readInt();
        this.dataDeleteTime = in.readString();
        this.recordingQuotaMinutes = in.readInt();
        this.targetEventCount = in.readInt();
    }

    public static final Parcelable.Creator<UserDataModel> CREATOR = new Parcelable.Creator<UserDataModel>() {
        @Override
        public UserDataModel createFromParcel(Parcel source) {
            return new UserDataModel(source);
        }

        @Override
        public UserDataModel[] newArray(int size) {
            return new UserDataModel[size];
        }
    };

    public String toString() {
        return "UserDataModel{" + "isActive=" + isActive + ", isIMEIVerified=" + isIMEIVerified + ", userId=" + userId + ", email='" + email + '\'' + ", phoneNumber='" + phoneNumber + '\'' + ", city='" + city + '\'' + ", cityId='" + cityId + '\'' + ", firstName='" + firstName + '\'' + ", lastName='" + lastName + '\'' + ", imageUrl='" + imageUrl + '\'' + ", token='" + token + '\'' + ", middleName='" + middleName + '\'' + ", panNumber='" + panNumber + '\'' + ", drivingLicence='" + drivingLicence + '\'' + ", AadharNumber='" + AadharNumber + '\'' + ", voterNumber='" + voterNumber + '\'' + ", secondaryPhoneNumber='" + secondaryPhoneNumber + '\'' + ", address='" + address + '\'' + ", bloodGroup='" + bloodGroup + '\'' + ", disease='" + disease + '\'' + ", allergy='" + allergy + '\'' + ", pfAccountNumber='" + pfAccountNumber + '\'' + ", bankAccount='" + bankAccount + '\'' + ", bankName='" + bankName + '\'' + ", bankBranch='" + bankBranch + '\'' + ", bankIFSC='" + bankIFSC + '\'' + ", IMEI='" + IMEI + '\'' + ", joiningDate=" + joiningDate + ", dataDeleteTime='" + dataDeleteTime + '\'' + ", recordingQuotaMinutes='" + recordingQuotaMinutes + '\'' + ", targetEventCount='" + targetEventCount + '\'' + '}';
    }
}

