package com.trafficviolationdriver.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ${RICHA} on 17-04-2019.
 */
public class MessageDetailModel implements Parcelable {

    @SerializedName("id")
    private int id;
    @SerializedName("message")
    private String message;
    @SerializedName("isRead")
    private boolean isRead;
    @SerializedName("dateTime")
    private int dateTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isIsRead() {
        return isRead;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }

    public int getDateTime() {
        return dateTime;
    }

    public void setDateTime(int dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        return "MessageDetailModel{" + "id=" + id + ", message='" + message + '\'' + ", isRead=" + isRead + ", dateTime=" + dateTime + '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.message);
        dest.writeByte(this.isRead ? (byte) 1 : (byte) 0);
        dest.writeInt(this.dateTime);
    }

    public MessageDetailModel() {
    }

    protected MessageDetailModel(Parcel in) {
        this.id = in.readInt();
        this.message = in.readString();
        this.isRead = in.readByte() != 0;
        this.dateTime = in.readInt();
    }

    public static final Parcelable.Creator<MessageDetailModel> CREATOR = new Parcelable.Creator<MessageDetailModel>() {
        @Override
        public MessageDetailModel createFromParcel(Parcel source) {
            return new MessageDetailModel(source);
        }

        @Override
        public MessageDetailModel[] newArray(int size) {
            return new MessageDetailModel[size];
        }
    };
}
