package com.trafficviolationdriver.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.trafficviolationdriver.util.TimeStamp;

/**
 * Created by viraj.patel on 13-Apr-19
 */
public class MessageModel implements Parcelable {


    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("vehicleNumber")
    @Expose
    private String vehicleNumber;
    @SerializedName("dateTime")
    @Expose
    private long dateTime;
    @SerializedName("message")
    @Expose
    private String message;

    public MessageModel(String id, String message, long dateTime) {
        this.id = id;
        this.message = message;
        this.dateTime = dateTime;
    }

    public MessageModel(String id, String message, String vehicleNumber, long dateTime) {
        this.id = id;
        this.message = message;
        this.vehicleNumber = vehicleNumber;
        this.dateTime = dateTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    @BindingAdapter("app:setMessageDateTime")
    public static void setMessageDateTime(TextView view, long timestamp) {
        view.setText(TimeStamp.getGTSDateFromTimestamp(timestamp));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.message);
        dest.writeString(this.vehicleNumber);
        dest.writeLong(this.dateTime);
    }

    protected MessageModel(Parcel in) {
        this.id = in.readString();
        this.message = in.readString();
        this.vehicleNumber = in.readString();
        this.dateTime = in.readLong();
    }

    public static final Parcelable.Creator<MessageModel> CREATOR = new Parcelable.Creator<MessageModel>() {
        @Override
        public MessageModel createFromParcel(Parcel source) {
            return new MessageModel(source);
        }

        @Override
        public MessageModel[] newArray(int size) {
            return new MessageModel[size];
        }
    };

    @Override
    public String toString() {
        return "MessageModel{" +
                "id='" + id + '\'' +
                ", vehicleNumber='" + vehicleNumber + '\'' +
                ", dateTime=" + dateTime +
                ", message='" + message + '\'' +
                '}';
    }
}
