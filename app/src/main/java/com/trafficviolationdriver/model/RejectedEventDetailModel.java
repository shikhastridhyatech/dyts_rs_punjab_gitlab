package com.trafficviolationdriver.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ${RICHA} on 17-04-2019.
 */
public class RejectedEventDetailModel implements Parcelable {

    @SerializedName("eventId")
    private int eventId;
    @SerializedName("violationDateTime")
    private int violationDateTime;
    @SerializedName("violationImage")
    private String violationImage;
    @SerializedName("violationVideo")
    private String violationVideo;
    @SerializedName("violationNumberPlate")
    private String violationNumberPlate;
    @SerializedName("violationReason")
    private String violationReason;
    @SerializedName("vehicalNumber")
    private String vehicalNumber;
    @SerializedName("rejectionReason")
    private String rejectionReason;

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getViolationDateTime() {
        return violationDateTime;
    }

    public void setViolationDateTime(int violationDateTime) {
        this.violationDateTime = violationDateTime;
    }

    public String getViolationImage() {
        return violationImage;
    }

    public void setViolationImage(String violationImage) {
        this.violationImage = violationImage;
    }

    public String getViolationVideo() {
        return violationVideo;
    }

    public void setViolationVideo(String violationVideo) {
        this.violationVideo = violationVideo;
    }

    public String getViolationNumberPlate() {
        return violationNumberPlate;
    }

    public void setViolationNumberPlate(String violationNumberPlate) {
        this.violationNumberPlate = violationNumberPlate;
    }

    public String getViolationReason() {
        return violationReason;
    }

    public void setViolationReason(String violationReason) {
        this.violationReason = violationReason;
    }

    public String getVehicalNumber() {
        return vehicalNumber;
    }

    public void setVehicalNumber(String vehicalNumber) {
        this.vehicalNumber = vehicalNumber;
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.eventId);
        dest.writeInt(this.violationDateTime);
        dest.writeString(this.violationImage);
        dest.writeString(this.violationVideo);
        dest.writeString(this.violationNumberPlate);
        dest.writeString(this.violationReason);
        dest.writeString(this.vehicalNumber);
        dest.writeString(this.rejectionReason);
    }

    public RejectedEventDetailModel() {
    }

    protected RejectedEventDetailModel(Parcel in) {
        this.eventId = in.readInt();
        this.violationDateTime = in.readInt();
        this.violationImage = in.readString();
        this.violationVideo = in.readString();
        this.violationNumberPlate = in.readString();
        this.violationReason = in.readString();
        this.vehicalNumber = in.readString();
        this.rejectionReason = in.readString();
    }

    public static final Parcelable.Creator<RejectedEventDetailModel> CREATOR = new Parcelable.Creator<RejectedEventDetailModel>() {
        @Override
        public RejectedEventDetailModel createFromParcel(Parcel source) {
            return new RejectedEventDetailModel(source);
        }

        @Override
        public RejectedEventDetailModel[] newArray(int size) {
            return new RejectedEventDetailModel[size];
        }
    };

    @Override
    public String toString() {
        return "RejectedEventDetailModel{" +
                "eventId=" + eventId +
                ", violationDateTime=" + violationDateTime +
                ", violationImage='" + violationImage + '\'' +
                ", violationVideo='" + violationVideo + '\'' +
                ", violationNumberPlate='" + violationNumberPlate + '\'' +
                ", violationReason='" + violationReason + '\'' +
                ", vehicalNumber='" + vehicalNumber + '\'' +
                ", rejectionReason='" + rejectionReason + '\'' +
                '}';
    }
}
