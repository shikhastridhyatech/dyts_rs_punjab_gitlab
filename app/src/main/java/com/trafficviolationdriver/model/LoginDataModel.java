package com.trafficviolationdriver.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ${RICHA} on 10-05-2019.
 */
public class LoginDataModel implements Parcelable {


    /**
     * isActive : true
     * isIMEIVerified : true
     */

    @SerializedName("isActive")
    private boolean isActive;
    @SerializedName("isIMEIVerified")
    private boolean isIMEIVerified;
    @SerializedName("cityName")
    private String city;
    @SerializedName("cityId")
    private String cityId;

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isIsIMEIVerified() {
        return isIMEIVerified;
    }

    public void setIsIMEIVerified(boolean isIMEIVerified) {
        this.isIMEIVerified = isIMEIVerified;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.isActive ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isIMEIVerified ? (byte) 1 : (byte) 0);
    }

    public LoginDataModel() {
    }

    protected LoginDataModel(Parcel in) {
        this.isActive = in.readByte() != 0;
        this.isIMEIVerified = in.readByte() != 0;
    }

    public static final Parcelable.Creator<LoginDataModel> CREATOR = new Parcelable.Creator<LoginDataModel>() {
        @Override
        public LoginDataModel createFromParcel(Parcel source) {
            return new LoginDataModel(source);
        }

        @Override
        public LoginDataModel[] newArray(int size) {
            return new LoginDataModel[size];
        }
    };

    @Override
    public String toString() {
        return "LoginDataModel{" +
                "isActive=" + isActive +
                ", isIMEIVerified=" + isIMEIVerified +
                ", city='" + city + '\'' +
                ", cityId='" + cityId + '\'' +
                '}';
    }
}
