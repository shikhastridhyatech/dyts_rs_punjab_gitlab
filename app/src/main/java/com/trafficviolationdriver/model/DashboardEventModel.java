package com.trafficviolationdriver.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ${RICHA} on 16-04-2019.
 */
public class DashboardEventModel {

    @SerializedName("totalEvents")
    private int totalEvents;
    @SerializedName("pendingEvents")
    private int pendingEvents;
    @SerializedName("approvedEvents")
    private int approvedEvents;
    @SerializedName("rejectedEvents")
    private int rejectedEvents;

    public int getToalEvents() {
        return totalEvents;
    }

    public void setToalEvents(int totalEvents) {
        this.totalEvents = totalEvents;
    }

    public int getPendingEvents() {
        return pendingEvents;
    }

    public void setPendingEvents(int pendingEvents) {
        this.pendingEvents = pendingEvents;
    }

    public int getApprovedEvents() {
        return approvedEvents;
    }

    public void setApprovedEvents(int approvedEvents) {
        this.approvedEvents = approvedEvents;
    }

    public int getRejectedEvents() {
        return rejectedEvents;
    }

    public void setRejectedEvents(int rejectedEvents) {
        this.rejectedEvents = rejectedEvents;
    }

    @Override
    public String toString() {
        return "DashboardEventModel{" + "totalEvents=" + totalEvents + ", pendingEvents=" + pendingEvents + ", approvedEvents=" + approvedEvents + ", rejectedEvents=" + rejectedEvents + '}';
    }
}
