package com.trafficviolationdriver.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ${RICHA} on 08-04-2019.
 */
public class ViolationReasonsModel implements Parcelable {

    @SerializedName("reasonId")
    private int reasonId;
    @SerializedName("reasonValue")
    private String reasonValue;

    @SerializedName("isAlreadyApplied")
    private boolean isAlreadyApplied;


    public int getReasonId() {
        return reasonId;
    }

    public void setReasonId(int reasonId) {
        this.reasonId = reasonId;
    }

    public String getReasonValue() {
        return reasonValue;
    }

    public void setReasonValue(String reasonValue) {
        this.reasonValue = reasonValue;
    }


    public boolean isIsAlreadyApplied() {
        return isAlreadyApplied;
    }

    public void setIsAlreadyApplied(boolean isAlreadyApplied) {
        this.isAlreadyApplied = isAlreadyApplied;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.reasonId);
        dest.writeString(this.reasonValue);
        dest.writeByte(this.isAlreadyApplied ? (byte) 1 : (byte) 0);
    }

    public ViolationReasonsModel() {
    }

    protected ViolationReasonsModel(Parcel in) {
        this.reasonId = in.readInt();
        this.reasonValue = in.readString();
        this.isAlreadyApplied = in.readByte() != 0;
    }

    public static final Parcelable.Creator<ViolationReasonsModel> CREATOR = new Parcelable.Creator<ViolationReasonsModel>() {
        @Override
        public ViolationReasonsModel createFromParcel(Parcel source) {
            return new ViolationReasonsModel(source);
        }

        @Override
        public ViolationReasonsModel[] newArray(int size) {
            return new ViolationReasonsModel[size];
        }
    };

    @Override
    public String toString() {
        return "ViolationReasonsModel{" +
                "reasonId=" + reasonId +
                ", reasonValue='" + reasonValue + '\'' +
                ", isAlreadyApplied=" + isAlreadyApplied +
                '}';
    }
}


