package com.trafficviolationdriver.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.trafficviolationdriver.interfaces.OnNotificationDataReceivedListener;
import com.trafficviolationdriver.util.Logger;

/**
 * Created by viraj.patel on 09-May-19
 */
public class NotificationBroadcastReceiver extends BroadcastReceiver {

    private Context mContext;
    private OnNotificationDataReceivedListener<Intent> mListener;
    private String intentFilter = "";

    public NotificationBroadcastReceiver(Context context, OnNotificationDataReceivedListener<Intent> listener, String filter) {
        this.mContext = context;
        this.mListener = listener;
        this.intentFilter = filter;

        LocalBroadcastManager.getInstance(mContext).registerReceiver(this, new IntentFilter(filter));
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // Get extra data included in the Intent
        String message = intent.getStringExtra("message");
        Logger.e("receiver", "Got message: " + message);
        if (mListener != null) {
            mListener.onDataReceived(intent);
        }
    }
}
