package com.trafficviolationdriver.worker;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.trafficviolationdriver.db.AppDatabase;
import com.trafficviolationdriver.db.dao.EventDao;
import com.trafficviolationdriver.db.entity.EventTableModel;
import com.trafficviolationdriver.service.EventUploadService;
import com.trafficviolationdriver.util.AppConstants;
import com.trafficviolationdriver.util.Logger;
import com.trafficviolationdriver.util.SessionManager;
import com.trafficviolationdriver.util.Utils;

import java.io.File;

/**
 * Created by viraj.patel on 26-Apr-19
 */
public class EventUploadWorker extends Worker {

    private static final String TAG = EventUploadWorker.class.getSimpleName();

    private Context mContext;
    private EventDao eventDao;
    private SessionManager session;

    public EventUploadWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        mContext = context;
        session = new SessionManager(mContext);
        eventDao = AppDatabase.getAppDatabase(mContext).eventDao();
    }

    @NonNull
    @Override
    public Result doWork() {

        Logger.e(TAG, "in doWork() method of upload event worker");

        checkAndUploadEvent();

        return Result.success();
    }

    private void checkAndUploadEvent() {
        if (!Utils.isServiceRunning(mContext, EventUploadService.class) && eventDao.getRemainingEventCount(session.getUserDetail().getUserId()) > 0) {
            EventTableModel mEventModel = eventDao.getRemainingEvent(session.getUserDetail().getUserId());

            File videoFile = new File(mEventModel.getCroppedVideoPath());
            File imageFile = new File(mEventModel.getImagePath());
            File numberPlateFile = new File(mEventModel.getNumberplatePath());

            if (videoFile.exists() && imageFile.exists() && numberPlateFile.exists()) {
                //Start Foreground Service
                Intent mIntent = new Intent(mContext, EventUploadService.class);
                mIntent.setAction(AppConstants.ACTION.START_ACTION);
                mIntent.putExtra(EventUploadService.BUNDLE_DATA_LOCAL_EVENT_MODEL, mEventModel);
                mContext.startService(mIntent);
            } else {
                Logger.e("Seems, Event file is deleted externally");
                Utils.deleteEventData(mContext, mEventModel.getId());
                checkAndUploadEvent();
            }
        }
    }
}