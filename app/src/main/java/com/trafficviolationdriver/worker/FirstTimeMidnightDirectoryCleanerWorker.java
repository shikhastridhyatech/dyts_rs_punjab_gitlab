package com.trafficviolationdriver.worker;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.trafficviolationdriver.db.AppDatabase;
import com.trafficviolationdriver.util.FileUtil;
import com.trafficviolationdriver.util.Logger;
import com.trafficviolationdriver.util.SessionManager;
import com.trafficviolationdriver.util.TimeStamp;
import com.trafficviolationdriver.util.Utils;
import com.trafficviolationdriver.webservice.APIs;
import com.trafficviolationdriver.webservice.JSONCallback;
import com.trafficviolationdriver.webservice.Retrofit;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.UUID;

import static com.trafficviolationdriver.util.TimeStamp.DateFormatFullMonth;
import static com.trafficviolationdriver.util.TimeStamp.DateFormatGTS;

/**
 * Created by viraj.patel on 26-Apr-19
 */
public class FirstTimeMidnightDirectoryCleanerWorker extends Worker {

    private static final String TAG = CleanDirectoriesWorker.class.getSimpleName();
    private Context mContext;
    private SessionManager session;

    public FirstTimeMidnightDirectoryCleanerWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        mContext = context;
        session = new SessionManager(mContext);
    }

    @NonNull
    @Override
    public Result doWork() {

        Logger.e(TAG, "in doWork() method of directory worker");
        if (session.isLoggedIn() && session.getUserDetail().getUserId() != null) {
            try {

                // Clean Entries from Database
                AppDatabase appDatabase = AppDatabase.getAppDatabase(mContext);
                appDatabase.videoDao().removeAllVideos();
                appDatabase.eventDao().removeAllEventsExceptLast();
                appDatabase.configDao().resetRemainingMinutes(session.getUserDetail().getUserId());

                // Delete all stored Videos and Images
                Utils.cleanDirectoryData(mContext);

                String dataFlushTime = AppDatabase.getAppDatabase(mContext).configDao().getConfigData().getDeleteTime();
                Logger.e("Data Delete Time: ", "=> " + dataFlushTime);

                // Update time in Session Manager
                long lastUpdateTimestamp = TimeStamp.formatToSecondsLocal(TimeStamp.customDateFormat(System.currentTimeMillis() / 1000, DateFormatFullMonth).concat(" ").concat(dataFlushTime), DateFormatGTS);
                session.storeDataByKey(SessionManager.KEY_LAST_CONFIG_UPDATE, lastUpdateTimestamp);

                String workerUuid = session.getDataByKey(SessionManager.KEY_WORKER_UUID_UPLOAD_EVENT);
                if (!workerUuid.isEmpty()) {
                    WorkManager.getInstance().cancelWorkById(UUID.fromString(workerUuid));
                }

                API_getDriverConfig(String.valueOf(session.getUserDetail().getUserId()));

                FileUtil.setCleanDirectoryEverydayWorker(mContext);

                return Result.success();

            } catch (NumberFormatException e) {
                e.printStackTrace();
                return Result.retry();
            } catch (Throwable throwable) {
                // If there were errors, return FAILURE
                Log.e(TAG, "Error cleaning directories", throwable);
                return Result.retry();
            }
        }
        return Result.success();
    }

    private void API_getDriverConfig(String userId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", userId);

        try {
            Retrofit.with(mContext)
                    .setAPI(APIs.API_GET_DRIVER_CONFIGURATION)
                    .setParameters(params)
                    .setCallBackListener(new JSONCallback(mContext) {
                        @Override
                        protected void onSuccess(int statusCode, JSONObject jsonObject) {
                            if (jsonObject.optJSONObject("data") != null && jsonObject.optJSONObject("data").length() > 0) {

                                int targetEventCount = jsonObject.optJSONObject("data").optInt("targetEventCount");
                                int quotaMinutes = jsonObject.optJSONObject("data").optInt("recordingQuotaMinutes");
                                String deleteTime = jsonObject.optJSONObject("data").optString("dataDeleteTime");

                                AppDatabase.getAppDatabase(mContext).configDao().updateConfigData(session.getUserDetail().getUserId(), targetEventCount, (quotaMinutes * 60 * 1000), (quotaMinutes * 60 * 1000), deleteTime);

                                // If time is changed then cancel previous work and set new work manager
                                if (!AppDatabase.getAppDatabase(mContext).configDao().getConfigData().getDeleteTime().equals(deleteTime)) {
                                    String oneTimeWorkerUuid = session.getDataByKey(SessionManager.KEY_WORKER_UUID_FIRST_TIME_CLEAN_DIRECTORIES);
                                    String periodicWorkerUuid = session.getDataByKey(SessionManager.KEY_WORKER_UUID_CLEAN_DIRECTORIES);
                                    if (!oneTimeWorkerUuid.isEmpty()) {
                                        WorkManager.getInstance().cancelWorkById(UUID.fromString(oneTimeWorkerUuid));
                                    }
                                    if (!periodicWorkerUuid.isEmpty()) {
                                        WorkManager.getInstance().cancelWorkById(UUID.fromString(periodicWorkerUuid));
                                    }
                                    FileUtil.setFirstTimeCleanDirectoryWorker(mContext);
                                }
                            }
                        }

                        @Override
                        protected void onFailed(int statusCode, String message) {
                            Logger.e(message);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}