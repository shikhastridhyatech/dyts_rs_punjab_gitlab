package com.trafficviolationdriver.worker;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.trafficviolationdriver.db.AppDatabase;
import com.trafficviolationdriver.db.dao.LocationDao;
import com.trafficviolationdriver.db.entity.DriverLocationTableModel;
import com.trafficviolationdriver.interfaces.OnLocationDataUploadListener;
import com.trafficviolationdriver.util.Logger;
import com.trafficviolationdriver.util.SessionManager;
import com.trafficviolationdriver.webservice.APIs;
import com.trafficviolationdriver.webservice.JSONCallback;
import com.trafficviolationdriver.webservice.Retrofit;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class LocationDataWorker extends Worker {

    private static final String TAG = LocationDataWorker.class.getSimpleName();
    private Context mContext;
    private SessionManager session;

    public LocationDataWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        mContext = context;
        session = new SessionManager(mContext);
    }

    @NonNull
    @Override
    public Result doWork() {

        Logger.e(TAG, "in doWork() method");
        if (session.isLoggedIn() && session.getUserDetail().getUserId() != null) {
            try {
                // Update Value in Database
                AppDatabase appDatabase = AppDatabase.getAppDatabase(mContext);
                int count = appDatabase.locationDao().getRemainingUploadCount();

                if (count > 0) {
                    int videoId = appDatabase.locationDao().getRemainingUploadVideoId();
                    Logger.e("VideoId of LocationData: ", String.valueOf(videoId));

                    ArrayList<String> data = (ArrayList<String>) appDatabase.locationDao().getUploadDataArray(videoId);
                    Logger.e("DriverLocationData: ", data.toString());
                    API_saveLocationData(String.valueOf(session.getUserDetail().getUserId()), data.toString(), videoId, new OnLocationDataUploadListener() {
                        @Override
                        public Result onSuccess() {
                            // If there were no errors, return SUCCESS
                            return Result.success();
                        }

                        @Override
                        public Result onFailure() {
                            // If there were errors, return RETRY
                            return Result.retry();
                        }
                    });
                } else {
                    Logger.e("DriverLocationData: ", "No Location Data available to upload");
                }

            } catch (NumberFormatException e) {
                e.printStackTrace();
                return Result.retry();
            } catch (Throwable throwable) {
                // If there were errors, return FAILURE
                Log.e(TAG, "Error increasing day", throwable);
                return Result.failure();
            }
        }
        return Result.success();
    }

    private void API_saveLocationData(String userId, String locationArray, int videoId, OnLocationDataUploadListener mListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", userId);
        params.put("locationArray", locationArray);
        LocationDao mLocationDao = AppDatabase.getAppDatabase(mContext).locationDao();
        try {
            Retrofit.with(mContext)
                    .setParameters(params)
                    .setAPI(APIs.API_UPLOAD_LOCATION_DATA)
                    .setCallBackListener(new JSONCallback(mContext) {
                        @Override
                        protected void onSuccess(int statusCode, JSONObject jsonObject) {

                            mLocationDao.updateLocationSyncStatus(videoId);
                            Logger.e("Update Sync of VideoId: ", String.valueOf(videoId));

                            ArrayList<DriverLocationTableModel> driverLocation = (ArrayList<DriverLocationTableModel>) mLocationDao.getAllLocations();
                            Logger.e(driverLocation.toString());

                            if (jsonObject.optJSONObject("status_code") != null) {
                                try {
                                    int newVideoId = mLocationDao.getRemainingUploadVideoId();
                                    Logger.e("NewVideoId of LocationData: ", String.valueOf(videoId));
                                    ArrayList<String> data = (ArrayList<String>) mLocationDao.getUploadDataArray(newVideoId);
                                    if (data.size() > 0) {
                                        API_saveLocationData(userId, data.toString(), newVideoId, mListener);
                                    } else {
                                        if (mListener != null) {
                                            mListener.onSuccess();
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    mListener.onFailure();
                                }
                            } else {
                                if (mListener != null) {
                                    mListener.onFailure();
                                }
                            }
                        }

                        @Override
                        protected void onFailed(int statusCode, String message) {
                            if (mListener != null) {
                                mListener.onFailure();
                            }
                        }
                    });
        } catch (Exception e) {
            if (mListener != null) {
                mListener.onFailure();
            }
        }
    }
}
