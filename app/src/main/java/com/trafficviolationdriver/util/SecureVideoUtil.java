package com.trafficviolationdriver.util;

import android.content.Context;
import android.os.AsyncTask;

import com.trafficviolationdriver.R;
import com.trafficviolationdriver.interfaces.OnVideoEncryptionDecryptionListener;
import com.trafficviolationdriver.ui.base.ProgressDialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by viraj.patel on 29-May-19
 */
public class SecureVideoUtil {

    public static class SecureVideoException extends RuntimeException {
        SecureVideoException(Throwable e) {
            super(e);
        }
    }

    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
    private static final String KEY_TRANSFORMATION = "AES/ECB/PKCS5Padding";
    private static final String SECRET_KEY_HASH_TRANSFORMATION = "SHA-256";
    private static final String CHARSET = "UTF-8";
    private final String secureKey = "GDLsg@$313DGsds4064!ewr$#llda45";

    private final Cipher writer;
    private final Cipher reader;
    private final Cipher keyWriter;

    private static SecureVideoUtil instance = null;

    private SecureVideoUtil() throws SecureVideoException {

        try {
            this.writer = Cipher.getInstance(TRANSFORMATION);
            this.reader = Cipher.getInstance(TRANSFORMATION);
            this.keyWriter = Cipher.getInstance(KEY_TRANSFORMATION);

            initCiphers();

        } catch (GeneralSecurityException | UnsupportedEncodingException e) {
            throw new SecureVideoException(e);
        }
    }

    public static SecureVideoUtil getInstance() {
        if (instance == null) {
            instance = new SecureVideoUtil();
        }
        return instance;
    }

    private void initCiphers() throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException {
        IvParameterSpec ivSpec = getIv();
        SecretKeySpec secretKey = getSecretKey();

        writer.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec);
        reader.init(Cipher.DECRYPT_MODE, secretKey, ivSpec);
        keyWriter.init(Cipher.ENCRYPT_MODE, secretKey);
    }

    private IvParameterSpec getIv() {
        byte[] iv = new byte[writer.getBlockSize()];
        System.arraycopy("dhjtrfodasjifudslfjdsaofshaufidsktr".getBytes(), 0, iv, 0, writer.getBlockSize());
        return new IvParameterSpec(iv);
    }

    private SecretKeySpec getSecretKey() throws UnsupportedEncodingException, NoSuchAlgorithmException {
        byte[] keyBytes = createKeyBytes();
        return new SecretKeySpec(keyBytes, TRANSFORMATION);
    }

    private byte[] createKeyBytes() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance(SECRET_KEY_HASH_TRANSFORMATION);
        md.reset();
        return md.digest(secureKey.getBytes(CHARSET));
    }

    public synchronized void doEncryption(String filePath, String outputFile, OnVideoEncryptionDecryptionListener mListener) {
//        ProgressDialog mProgressDialog = new ProgressDialog(mContext, mContext.getResources().getString(R.string.please_wait));
        new SecureVideoUtil.EncryptVideo(filePath, outputFile, writer, mListener).execute();
    }

    private static class EncryptVideo extends AsyncTask<Void, Integer, String> {

        String filePath;
        String outputFile;
        //        ProgressDialog mProgressDialog;
        Cipher writer;
        OnVideoEncryptionDecryptionListener mListener;

        EncryptVideo(String filePath, String outputFile, Cipher writer, OnVideoEncryptionDecryptionListener listener) {
            this.filePath = filePath;
            this.outputFile = outputFile;
//            this.mProgressDialog = progressDialog;
            this.writer = writer;
            this.mListener = listener;
        }

        @Override
        protected String doInBackground(Void... voids) {
            InputStream in = null;
            OutputStream out = null;
            try {
                in = new FileInputStream(filePath);
                out = new FileOutputStream(outputFile);
                // byte[] iv = new byte[] { (byte) 0x8E, 0x12, 0x39, (byte) 0x9C,
                // 0x07, 0x72, 0x6F, 0x5A, (byte) 0x8E, 0x12, 0x39, (byte) 0x9C,
                // 0x07, 0x72, 0x6F, 0x5A };
                //generate new AlgorithmParameterSpec
                // AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
                out = new CipherOutputStream(out, writer);
                long total = 0;
                int fileSize = Integer.parseInt(String.valueOf(new File(filePath).length()));
                int count, previous = 0, present;
                byte[] buffer = new byte[1024];
                while ((count = in.read(buffer)) >= 0) {
                    total += count;
//                    publishProgress((int) ((total * 100) / fileSize));
                    present = (int) ((total * 100) / fileSize);
                    if (mListener != null && (present > previous)) {
                        previous = present;
                        mListener.onProgressUpdate(present);
                    }
                    out.write(buffer, 0, count);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                if (mListener != null) {
                    mListener.onVideoOperationFailed(e.getMessage());
                }
            } catch (IOException e) {
                e.printStackTrace();
                if (mListener != null) {
                    mListener.onVideoOperationFailed(e.getMessage());
                }
            } finally {
                boolean isSuccess = true;
                String failureMessage = "Video encryption failed";
                if (out != null) {
                    try {
                        out.flush();
                        out.close();
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        isSuccess = false;
                        failureMessage = e.getMessage();
                    }
                }
                if (mListener != null) {
                    if (isSuccess) {
                        mListener.onVideoOperationSucceed();
                    } else {
                        mListener.onVideoOperationFailed(failureMessage);
                    }
                }
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            /*mProgressDialog.updateText("100% Completed");
            mProgressDialog.dismiss();*/
            Logger.e("Video Encrypted Successfully");

            /*if (mListener != null) {
                mListener.onVideoOperationSucceed();
            }*/
        }

        @Override
        protected void onPreExecute() {
//            mProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
//            mProgressDialog.updateText(String.valueOf(values[0]).concat("% Completed"));
        }
    }

    public void encryptVideo(String filePath, String outputFile, OnVideoEncryptionDecryptionListener mListener) {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = new FileInputStream(filePath);
            out = new FileOutputStream(outputFile);
            // byte[] iv = new byte[] { (byte) 0x8E, 0x12, 0x39, (byte) 0x9C,
            // 0x07, 0x72, 0x6F, 0x5A, (byte) 0x8E, 0x12, 0x39, (byte) 0x9C,
            // 0x07, 0x72, 0x6F, 0x5A };
            //generate new AlgorithmParameterSpec
            // AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
            out = new CipherOutputStream(out, writer);
            long total = 0;
            int fileSize = Integer.parseInt(String.valueOf(new File(filePath).length()));
            int count, previous = 0, present;
            byte[] buffer = new byte[1024];
            while ((count = in.read(buffer)) >= 0) {
                total += count;
                present = (int) ((total * 100) / fileSize);
                if (mListener != null && (present > previous)) {
                    previous = present;
                    mListener.onProgressUpdate(present);
                }
                out.write(buffer, 0, count);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            if (mListener != null) {
                mListener.onVideoOperationFailed(e.getMessage());
            }
        } catch (IOException e) {
            e.printStackTrace();
            if (mListener != null) {
                mListener.onVideoOperationFailed(e.getMessage());
            }
        } finally {
            boolean isSuccess = true;
            String failureMessage = "Video encryption failed";
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    isSuccess = false;
                    failureMessage = e.getMessage();
                }
            }
            if (mListener != null) {
                if (isSuccess) {
                    mListener.onVideoOperationSucceed();
                } else {
                    mListener.onVideoOperationFailed(failureMessage);
                }
            }
        }
    }

    public void doDecryption(Context mContext, String filePath, File outputFile, OnVideoEncryptionDecryptionListener mListener) {
        ProgressDialog mProgressDialog = new ProgressDialog(mContext, mContext.getResources().getString(R.string.please_wait));
        new SecureVideoUtil.DecryptVideo(filePath, outputFile, mProgressDialog, reader, mListener).execute();
    }

    private static class DecryptVideo extends AsyncTask<Void, Integer, String> {

        String filePath;
        File outputFile;
        ProgressDialog mProgressDialog;
        Cipher reader;
        OnVideoEncryptionDecryptionListener mListener;

        DecryptVideo(String filePath, File outputFile, ProgressDialog progressDialog, Cipher reader, OnVideoEncryptionDecryptionListener listener) {
            this.filePath = filePath;
            this.outputFile = outputFile;
            this.mProgressDialog = progressDialog;
            this.reader = reader;
            this.mListener = listener;
        }

        @Override
        protected String doInBackground(Void... voids) {
            InputStream in = null;
            OutputStream out = null;
            try {
                in = new FileInputStream(filePath);
                out = new FileOutputStream(outputFile);
                // byte[] iv = new byte[] { (byte) 0x8E, 0x12, 0x39, (byte) 0x9C,
                // 0x07, 0x72, 0x6F, 0x5A, (byte) 0x8E, 0x12, 0x39, (byte) 0x9C,
                // 0x07, 0x72, 0x6F, 0x5A };
                // read from input stream AlgorithmParameterSpec
                // AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
                out = new CipherOutputStream(out, reader);
                long total = 0;
                int fileSize = Integer.parseInt(String.valueOf(new File(filePath).length()));
                int count, previous = 0, present;
                byte[] buffer = new byte[1024];
                while ((count = in.read(buffer)) >= 0) {
                    total += count;
                    present = (int) ((total * 100) / fileSize);
                    if (mListener != null && (present > previous)) {
                        previous = present;
                        publishProgress(present);
                    }
                    out.write(buffer, 0, count);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                if (mListener != null) {
                    mListener.onVideoOperationFailed(e.getMessage());
                }
            } catch (IOException e) {
                e.printStackTrace();
                if (mListener != null) {
                    mListener.onVideoOperationFailed(e.getMessage());
                }
            } finally {
                mProgressDialog.dismiss();
                if (out != null) {
                    try {
                        out.flush();
                        out.close();
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        if (mListener != null) {
                            mListener.onVideoOperationFailed(e.getMessage());
                        }
                    }
                }
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            mProgressDialog.updateText("100% Completed");
            mProgressDialog.dismiss();
            Logger.e("Video Decrypted Successfully");
            if (mListener != null) {
                mListener.onVideoOperationSucceed();
            }
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            mProgressDialog.updateText(String.valueOf(values[0]).concat("% Completed"));
        }
    }
}
