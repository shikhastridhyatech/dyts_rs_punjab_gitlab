package com.trafficviolationdriver.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Base64;

import com.trafficviolationdriver.R;
import com.trafficviolationdriver.ui.base.ProgressDialog;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class SecurePreferences {

    public static class SecurePreferencesException extends RuntimeException {

        public SecurePreferencesException(Throwable e) {
            super(e);
        }
    }

    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
    private static final String KEY_TRANSFORMATION = "AES/ECB/PKCS5Padding";
    private static final String SECRET_KEY_HASH_TRANSFORMATION = "SHA-256";
    private static final String CHARSET = "UTF-8";

    private final boolean encryptKeys;
    private final Cipher writer;
    private final Cipher reader;
    private final Cipher keyWriter;
    private final SharedPreferences preferences;
    private static SecurePreferences instance = null;

    public SecurePreferences(Context context, String preferenceName,
                             String secureKey, boolean encryptKeys)
            throws SecurePreferencesException {
        try {
            this.writer = Cipher.getInstance(TRANSFORMATION);
            this.reader = Cipher.getInstance(TRANSFORMATION);
            this.keyWriter = Cipher.getInstance(KEY_TRANSFORMATION);

            initCiphers(secureKey);

            this.preferences = context.getSharedPreferences(preferenceName,
                    Context.MODE_PRIVATE);

            this.encryptKeys = encryptKeys;
        } catch (GeneralSecurityException | UnsupportedEncodingException e) {
            throw new SecurePreferencesException(e);
        }
    }

    public static SecurePreferences getInstance(Context contxt, String prefName) {
        if (instance == null) {
            instance = new SecurePreferences(contxt, prefName,
                    prefName + "Key", true);
        }
        return instance;
    }

    private void initCiphers(String secureKey)
            throws UnsupportedEncodingException, NoSuchAlgorithmException,
            InvalidKeyException, InvalidAlgorithmParameterException {
        IvParameterSpec ivSpec = getIv();
        SecretKeySpec secretKey = getSecretKey(secureKey);

        writer.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec);
        reader.init(Cipher.DECRYPT_MODE, secretKey, ivSpec);
        keyWriter.init(Cipher.ENCRYPT_MODE, secretKey);
    }

    private IvParameterSpec getIv() {
        byte[] iv = new byte[writer.getBlockSize()];
        System.arraycopy("dhjtrfodasjifudslfjdsaofshaufidsktr".getBytes(), 0,
                iv, 0, writer.getBlockSize());
        return new IvParameterSpec(iv);
    }

    private SecretKeySpec getSecretKey(String key)
            throws UnsupportedEncodingException, NoSuchAlgorithmException {
        byte[] keyBytes = createKeyBytes(key);
        return new SecretKeySpec(keyBytes, TRANSFORMATION);
    }

    private byte[] createKeyBytes(String key)
            throws UnsupportedEncodingException, NoSuchAlgorithmException {
        MessageDigest md = MessageDigest
                .getInstance(SECRET_KEY_HASH_TRANSFORMATION);
        md.reset();
        return md.digest(key.getBytes(CHARSET));
    }

    public void putString(String key, String value) {
        if (value == null) {
            preferences.edit().remove(toKey(key)).apply();
        } else {
            putValue(toKey(key), value);
        }
    }

    public void putBoolean(String key, Boolean value) {
        if (value == null) {
            preferences.edit().remove(toKey(key)).apply();
        } else {
            putValue(toKey(key), Boolean.toString(value));
        }
    }

    public void putLong(String key, long value) {

        putValue(toKey(key), Long.toString(value));

    }

    public void putInt(String key, int value) {

        putValue(toKey(key), Integer.toString(value));

    }

    public boolean containsKey(String key) {
        return preferences.contains(toKey(key));
    }

    public void removeValue(String key) {
        preferences.edit().remove(toKey(key)).apply();
    }

    public String getString(String key, String value)
            throws SecurePreferencesException {
        if (preferences.contains(toKey(key))) {
            String securedEncodedValue = preferences.getString(toKey(key), "");
            return decrypt(securedEncodedValue);
        }
        return value;
    }

    public long getLong(String key, long value)
            throws SecurePreferencesException {
        if (preferences.contains(toKey(key))) {
            String securedEncodedValue = preferences.getString(toKey(key), "");
            return Long.parseLong(decrypt(securedEncodedValue));
        }
        return value;
    }

    public boolean getBoolean(String key, boolean value)
            throws SecurePreferencesException {
        if (preferences.contains(toKey(key))) {
            String securedEncodedValue = preferences.getString(toKey(key), "");
            return Boolean.parseBoolean(decrypt(securedEncodedValue));
        }
        return value;
    }

    public int getInt(String key, int value) throws SecurePreferencesException {
        if (preferences.contains(toKey(key))) {
            String securedEncodedValue = preferences.getString(toKey(key), "");
            return Integer.parseInt(decrypt(securedEncodedValue));
        }
        return value;
    }

    public void commit() {
        preferences.edit().apply();
    }

    public void clear() {
        preferences.edit().clear().apply();
    }

    private String toKey(String key) {
        if (encryptKeys)
            return encrypt(key, keyWriter);
        else
            return key;
    }

    private void putValue(String key, String value)
            throws SecurePreferencesException {
        String secureValueEncoded = encrypt(value, writer);

        preferences.edit().putString(key, secureValueEncoded).apply();
    }

    private String encrypt(String value, Cipher writer)
            throws SecurePreferencesException {
        byte[] secureValue;
        try {
            secureValue = convert(writer, value.getBytes(CHARSET));
        } catch (UnsupportedEncodingException e) {
            throw new SecurePreferencesException(e);
        }
        return Base64.encodeToString(secureValue,
                Base64.NO_WRAP);
    }

    private String decrypt(String securedEncodedValue) {
        byte[] securedValue = Base64
                .decode(securedEncodedValue, Base64.NO_WRAP);
        byte[] value = convert(reader, securedValue);
        try {
            return new String(value, CHARSET);
        } catch (UnsupportedEncodingException e) {
            throw new SecurePreferencesException(e);
        }
    }

    private static byte[] convert(Cipher cipher, byte[] bs)
            throws SecurePreferencesException {
        try {
            return cipher.doFinal(bs);
        } catch (Exception e) {
            throw new SecurePreferencesException(e);
        }
    }

    public void doEncryption(Context mContext, String filePath, File outputFile) {
        new EncryptVideo(mContext, filePath, outputFile).execute("");
    }

    private class EncryptVideo extends AsyncTask<String, Integer, String> {

        Context mContext;
        String filePath;
        File outputFile;
        ProgressDialog mProgressDialog;

        EncryptVideo(Context context, String filePath, File outputFile) {
            this.mContext = context;
            this.filePath = filePath;
            this.outputFile = outputFile;
        }

        @Override
        protected String doInBackground(String... params) {
            InputStream in = null;
            OutputStream out = null;
            try {
                in = new FileInputStream(filePath);
                out = new FileOutputStream(outputFile);
                // byte[] iv = new byte[] { (byte) 0x8E, 0x12, 0x39, (byte) 0x9C,
                // 0x07, 0x72, 0x6F, 0x5A, (byte) 0x8E, 0x12, 0x39, (byte) 0x9C,
                // 0x07, 0x72, 0x6F, 0x5A };
                //generate new AlgorithmParameterSpec
                // AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
                out = new CipherOutputStream(out, writer);
                long total = 0;
                int fileSize = Integer.parseInt(String.valueOf(new File(filePath).length()));
                int count = 0;
                byte[] buffer = new byte[1024];
                while ((count = in.read(buffer)) >= 0) {
                    total += count;
                    publishProgress((int) ((total * 100) / fileSize));
                    out.write(buffer, 0, count);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {

                if (out != null) {
                    try {
                        out.flush();
                        out.close();
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            mProgressDialog.updateText("100% Completed");
            mProgressDialog.dismiss();
            Logger.e("Video Encrypted Successfully");
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog = new ProgressDialog(mContext, mContext.getResources().getString(R.string.please_wait));
            mProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            mProgressDialog.updateText(String.valueOf(values[0]).concat("% Completed"));
        }
    }

    @SuppressWarnings("resource")
    public void encrypt(String filePath, File outputFile) {

        InputStream in = null;
        OutputStream out = null;
        try {
            in = new FileInputStream(filePath);
            out = new FileOutputStream(outputFile);
            // byte[] iv = new byte[] { (byte) 0x8E, 0x12, 0x39, (byte) 0x9C,
            // 0x07, 0x72, 0x6F, 0x5A, (byte) 0x8E, 0x12, 0x39, (byte) 0x9C,
            // 0x07, 0x72, 0x6F, 0x5A };
            //generate new AlgorithmParameterSpec
            // AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
            out = new CipherOutputStream(out, writer);
            int count = 0;
            long total = 0;
            byte[] buffer = new byte[1024];
            while ((count = in.read(buffer)) >= 0) {
                total += count;

                out.write(buffer, 0, count);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            if (out != null) {
                try {
                    out.flush();
                    out.close();
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @SuppressWarnings("resource")
    public void decrypt(String filePath, File outputFile) {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = new FileInputStream(filePath);
            out = new FileOutputStream(outputFile);
            // byte[] iv = new byte[] { (byte) 0x8E, 0x12, 0x39, (byte) 0x9C,
            // 0x07, 0x72, 0x6F, 0x5A, (byte) 0x8E, 0x12, 0x39, (byte) 0x9C,
            // 0x07, 0x72, 0x6F, 0x5A };
            // read from input stream AlgorithmParameterSpec
            // AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
            out = new CipherOutputStream(out, reader);
            int count = 0;
            byte[] buffer = new byte[1024];
            while ((count = in.read(buffer)) >= 0) {
                out.write(buffer, 0, count);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public File getEncryptedFile(String filePath, File outputFile) {

        try {

            FileInputStream in = new FileInputStream(new File(filePath));
            FileOutputStream out = new FileOutputStream(outputFile);

            CipherInputStream cis = new CipherInputStream(in, reader);

            byte[] bytes = new byte[2048];
            int nread;
            while ((nread = cis.read(bytes)) > 0) {
                byte[] enc = writer.update(bytes, 0, nread);
                out.write(enc);
            }
            byte[] enc = writer.doFinal();
            out.write(enc);

            out.flush();
            out.close();
            cis.close();


            /*BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();*/
        } catch (FileNotFoundException | IllegalBlockSizeException | BadPaddingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return outputFile;
    }

    private File encryptFile(byte[] value, Cipher writer, File mFile)
            throws SecurePreferencesException {
        byte[] secureValue;
        secureValue = convert(writer, value);
        try {
            writeToFile(Base64.encode(secureValue, Base64.NO_WRAP), mFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mFile;
    }

    private void writeToFile(byte[] data, File mEncodedFile) throws IOException {
        FileOutputStream out = new FileOutputStream(mEncodedFile);
        out.write(data);
        out.close();
    }

    public File getDecryptedFile(String filePath, File outputFile) {

        File file = new File(filePath);
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return decryptFile(bytes, outputFile);
    }

    private File decryptFile(byte[] encodedFileValue, File mDecodedFile) {
        byte[] securedValue = Base64.decode(encodedFileValue, Base64.NO_WRAP);
        byte[] value = convert(reader, securedValue);
        try {
            writeToFile(value, mDecodedFile);
        } catch (UnsupportedEncodingException e) {
            throw new SecurePreferencesException(e);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mDecodedFile;
    }

    /*public File encryptFileNew(File f, byte[] key) throws Exception {

        System.out.println("Starting Encryption");
        SecretKeySpec secretKey = new SecretKeySpec(key, ALGORITHM);
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);

        System.out.println(plainText.length);

        Path outPath = Paths.get("D:/Temp");
        byte[] plainBuf = new byte[8192];
        try (InputStream in = Files.newInputStream(f.toPath());
             OutputStream out = Files.newOutputStream(outPath)) {
            int nread;
            while ((nread = in.read(plainBuf)) > 0) {
                byte[] enc = cipher.update(plainBuf, 0, nread);
                out.write(enc);
            }
            byte[] enc = cipher.doFinal();
            out.write(enc);
        }
        return outPath.toFile();
    }*/

}