package com.trafficviolationdriver.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;

import com.trafficviolationdriver.BuildConfig;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.dialogs.MessageDialog;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Locale;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import static org.apache.commons.codec.binary.Hex.encodeHex;

public class Utils {

    public static void setLocale(Context context, String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }

    public static void hideSoftKeyboard(Activity me, EditText et) {
        try {
            InputMethodManager imm = (InputMethodManager) me.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String encode(String data) {
        Mac sha256_HMAC;
        String s = "";
        try {
            sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec("}{(*%$}&*^\\({#$%&@}^*$#{?}@^%#)(^%&".getBytes("UTF-8"), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte digest[] = sha256_HMAC.doFinal(data.getBytes("UTF-8"));
            s = new String(encodeHex(digest));
        } catch (NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return s;
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivityManager.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network mNetwork : networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                    Logger.d("Network", "NETWORK NAME: " + networkInfo.getTypeName());
                    return true;
                }
            }
        } else {
            if (connectivityManager != null) {
                NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                if (info != null) {
                    for (NetworkInfo anInfo : info) {
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            Logger.d("Network", "NETWORK NAME: " + anInfo.getTypeName());
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static void InternetFailureDialog(Context context) {
        new MessageDialog(context).setMessage(context.getString(R.string.no_internet_connection)).setEvent(true, context.getResources().getDrawable(R.drawable.ic_selected_check_with_green_circle)).isAutoDismiss(true).cancelable(true).show();

    }


    //Image Real Path from URI
    public static String getRealPathFromURI(Context mContext, Uri contentURI) {
        String result;
//        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        String[] filePathColumn = {MediaStore.Images.ImageColumns.DATA};
        Cursor cursor = mContext.getContentResolver().query(contentURI, filePathColumn, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(filePathColumn[0]);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public static void setCurrentDate(AppCompatTextView tvDay, AppCompatTextView tvDate) {
        String[] strDate = TimeStamp.getCurrentDateElements();
        tvDay.setText(strDate[0]);
        tvDate.setText(strDate[1]);
    }

    public static void setVectorForPreLollipop(TextView textView, int resourceId, Context activity, int position) {
        Drawable icon;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            icon = VectorDrawableCompat.create(activity.getResources(), resourceId, activity.getTheme());
        } else {
            icon = activity.getResources().getDrawable(resourceId, activity.getTheme());
        }
        switch (position) {
            case AppConstants.DRAWABLE_LEFT:
                textView.setCompoundDrawablesWithIntrinsicBounds(icon, null, null, null);
                break;

            case AppConstants.DRAWABLE_RIGHT:
                textView.setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null);
                break;

            case AppConstants.DRAWABLE_TOP:
                textView.setCompoundDrawablesWithIntrinsicBounds(null, icon, null, null);
                break;

            case AppConstants.DRAWABLE_BOTTOM:
                textView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, icon);
                break;
        }
    }

    public static String getVideoFolder(Context me) {
        try {
            File fileDir = new File(Environment.getExternalStorageDirectory() + String.format(me.getString(R.string.hidden_storage_folder), me.getString(R.string.app_name)));
            if (!fileDir.exists()) {
                fileDir.mkdirs();
            }

            File fileDirChild = new File(fileDir, me.getString(R.string.videos_folder));
            if (!fileDirChild.exists()) {
                fileDirChild.mkdirs();
            }
            return fileDirChild.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getTempVideoFolder(Context me) {
        try {
            File fileDir = new File(Environment.getExternalStorageDirectory() + String.format(me.getString(R.string.hidden_storage_folder), me.getString(R.string.app_name)));
            if (!fileDir.exists()) {
                fileDir.mkdirs();
            }

            File fileDirChild = new File(fileDir, me.getString(R.string.videos_folder));
            if (!fileDirChild.exists()) {
                fileDirChild.mkdirs();
            }

            File TempVideoDir = new File(fileDirChild, me.getString(R.string.temp_videos_folder));
            if (!TempVideoDir.exists()) {
                TempVideoDir.mkdirs();
            }
            return TempVideoDir.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getEventMainFolder(Context me) {
        try {
            File fileDir = new File(Environment.getExternalStorageDirectory() + String.format(me.getString(R.string.hidden_storage_folder), me.getString(R.string.app_name)));
            if (!fileDir.exists()) {
                fileDir.mkdirs();
            }

            File fileDirChild = new File(fileDir, me.getString(R.string.events_folder));
            if (!fileDirChild.exists()) {
                fileDirChild.mkdirs();
            }

            return fileDirChild.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getEventFolder(Context me, int eventId) {
        try {
            File fileDir = new File(Environment.getExternalStorageDirectory() + String.format(me.getString(R.string.hidden_storage_folder), me.getString(R.string.app_name)));
            if (!fileDir.exists()) {
                fileDir.mkdirs();
            }

            File fileDirChild = new File(fileDir, me.getString(R.string.events_folder));
            if (!fileDirChild.exists()) {
                fileDirChild.mkdirs();
            }

            File fileDirInnerChild = new File(fileDirChild, String.valueOf(eventId));
            if (!fileDirInnerChild.exists()) {
                fileDirInnerChild.mkdirs();
            }
            return fileDirInnerChild.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static File createNewFile(Context me, boolean videoFile, boolean encypted, boolean numberPlateImage, int userOrEventId) {
        return createNewFile(me, videoFile, false, encypted, numberPlateImage, userOrEventId);
    }

    public static File createNewFile(Context me, boolean videoFile, boolean isCompressedVideo, boolean encypted, boolean numberPlateImage, int userOrEventId) {
        String path = "", extension = "", fileName = "", filePath = "";
        File file = null;
        boolean ifEvent;
        try {
            if (videoFile) {
                path = path + me.getString(R.string.video_file_name);
                extension = me.getString(R.string.video_format);

                if (encypted) {
                    ifEvent = false;
                    path = path + me.getString(R.string.separator) + me.getString(R.string.encrypted_video_name);
                } else {
                    ifEvent = true;
                    path = path + me.getString(R.string.separator) + me.getString(isCompressedVideo ? R.string.compressed : R.string.cropped);
                }
            } else {
                ifEvent = true;
                path = path + me.getString(R.string.image_file_name);
                extension = me.getString(R.string.image_format);

                if (numberPlateImage) {
                    path = path + me.getString(R.string.separator) + me.getString(R.string.numberplate_image);
                } else {
                    path = path + me.getString(R.string.separator) + me.getString(R.string.cropped);
                }
            }
            fileName = path + me.getString(R.string.separator) + userOrEventId + me.getString(R.string.separator) + System.currentTimeMillis() + extension;

            if (ifEvent) {
                filePath = Utils.getEventFolder(me, userOrEventId);
            } else {
                filePath = Utils.getVideoFolder(me);
            }
            file = new File(filePath, fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    public static void deleteVideo(String videoPath) {

        File fileDir = new File(videoPath);
        if (fileDir.exists()) {
            try {
                Logger.e("VideoPath: ", "=>" + videoPath);
                FileUtils.forceDelete(fileDir);
                Logger.e("Deleted Video Name: ", "=>" + fileDir.getName());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void deleteEventData(Context context, int eventId) {
        File dir = new File(getEventFolder(context, eventId));
        try {
            Logger.e("Delete Event Directory: ", "=>" + eventId);
            FileUtils.deleteDirectory(dir);
            Logger.e("Delete Event Directory: ", "=>" + dir.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void cleanDirectoryData(Context context) {
        File eventDir = new File(getEventMainFolder(context));
        File videoDir = new File(getVideoFolder(context));
        try {
            FileUtils.cleanDirectory(eventDir);
            FileUtils.cleanDirectory(videoDir);
            Logger.e("Clean Event Directory: ", "=>" + eventDir.getName());
            Logger.e("Clean Video Directory: ", "=>" + videoDir.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void cleanTempDirectoryData(Context context) {
        File tempVideoDir = new File(getTempVideoFolder(context));
        try {
            FileUtils.cleanDirectory(tempVideoDir);
            Logger.e("Clean Temp Video Directory: ", "=>" + tempVideoDir.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static boolean isAppRunning(final Context context) {

        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (activityManager != null) {
            final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
            if (procInfos != null) {
                for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                    if (processInfo.processName.equals(BuildConfig.APPLICATION_ID)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean isServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}