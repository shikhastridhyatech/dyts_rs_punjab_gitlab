package com.trafficviolationdriver.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.trafficviolationdriver.ApplicationClass;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.model.UserDataModel;
import com.trafficviolationdriver.ui.login.LoginActivity;


public class SessionManager {

    public static final String KEY_WORKER_UUID_LOCATION = "worker_uuid_location";
    public static final String KEY_WORKER_UUID_FIRST_TIME_CLEAN_DIRECTORIES = "worker_uuid_first_time_clean_directories";
    public static final String KEY_WORKER_UUID_CLEAN_DIRECTORIES = "worker_uuid_clean_directories";
    public static final String KEY_WORKER_UUID_UPLOAD_EVENT = "worker_uuid_upload_event";
    public static final String KEY_USER_STATE_FLAG = "user_state_flag";

    public static final String KEY_LANGUAGE = "languageCode";
    public static final String KEY_LAST_CONFIG_UPDATE = "last_config_update";
    public static final String IS_LOGIN = "is_login";
    private static final String KEY_USER_MODEL = "user_model";
    private static final String KEY_PREF = "KEY_PREF";

    private SecurePreferences pref;
    private Context mContext;

    public SessionManager(Context context) {
        String PREF_NAME = context.getResources().getString(R.string.app_name);
        pref = SecurePreferences.getInstance(context, PREF_NAME);
        mContext = context;
    }

    public void storeUserDetail(UserDataModel userModel) {
        storeUserDetail(userModel, true);
    }

    public void storeUserDetail(UserDataModel userModel, boolean isLogin) {
        Gson gson = new Gson();
        String json = gson.toJson(userModel); // myObject - instance of UserModel
        pref.putString(KEY_USER_MODEL, json);
        pref.putBoolean(IS_LOGIN, isLogin);
        pref.commit();
    }

    public UserDataModel getUserDetail() {
        Gson gson = new Gson();
        String json = pref.getString(KEY_USER_MODEL, null);
        return gson.fromJson(json, UserDataModel.class);
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public String getDataByKey(String Key) {
        return getDataByKey(Key, "");
    }

    public long getLongDataByKey(String Key, long DefaultValue) {
        if (pref.containsKey(Key)) {
            return pref.getLong(Key, DefaultValue);
        } else {
            return DefaultValue;
        }
    }

    public String getDataByKey(String Key, String DefaultValue) {
        String returnValue;
        if (pref.containsKey(Key)) {
            returnValue = pref.getString(Key, DefaultValue);
        } else {
            returnValue = DefaultValue;
        }
        return returnValue;
    }

    public Boolean getDataByKey(String Key, boolean DefaultValue) {
        if (pref.containsKey(Key)) {
            return pref.getBoolean(Key, DefaultValue);
        } else {
            return DefaultValue;
        }
    }

    public int getDataByKey(String Key, int DefaultValue) {
        if (pref.containsKey(Key)) {
            return pref.getInt(Key, DefaultValue);
        } else {
            return DefaultValue;
        }
    }

    public void storeDataByKey(String key, int Value) {
        pref.putInt(key, Value);
        pref.commit();
    }

    public void storeDataByKey(String key, String Value) {
        pref.putString(key, Value);
        pref.commit();
    }

    public void storeDataByKey(String key, boolean Value) {
        pref.putBoolean(key, Value);
        pref.commit();
    }

    public void storeDataByKey(String key, long Value) {
        pref.putLong(key, Value);
        pref.commit();
    }

    public void clearDataByKey(String key) {
        pref.removeValue(key);
    }

    // region Helper Methods
    private static SharedPreferences.Editor getEditor(Context context) {
        SharedPreferences preferences = getSharedPreferences(context);
        return preferences.edit();
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(KEY_PREF, Context.MODE_PRIVATE);
    }

    // Reset User Session
    public void logoutUser(Context context) {
        storeDataByKey(SessionManager.IS_LOGIN, false);
        storeDataByKey(SessionManager.KEY_USER_MODEL, null);
        SharedPreferences.Editor editor = getEditor(context);
        editor.remove(KEY_USER_MODEL).apply();

        // Canceling Periodic and One Time Worker if any
        FileUtil.cancelWorkByUUID(context);
        LocationUtil.cancelWorkByUUID(context);

        if (ApplicationClass.isAppRunning) {
            // Redirect to Welcome Screen
            Intent intent = new Intent(context, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
        }
    }
}
