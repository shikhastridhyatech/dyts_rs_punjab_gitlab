package com.trafficviolationdriver.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;

import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.trafficviolationdriver.db.AppDatabase;
import com.trafficviolationdriver.worker.CleanDirectoriesWorker;
import com.trafficviolationdriver.worker.FirstTimeMidnightDirectoryCleanerWorker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by viraj.patel on 26-Apr-19
 */
public class FileUtil {

    public static void setFirstTimeCleanDirectoryWorker(Context mContext) {

        SessionManager session = new SessionManager(mContext);
        String oneTimeWorkerUuid = session.getDataByKey(SessionManager.KEY_WORKER_UUID_FIRST_TIME_CLEAN_DIRECTORIES);
        String periodicWorkerUuid = session.getDataByKey(SessionManager.KEY_WORKER_UUID_CLEAN_DIRECTORIES);
        if (oneTimeWorkerUuid.equals("") && periodicWorkerUuid.equals("")) {

            Calendar c = Calendar.getInstance();
            c.add(Calendar.DAY_OF_MONTH, 1);
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            long howMany = (c.getTimeInMillis() - System.currentTimeMillis());

            int remainingMinutes = (int) TimeUnit.MILLISECONDS.toMinutes(howMany);
            Logger.e("Remaining Minutes to 12 o'clock: ", "=> " + remainingMinutes);

            String dataFlushTime = AppDatabase.getAppDatabase(mContext).configDao().getConfigData().getDeleteTime();
            Logger.e("Data Delete Time: ", "=> " + dataFlushTime);

            int additionalMinutes = TimeStamp.getAdditionalMinutes(dataFlushTime, System.currentTimeMillis());
            Logger.e("String time to minutes: ", String.valueOf(additionalMinutes));

            int remainingMinutesAfterAdditionalMinutes = remainingMinutes - additionalMinutes;
            Logger.e("Remaining Minutes to Delete Video: ", "=> " + remainingMinutesAfterAdditionalMinutes);

            long lastUpdateTimestamp = TimeStamp.getLastUpdateTimestamp(dataFlushTime, System.currentTimeMillis());
            session.storeDataByKey(SessionManager.KEY_LAST_CONFIG_UPDATE, lastUpdateTimestamp);

            OneTimeWorkRequest.Builder firstDayWorkBuilder =
                    new OneTimeWorkRequest.Builder(FirstTimeMidnightDirectoryCleanerWorker.class)
                            .setInitialDelay(remainingMinutesAfterAdditionalMinutes, TimeUnit.MINUTES);

            // Add Tag to workBuilder
            firstDayWorkBuilder.addTag(AppConstants.DIRECTORY_DATA_DELETE_FIRST_TIME_WORK_NAME);
            // Create the actual work object:
            OneTimeWorkRequest firstDayWork = firstDayWorkBuilder.build();
            // Then enqueue the recurring task:
            WorkManager.getInstance().enqueue(firstDayWork);

            // Set UUID in session for future use
            UUID workerId = firstDayWork.getId();
            session.storeDataByKey(SessionManager.KEY_WORKER_UUID_FIRST_TIME_CLEAN_DIRECTORIES, workerId.toString());
            Logger.e("Save OneTime Worker UUID", workerId.toString());
        } else {
            Logger.e("Stored OneTime Worker UUID", oneTimeWorkerUuid);
            Logger.e("Stored Periodic Dir. Worker UUID", periodicWorkerUuid);
        }
    }

    public static void setCleanDirectoryEverydayWorker(Context mContext) {

        SessionManager session = new SessionManager(mContext);
        String workerUuid = session.getDataByKey(SessionManager.KEY_WORKER_UUID_CLEAN_DIRECTORIES);
        if (workerUuid.equals("")) {

            PeriodicWorkRequest.Builder dayWorkBuilder =
                    new PeriodicWorkRequest.Builder(CleanDirectoriesWorker.class, 24, TimeUnit.HOURS, 5,
                            TimeUnit.MINUTES);

            // Add Tag to workBuilder
            dayWorkBuilder.addTag(AppConstants.DIRECTORY_DATA_DELETE_WORK_NAME);
            // Create the actual work object:
            PeriodicWorkRequest dayWork = dayWorkBuilder.build();
            // Then enqueue the recurring task:
            WorkManager.getInstance().enqueue(dayWork);

            // Set UUID in session for future use
            UUID workerId = dayWork.getId();
            session.storeDataByKey(SessionManager.KEY_WORKER_UUID_CLEAN_DIRECTORIES, workerId.toString());
            Logger.e("Save Dir. Worker UUID", workerId.toString());
        } else {
            Logger.e("Stored Dir. Worker UUID", workerUuid);
        }
    }

    /**
     * Cancel work using the work's unique id
     */
    public static void cancelWorkByUUID(Context mContext) {
        SessionManager session = new SessionManager(mContext);
        String periodicWorkerUuid = session.getDataByKey(SessionManager.KEY_WORKER_UUID_CLEAN_DIRECTORIES);
        Logger.e("Canceling Dir. Worker UUID", periodicWorkerUuid);
        if (periodicWorkerUuid != null && periodicWorkerUuid.length() > 0) {
            WorkManager.getInstance().cancelWorkById(UUID.fromString(periodicWorkerUuid));
            WorkManager.getInstance().pruneWork();
        }

        String oneTimeWorkerUuid = session.getDataByKey(SessionManager.KEY_WORKER_UUID_FIRST_TIME_CLEAN_DIRECTORIES);
        Logger.e("Canceling Dir. Worker UUID", oneTimeWorkerUuid);
        if (oneTimeWorkerUuid != null && oneTimeWorkerUuid.length() > 0) {
            WorkManager.getInstance().cancelWorkById(UUID.fromString(oneTimeWorkerUuid));
            WorkManager.getInstance().pruneWork();
        }

        session.storeDataByKey(SessionManager.KEY_WORKER_UUID_CLEAN_DIRECTORIES, "");
        session.storeDataByKey(SessionManager.KEY_WORKER_UUID_FIRST_TIME_CLEAN_DIRECTORIES, "");
    }

    /**
     * Cancel work using the work's unique name
     */
    public static void cancelWorkByTag(String workerUdi) {
        WorkManager.getInstance().cancelAllWorkByTag(AppConstants.DIRECTORY_DATA_DELETE_WORK_NAME);
    }

    public static boolean storeImage(String videoPath, File pictureFile) {

        Bitmap image = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MINI_KIND);

        if (pictureFile == null) {
            Logger.e("Error creating media file, check storage permissions: ");// e.getMessage());
            return false;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Logger.e("File not found: " + e.getMessage());
            return false;
        } catch (IOException e) {
            Logger.e("Error accessing file: " + e.getMessage());
            return false;
        }
        return true;
    }
}
