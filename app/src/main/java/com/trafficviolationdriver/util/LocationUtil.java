package com.trafficviolationdriver.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.preference.PreferenceManager;

import com.trafficviolationdriver.R;
import com.trafficviolationdriver.worker.LocationDataWorker;

import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import androidx.annotation.RequiresApi;
import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

/**
 * Created by ${RICHA} on 08-04-2019.
 */
public class LocationUtil {
    public static final String KEY_REQUESTING_LOCATION_UPDATES = "requesting_locaction_updates";

    /**
     * Returns true if requesting location updates, otherwise returns false.
     *
     * @param context The {@link Context}.
     */
    public static boolean requestingLocationUpdates(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(KEY_REQUESTING_LOCATION_UPDATES, false);
    }

    /**
     * Stores the location updates state in SharedPreferences.
     *
     * @param requestingLocationUpdates The location updates state.
     */
    public static void setRequestingLocationUpdates(Context context, boolean requestingLocationUpdates) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(KEY_REQUESTING_LOCATION_UPDATES, requestingLocationUpdates).apply();
    }

    /**
     * Returns the {@code location} object as a human readable string.
     *
     * @param location The {@link Location}.
     */
    public static String getLocationText(Location location) {
        return location == null ? "Unknown location" : "(" + location.getLatitude() + ", " + location.getLongitude() + ")";
    }

    public static String getLocationTitle(Context context) {
        return context.getString(R.string.location_updated, DateFormat.getDateTimeInstance().format(new Date()));
    }

    @RequiresApi(Build.VERSION_CODES.O)
    public static void createChannel(NotificationManager mNotificationManager, String channelId, String channelName, String channelDescription, int importance) {

        NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);

        // Configure the notification channel.
        mChannel.setDescription(channelDescription);
        mChannel.setShowBadge(false);
        mChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        mChannel.enableVibration(true);
        mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        mNotificationManager.createNotificationChannel(mChannel);
    }

    public static void setLocationDataUploadWorker(Context mContext) {

        SessionManager session = new SessionManager(mContext);
        String workerUuid = session.getDataByKey(SessionManager.KEY_WORKER_UUID_LOCATION);
        if (workerUuid.equals("")) {

            // Create a Constraints object that defines when the task should run
            Constraints constraints = new Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build();

            PeriodicWorkRequest.Builder dayWorkBuilder =
                    new PeriodicWorkRequest.Builder(LocationDataWorker.class, 5, TimeUnit.MINUTES, 5,
                            TimeUnit.MINUTES);

            // Add Internet connectivity constraint
            dayWorkBuilder.setConstraints(constraints);
            // Add Tag to workBuilder
            dayWorkBuilder.addTag(AppConstants.LOCATION_DATA_UPLOAD_WORK_NAME);
            // Create the actual work object:
            PeriodicWorkRequest dayWork = dayWorkBuilder.build();
            // Then enqueue the recurring task:
            WorkManager.getInstance().enqueue(dayWork);

            // Set UUID in session for future use
            UUID workerId = dayWork.getId();
            session.storeDataByKey(SessionManager.KEY_WORKER_UUID_LOCATION, workerId.toString());
            Logger.e("Save Worker UUID", workerId.toString());
        } else {
            Logger.e("Stored Worker UUID", workerUuid);
        }
    }

    /**
     * Cancel work using the work's unique id
     */
    public static void cancelWorkByUUID(Context mContext) {
        SessionManager session = new SessionManager(mContext);
        String workerUuid = session.getDataByKey(SessionManager.KEY_WORKER_UUID_LOCATION);
        Logger.e("Canceling Worker UUID", workerUuid);
        if (workerUuid != null && workerUuid.length() > 0) {
            WorkManager.getInstance().cancelWorkById(UUID.fromString(workerUuid));
            WorkManager.getInstance().pruneWork();
        }
        session.storeDataByKey(SessionManager.KEY_WORKER_UUID_LOCATION, "");
    }

    /**
     * Cancel work using the work's unique name
     */
    public static void cancelWorkByTag(String workerUdi) {
        WorkManager.getInstance().cancelAllWorkByTag(AppConstants.LOCATION_DATA_UPLOAD_WORK_NAME);
    }
}
