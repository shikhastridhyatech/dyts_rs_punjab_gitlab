package com.trafficviolationdriver.util;

public class AppConstants {

    //  public static final String BASE_DOMAIN = "https://php10.shaligraminfotech.com/traffic_violation/public/";   //Local
//    public static final String BASE_DOMAIN = "https://php2.shaligraminfotech.com/dyts/public/";   //Local
    //        public static final String BASE_DOMAIN = "http://192.168.1.46/traffic_violation/public/";   //Local
//    public static final String BASE_DOMAIN = "http://106.201.238.169:3001";   //Client
//    public static final String BASE_DOMAIN = "http://www.google.com/";   //Server
  //  public static final String BASE_DOMAIN = "http://phpstack-301626-923392.cloudwaysapps.com/public/"; //client server
    public static final String BASE_DOMAIN = "https://www.shethsagainfinite.com/public/"; //client server

    // Crop video both side using this value from pause state
    public static final int VIDEO_CROP_ONE_SIDE_DURATION = 6000;
    public static final int CONFIG_TARGET_EVENTS = 10;
    public static final int CONFIG_QUOTA_MINUTES = 90;
    public static final String CONFIG_DATA_DELETE_TIME = "00:00";

    public static final String API_KEY = "AIzaSyBtWnExOjgjgPQRyVXZ1E0AvvSj75Y5Ods";

    // Local Broadcast Receiver
    public static final String INTENT_FILTER_MESSAGE_RECEIVED = "message_received.broadcast";

    // Image Selection
    public static final int CAMERA = 0x50;
    public static final int GALLERY = 0x51;


    public static final String IMEI = "IMEI";

    //Drawable Sides
    public static final int DRAWABLE_LEFT = 0x29;
    public static final int DRAWABLE_RIGHT = 0x2A;
    public static final int DRAWABLE_TOP = 0x2B;
    public static final int DRAWABLE_BOTTOM = 0x2C;

    public static final String RECORD_NEXT = "RECORD_NEXT";
    public static final String GO_TO_ANALYSIS = "GO_TO_ANALYSIS";
    public static final String GO_TO_HOME = "GO_TO_HOME";

    // Multi Languages
    public static final String EN = "en";
    public static final String GU = "gu";
    public static final String HI = "hi";
    public static final String MR = "mr";
    public static final String PA = "pa";
    public static final String BN = "bn";
    public static final String KN = "kn";
    public static final String TA = "ta";
    public static final String TE = "te";
    public static final String ML = "ml";
    public static final String OR = "or";
    public static final String BHO = "bho";

    // The name of the day increment work
    public static final String LOCATION_DATA_UPLOAD_WORK_NAME = "location_data_work";
    public static final String DIRECTORY_DATA_DELETE_WORK_NAME = "directory_data_delete_work";
    public static final String DIRECTORY_DATA_DELETE_FIRST_TIME_WORK_NAME = "directory_data_delete_first_time_work";
    public static final String EVENT_DATA_UPLOAD_WORK_NAME = "event_data_upload_work";
    public static final String VIDEO_ENCRYPTION_WORK = "video_encryption_work";

    public static final int NOTIFICATION_ID_FOREGROUND_SERVICE = 8466503;

    public static class ACTION {
        public static final String MAIN_ACTION = "test.action.main";
        public static final String START_ACTION = "test.action.start";
        public static final String STOP_ACTION = "test.action.stop";
    }

    public static class STATE_SERVICE {
        public static final int CONNECTED = 10;
        public static final int NOT_CONNECTED = 0;
    }
}
