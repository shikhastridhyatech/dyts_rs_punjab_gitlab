package com.trafficviolationdriver.dialogs;

import android.app.Activity;
import android.os.Bundle;

import com.trafficviolationdriver.R;

public class MyServiceAlertDialog extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String message = getIntent().hasExtra("Message") ? getIntent().getStringExtra("Message") : getString(R.string.something_went_wrong);

        new MessageDialog(this)
                .setMessage(message)
                .cancelable(false)
                .setPositiveButton(getString(R.string.ok),
                        (dialog, which) -> {
                            dialog.dismiss();
                            finish();
                        }).show();
    }
}