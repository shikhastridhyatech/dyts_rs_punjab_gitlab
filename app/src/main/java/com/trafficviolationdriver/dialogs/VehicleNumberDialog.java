package com.trafficviolationdriver.dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Binder;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.trafficviolationdriver.R;
import com.trafficviolationdriver.databinding.DialogVehicleNumberOldBinding;
import com.trafficviolationdriver.interfaces.OnBottomSheetClickListener;
import com.trafficviolationdriver.util.SessionManager;
import com.trafficviolationdriver.util.Utils;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Created by ${RICHA} on 12-04-2019.
 */
public class VehicleNumberDialog extends AlertDialog implements View.OnClickListener {
	private boolean cancelable = true;
	private String title, message;
	private String stateNumber = "", cityNumber = "", seriesNumber = "", vehicleNumber = "";
	private Context context;
	private String regexNumberPlate = "^[A-Z]{2}[ -][0-9]{1,2}(?: [A-Z])?(?: [A-Z]*){1,2}? [0-9]{4}$";
	private DialogVehicleNumberOldBinding mBinder;
	private String imagePath = "", videoPath = "";
	private long challanCaptureTime = 0;
	private int videoId = 0, eventId = 0;
	int orgX = 0, orgY = 0;
	int offsetX, offsetY;
	private Activity activity;
	char aChar;
	int index;

	private OnBottomSheetClickListener<String> mListener;
	private SessionManager sessionManager;

	public VehicleNumberDialog(Context context, Activity activity) {
		super(context, R.style.DialogWithAnimation);
		this.context = context;
		this.activity = activity;
	}


	public void setVehicleNumber(String numberPlateData) {
		List<String> months = Arrays.asList(context.getResources().getStringArray(R.array.state_list));

		String strStateCode = new String("");
		String strState = new String("");
		mBinder.etTwo.setText("");
		mBinder.etThree.setText("");
		mBinder.etFour.setText("");
		try {
			if (numberPlateData != null) {
				for (int i = 0; i < numberPlateData.length(); i++) {
					aChar = numberPlateData.charAt(i);

					if (Character.isDigit(aChar)) {
						strStateCode = strStateCode + aChar;
					} else if (Character.isLetter(aChar)) {
						strState = strState + aChar;
					}

				}
				if(strStateCode.length() >2){

					vehicleNumber = strStateCode.substring(2, 6);
				}if(strState.length() >2){


					seriesNumber = strState.substring(2, 4);
				}
				cityNumber = strStateCode.substring(0, 2);
				stateNumber = strState.substring(0, 2);

				for (int i = 0; i <= months.size() - 1; i++) {
					if (stateNumber.equals(months.get(i))) {
						index = i;
						if(numberPlateData.length()>=2 && numberPlateData.substring(0, 2).equals(months.get(i))){
							mBinder.etOne.setSelection(index);

						}
						break;
					}

				}
				mBinder.etTwo.setText(cityNumber);
				mBinder.etThree.setText(seriesNumber);
				mBinder.etFour.setText(vehicleNumber);

               /*  if(mBinder.etOne != null && mBinder.etTwo != null && mBinder.etThree != null && mBinder.etFour != null) {
                     Log.d("File Found", "File not found: " );
                     mBinder.etFour.requestFocus();
                     mBinder.etFour.setSelection(mBinder.etFour.getText().length());
                }
                else {
                     Log.d("space Found", "File not found: " );

                     mBinder.etTwo.setSelection(mBinder.etTwo.getText().length());
                }*/
			}


		} catch (Exception e) {
			e.printStackTrace();
		}



	}

	public VehicleNumberDialog setTitle(String title) {
		this.title = title;
		return this;
	}

	public VehicleNumberDialog setMessage(String message) {
		this.message = message;
		return this;
	}

	public VehicleNumberDialog cancelable(boolean cancelable) {
		this.cancelable = cancelable;
		return this;
	}

	public VehicleNumberDialog setListener(OnBottomSheetClickListener<String> listener) {
		this.mListener = listener;
		return this;
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mBinder = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_vehicle_number_old, null, false);
		sessionManager = new SessionManager(context);

		setContentView(mBinder.getRoot());
		setCanceledOnTouchOutside(cancelable);
		setCancelable(cancelable);
		this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		//Month
		List<String> months = Arrays.asList(context.getResources().getStringArray(R.array.state_list));
		ArrayAdapter<String> monthAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, months);
		monthAdapter.setDropDownViewResource(R.layout.layout_dropdown_item);
		mBinder.etOne.setAdapter(monthAdapter);



		mBinder.etOne.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				stateNumber = parent.getItemAtPosition(position).toString();
				sessionManager.storeDataByKey(SessionManager.KEY_USER_STATE_FLAG, position);
				mBinder.etTwo.requestFocus();
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {

			}
		});

		  mBinder.etOne.setSelection(sessionManager.getDataByKey(SessionManager.KEY_USER_STATE_FLAG, 0));
		mBinder.tvTitle.setVisibility(title != null ? View.VISIBLE : View.GONE);
		if (title != null) mBinder.tvTitle.setText(title);

		mBinder.tvMessage.setVisibility(message != null ? View.VISIBLE : View.GONE);
		if (message != null) mBinder.tvMessage.setText(message);

		if (cityNumber != null) mBinder.etTwo.setText(cityNumber);

		if (seriesNumber != null) mBinder.etThree.setText(seriesNumber);

		if (vehicleNumber != null) {
			mBinder.etFour.setText(vehicleNumber);
			mBinder.etFour.requestFocus();
		}

		mBinder.tvButtonPositive.setOnClickListener(this);
		mBinder.tvButtonNegative.setOnClickListener(this);

		mBinder.layoutMain.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						orgX = (int) event.getX();
						orgY = (int) event.getY();
						break;

					case MotionEvent.ACTION_MOVE:
						offsetX = (int) event.getRawX() - orgX;
						offsetY = (int) event.getRawY() - orgY;

						WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
						layoutParams.gravity = Gravity.TOP | Gravity.CENTER;
						layoutParams.x = offsetX;
						layoutParams.y = offsetY;
						getWindow().setAttributes(layoutParams);
						break;
				}
				return true;
			}
		});

		if(mBinder.etOne != null && mBinder.etTwo != null && mBinder.etThree != null && mBinder.etFour != null) {
			Log.d("File Found", "File not found: " );
			mBinder.etTwo.clearFocus();
			mBinder.etThree.clearFocus();
			mBinder.etFour.setSelection(mBinder.etFour.getText().length());
		}
		else {
			Log.d("space Found", "File not found: " );
			mBinder.etTwo.requestFocus();
			mBinder.etTwo.setSelection(mBinder.etTwo.getText().length());
		}

		mBinder.etTwo.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (mBinder.etTwo.getText().toString().length() == 2) {
					mBinder.etThree.requestFocus();
				}
			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			public void afterTextChanged(Editable s) {
			}

		});

		mBinder.etThree.addTextChangedListener(new TextWatcher() {

			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (mBinder.etThree.getText().toString().length() == 2) {
					mBinder.etFour.requestFocus();
				}
			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			public void afterTextChanged(Editable s) {
			}
		});

		mBinder.etFour.addTextChangedListener(new TextWatcher() {

			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (mBinder.etFour.getText().toString().length() == 4) {
					Utils.hideSoftKeyboard(activity, mBinder.etFour);
				}
			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			public void afterTextChanged(Editable s) {
			}
		});

		mBinder.etFour.setOnKeyListener(new View.OnKeyListener() {

			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (event.getAction() == KeyEvent.ACTION_DOWN) {
					if (keyCode == KeyEvent.KEYCODE_DEL) {
						if (mBinder.etFour.getText().length() == 0) {
							mBinder.etThree.requestFocus();
							mBinder.etThree.setSelection(mBinder.etThree.getText().length());
							return true;
						}
					}
				}
				return false;
			}
		});

		mBinder.etThree.setOnKeyListener(new View.OnKeyListener() {

			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (event.getAction() == KeyEvent.ACTION_DOWN) {
					if (keyCode == KeyEvent.KEYCODE_DEL) {
						if (mBinder.etThree.getText().length() == 0) {
							mBinder.etTwo.requestFocus();
							mBinder.etTwo.setSelection(mBinder.etTwo.getText().length());
							return true;
						}
					}
				}
				return false;
			}
		});
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.tvButtonPositive:
				if (validation()) {
					String numberPlate = getNumberPlate().replaceAll(" ", "-");
					Utils.hideSoftKeyboard(activity, mBinder.etFour);
					if (mListener != null) mListener.onItemClicked(numberPlate, this);
				}
				break;
			case R.id.tvButtonNegative:
				Utils.hideSoftKeyboard(activity, mBinder.etFour);
				if (mListener != null) mListener.onItemCancel(null);
				dismiss();
				break;
		}
	}


	private String getNumberPlate() {
		String stateCode = mBinder.etTwo.getText().toString().length() > 1 ? mBinder.etTwo.getText().toString() : "0" + mBinder.etTwo.getText().toString();
		String series = mBinder.etThree.getText().toString().toUpperCase();
		String vehicleNumber = mBinder.etFour.getText().toString();
		try {
			DecimalFormat formatter = new DecimalFormat("0000");
			vehicleNumber = formatter.format(Integer.parseInt(vehicleNumber));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return (stateNumber + " " + stateCode + " " + series + " " + vehicleNumber);
	}

	private boolean validation() {
		if (mBinder.etTwo.getText().toString().trim().equals("0") || mBinder.etTwo.getText().toString().trim().equals("00")) {
			Toast.makeText(context, context.getResources().getString(R.string.number_plate_not_valid), Toast.LENGTH_LONG).show();
			return false;
		} else if (mBinder.etThree.getText().toString().trim().length() < 1) {
			Toast.makeText(context, context.getResources().getString(R.string.number_plate_not_valid), Toast.LENGTH_LONG).show();
			return false;
		} else if (!(getNumberPlate()).matches(regexNumberPlate)) {
			Toast.makeText(context, context.getResources().getString(R.string.number_plate_not_valid), Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}

	@Override
	protected void onStart() {
		super.onStart();
		if (getWindow() != null)
			getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
	}


}