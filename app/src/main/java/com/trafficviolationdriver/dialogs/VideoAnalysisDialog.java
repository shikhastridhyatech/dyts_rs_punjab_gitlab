package com.trafficviolationdriver.dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.trafficviolationdriver.R;
import com.trafficviolationdriver.databinding.DialogVideoAnalysisBinding;
import com.trafficviolationdriver.interfaces.OnDialogClickListener;
import com.trafficviolationdriver.util.AppConstants;
import com.trafficviolationdriver.util.SessionManager;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Created by ${RICHA} on 08-04-2019.
 */
public class VideoAnalysisDialog extends AlertDialog implements View.OnClickListener {
    private boolean cancelable = true, onlyPositiveButton = false;
    private String message, title;
    private OnDialogClickListener<String> onActionSelected;
    private Context context;
    private DialogVideoAnalysisBinding mBinder;
    private SessionManager sessionManager;
    private Activity activity;

    public VideoAnalysisDialog(Context context) {
        super(context, R.style.DialogWithAnimation);
    }

    public VideoAnalysisDialog(Context context, Activity activity) {
        super(context, R.style.DialogWithAnimation);
        this.context = context;
        this.activity = activity;
    }

    public VideoAnalysisDialog(Context context, boolean onlyPositiveButton) {
        super(context, R.style.DialogWithAnimation);
        this.onlyPositiveButton = onlyPositiveButton;
    }

    public VideoAnalysisDialog setTitle(String title) {
        String title1 = title;
        return this;
    }


    public VideoAnalysisDialog setMessage(String message) {
        this.message = message;
        return this;
    }

    public VideoAnalysisDialog cancelable(boolean cancelable) {
        this.cancelable = cancelable;
        return this;
    }

    public VideoAnalysisDialog setActionClickListener(OnDialogClickListener<String> listener) {
        this.onActionSelected = listener;
        return this;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinder = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_video_analysis, null, false);
        setContentView(mBinder.getRoot());
        setCanceledOnTouchOutside(cancelable);
        setCancelable(cancelable);
        Objects.requireNonNull(this.getWindow()).clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        List<String> months = Arrays.asList(context.getResources().getStringArray(R.array.state_list));
        ArrayAdapter<String> monthAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, months);
        monthAdapter.setDropDownViewResource(R.layout.layout_dropdown_item);
        mBinder.tvTitle.setVisibility(title != null ? View.VISIBLE : View.GONE);
        if (title != null) mBinder.tvTitle.setText(title);
        mBinder.tvMessage.setVisibility(message != null ? View.VISIBLE : View.GONE);
        if (message != null) mBinder.tvMessage.setText(message);

        mBinder.tvRecordNext.setOnClickListener(this);
        mBinder.tvGoToDashboard.setOnClickListener(this);
        mBinder.tvGoToAnalysis.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_record_next:
                if (onActionSelected != null)
                    onActionSelected.onItemClicked(AppConstants.RECORD_NEXT);
                dismiss();
                break;
            case R.id.tv_go_to_analysis:
                if (onActionSelected != null)
                    onActionSelected.onItemClicked(AppConstants.GO_TO_ANALYSIS);
                dismiss();
                break;
            case R.id.tv_go_to_dashboard:
                if (onActionSelected != null)
                    onActionSelected.onItemClicked(AppConstants.GO_TO_HOME);
                dismiss();
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (getWindow() != null)
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }
}