package com.trafficviolationdriver.dialogs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.trafficviolationdriver.db.AppDatabase;
import com.trafficviolationdriver.service.VideoEncryptionService;
import com.trafficviolationdriver.ui.base.BaseActivity;
import com.trafficviolationdriver.util.AppConstants;
import com.trafficviolationdriver.util.Logger;
import com.trafficviolationdriver.util.Utils;

/**
 * Created by viraj.patel on 29-May-19
 */
public class VideoEncodeDialog extends Activity {

    AppDatabase appDatabase;

    String newFile, encryptedFilePath;
    int videoId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent().getExtras() != null) {
            newFile = getIntent().getStringExtra(VideoEncryptionService.KEY_INPUT_PATH);
            encryptedFilePath = getIntent().getStringExtra(VideoEncryptionService.KEY_OUTPUT_PATH);
            videoId = getIntent().getIntExtra(VideoEncryptionService.KEY_VIDEO_ID, -1);

            Logger.e("In VideoEncodeDialog");
            appDatabase = AppDatabase.getAppDatabase(this);

            if (!Utils.isServiceRunning(this, VideoEncryptionService.class)) {
                Intent mIntent = new Intent(this, VideoEncryptionService.class);
                mIntent.setAction(AppConstants.ACTION.START_ACTION);
                mIntent.putExtra(VideoEncryptionService.KEY_INPUT_PATH, newFile);
                mIntent.putExtra(VideoEncryptionService.KEY_OUTPUT_PATH, encryptedFilePath);
                mIntent.putExtra(VideoEncryptionService.KEY_VIDEO_ID, videoId);
                startService(mIntent);
                finish();
            } else {
                finish();
            }

            /*SecureVideoUtil.getInstance().doEncryption(VideoEncodeDialog.this, newFile, new File(encryptedFilePath), new OnVideoEncryptionDecryptionListener() {
                @Override
                public void onVideoOperationSucceed() {
                    VideoTableModel videoTableModel = appDatabase.videoDao().getVideoData(videoId);
                    videoTableModel.setEncryptedVideoPath(encryptedFilePath);
                    videoTableModel.setEncrypted(true);
                    videoTableModel.setEncryptedPercentage(100);
                    videoTableModel.setVideoPath("");
                    appDatabase.videoDao().updateVideoData(videoTableModel);
                    Utils.deleteVideo(newFile);

                    finish();
                }

                @Override
                public void onProgressUpdate(int progress) {

                }

                @Override
                public void onVideoOperationFailed(String message) {
                    finish();
                }
            });*/
        } else {
            finish();
        }
    }
}
