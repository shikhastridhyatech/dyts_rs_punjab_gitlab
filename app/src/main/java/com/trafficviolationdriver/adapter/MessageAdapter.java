package com.trafficviolationdriver.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.trafficviolationdriver.R;
import com.trafficviolationdriver.databinding.ItemMessageBinding;
import com.trafficviolationdriver.interfaces.OnRecyclerViewItemClicked;
import com.trafficviolationdriver.model.MessageModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by viraj.patel on 13-04-2019.
 */
public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    private ArrayList<MessageModel> mData;
    private Context mContext;
    private OnRecyclerViewItemClicked<MessageModel> mListener;

    public MessageAdapter(Context context, ArrayList<MessageModel> mData, OnRecyclerViewItemClicked<MessageModel> listener) {
        this.mContext = context;
        this.mData = mData;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemMessageBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.item_message, parent, false);
        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MessageModel item = mData.get(position);
        holder.bind(item);
    }

    @Override
    public int getItemCount() {
        if (mData != null && mData.size() > 0) return mData.size();
        else return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ItemMessageBinding mBinder;

        ViewHolder(ItemMessageBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;

            itemView.setOnClickListener(view -> {
                if (mListener != null) {
                    mListener.onItemClicked(mData.get(getAdapterPosition()));
                }
            });
        }

        void bind(MessageModel item) {
            mBinder.setMessage(item);
            mBinder.executePendingBindings();
        }
    }

    public void updateMessageList(ArrayList<MessageModel> messageList) {
        mData = messageList;
        notifyDataSetChanged();
    }
}