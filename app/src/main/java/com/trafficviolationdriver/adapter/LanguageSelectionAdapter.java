package com.trafficviolationdriver.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;


import com.trafficviolationdriver.R;
import com.trafficviolationdriver.databinding.ItemLanguageBinding;
import com.trafficviolationdriver.interfaces.OnRecyclerViewItemClicked;
import com.trafficviolationdriver.model.LanguageItemModel;
import com.trafficviolationdriver.util.SessionManager;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by viraj.patel on 13-04-2019.
 */
public class LanguageSelectionAdapter extends RecyclerView.Adapter<LanguageSelectionAdapter.ViewHolder> {

    private ArrayList<LanguageItemModel> mData;
    private Context mContext;
    private OnRecyclerViewItemClicked<LanguageItemModel> mListener;
    private String locale;
    private SessionManager session;

    public LanguageSelectionAdapter(Context context, ArrayList<LanguageItemModel> mData, OnRecyclerViewItemClicked<LanguageItemModel> listener) {
        this.mContext = context;
        this.mData = mData;
        this.mListener = listener;
        session = new SessionManager(mContext);
        locale = session.getDataByKey(SessionManager.KEY_LANGUAGE, "").toLowerCase();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemLanguageBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.item_language, parent, false);
        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        LanguageItemModel item = mData.get(position);
        holder.mBinder.tvTitle.setText(item.getTitle());

//		holder.mBinder.ivImage.setVisibility(Objects.equals(locale, item.getLocal()) ? View.VISIBLE : View.INVISIBLE);
        holder.mBinder.cvLanguage.setCardBackgroundColor(Objects.equals(locale, item.getLocal()) ? mContext.getResources().getColor(R.color.grey_disable) : mContext.getResources().getColor(R.color.white));
//		holder.mBinder.cvLanguage.setAlpha(Objects.equals(locale, item.getLocal()) ? 0.4f : 1);
    }

    @Override
    public int getItemCount() {
        if (mData != null && mData.size() > 0) return mData.size();
        else return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ItemLanguageBinding mBinder;

        ViewHolder(ItemLanguageBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;

            itemView.setOnClickListener(view -> {
                if (mListener != null) {
                    mListener.onItemClicked(mData.get(getAdapterPosition()));
                }
            });
        }
    }
}