package com.trafficviolationdriver.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.databinding.ItemVideosBinding;
import com.trafficviolationdriver.db.entity.VideoTableModel;
import com.trafficviolationdriver.interfaces.OnRecyclerViewItemClicked;
import com.trafficviolationdriver.util.Logger;

import org.apache.commons.io.FilenameUtils;

import java.util.ArrayList;

/**
 * Created by viraj.patel on 13-04-2019.
 */
public class RecordedVideoListAdapter extends RecyclerView.Adapter<RecordedVideoListAdapter.ViewHolder> {

    private ArrayList<VideoTableModel> mData;
    private Context mContext;
    private OnRecyclerViewItemClicked<VideoTableModel> mListener;

    public RecordedVideoListAdapter(Context context, ArrayList<VideoTableModel> mData, OnRecyclerViewItemClicked<VideoTableModel> listener) {
        this.mContext = context;
        this.mData = mData;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemVideosBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.item_videos, parent, false);
        return new ViewHolder(mBinding);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        VideoTableModel item = mData.get(position);
        holder.bind(item);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.color.grey);
        requestOptions.error(R.color.grey);

        Glide.with(mContext)
                .load(item.getVideoThumbnail())
                .apply(requestOptions)
//                .thumbnail(Glide.with(mContext).load(item.getVideoPath()))
                .into(holder.mBinder.imgVideo);
        String[] videoInfo = FilenameUtils.getBaseName(item.getVideoName()).split("-");
        holder.mBinder.tvVideoDate.setText(videoInfo[0].substring(0, 2).concat("-").concat(videoInfo[0].substring(2, 4)).concat("-20").concat(videoInfo[0].substring(4, 6)));
        holder.mBinder.tvStartTime.setText(videoInfo[1].substring(0, 2).concat(":").concat(videoInfo[1].substring(2, 4)));
        holder.mBinder.tvEndTime.setText(videoInfo[2].substring(0, 2).concat(":").concat(videoInfo[2].substring(2, 4)));
    }

    @Override
    public int getItemCount() {
        if (mData != null && mData.size() > 0) return mData.size();
        else return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ItemVideosBinding mBinder;

        ViewHolder(ItemVideosBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;

            itemView.setOnClickListener(view -> {
                if (mListener != null) {
                 /*   if (mData.get(getAdapterPosition()).isEncrypted()) {
                        mListener.onItemClicked(mData.get(getAdapterPosition()));
                    } else {
                        Logger.e("Video not Encrypted");
                    }*/
                    if (mData.get(getAdapterPosition()).isEncrypted()) {
                        mListener.onItemClicked(mData.get(getAdapterPosition()));
                    } else {
                        Logger.e("Video not Encrypted");
                    }
                }
            });
        }

        void bind(VideoTableModel item) {
            mBinder.setVideos(item);
            mBinder.executePendingBindings();
        }
    }

    public void updateMessageList(ArrayList<VideoTableModel> messageList) {
        mData = messageList;
        notifyDataSetChanged();
    }
}