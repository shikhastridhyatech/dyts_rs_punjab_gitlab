package com.trafficviolationdriver.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.trafficviolationdriver.R;
import com.trafficviolationdriver.databinding.ItemViolationReasonsBinding;
import com.trafficviolationdriver.interfaces.OnRecyclerViewItemClicked;
import com.trafficviolationdriver.model.ViolationReasonsModel;

import java.util.ArrayList;

/**
 * Created by ${RICHA} on 08-04-2019.
 */
public class ViolationReasonsAdapter extends RecyclerView.Adapter<ViolationReasonsAdapter.ViewHolder> {

    private ArrayList<ViolationReasonsModel> mData;
    private Context mContext;
    private OnRecyclerViewItemClicked<ViolationReasonsModel> mListener;

    public ViolationReasonsAdapter(Context context, ArrayList<ViolationReasonsModel> mData, OnRecyclerViewItemClicked<ViolationReasonsModel> listener) {
        this.mContext = context;
        this.mData = mData;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemViolationReasonsBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.item_violation_reasons, parent, false);
        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ViolationReasonsModel item = mData.get(position);
        holder.bind(item);

        holder.mBinder.cvLanguage.setVisibility((item.getReasonValue() == null || item.getReasonValue().equals("null")) ? View.GONE : View.VISIBLE);
        holder.mBinder.tvReason.setText(String.valueOf(item.getReasonValue()));

    }

    @Override
    public int getItemCount() {
        if (mData != null && mData.size() > 0) return mData.size();
        else return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ItemViolationReasonsBinding mBinder;

        ViewHolder(ItemViolationReasonsBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;

            itemView.setOnClickListener(view -> {
                if (mListener != null) {
                    mListener.onItemClicked(mData.get(getAdapterPosition()));
                }
            });
        }

        void bind(ViolationReasonsModel item) {
            mBinder.setReasons(item);
            mBinder.executePendingBindings();
        }
    }

    public void updateViolationReasonsList(ArrayList<ViolationReasonsModel> violationReasonsList) {
        mData = violationReasonsList;
        notifyDataSetChanged();
    }

}