package com.trafficviolationdriver.interfaces;

public interface OnSnackBarActionListener {
    void onAction();
}
