package com.trafficviolationdriver.interfaces;

/**
 * Created by viraj.patel on 09-May-19
 */
public interface OnNotificationDataReceivedListener<T> {
    void onDataReceived(T data);
}
