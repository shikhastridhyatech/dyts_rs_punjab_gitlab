package com.trafficviolationdriver.interfaces;

import androidx.work.ListenableWorker;

/**
 * Created by viraj.patel on 11-Apr-19
 */
public interface OnLocationDataUploadListener {
    ListenableWorker.Result onSuccess();

    ListenableWorker.Result onFailure();
}
