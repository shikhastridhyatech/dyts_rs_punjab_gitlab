package com.trafficviolationdriver.interfaces;

/**
 * Created by viraj.patel on 29-May-19
 */
public interface OnVideoEncryptionDecryptionListener {
    void onVideoOperationSucceed();

    void onProgressUpdate(int progress);

    void onVideoOperationFailed(String message);
}
