package com.trafficviolationdriver.interfaces;

/**
 * Created by ${RICHA} on 22-04-2019.
 */
public interface OnDialogClickListener<T> {
    void onItemClicked(T object);

}
