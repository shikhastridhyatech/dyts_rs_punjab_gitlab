package com.trafficviolationdriver.interfaces;

import android.app.Dialog;

/**
 * Created by ${RICHA} on 08-04-2019.
 */
public interface OnBottomSheetClickListener<T> {
    void onItemClicked(T t, Dialog dialog);

    void onItemCancel(Dialog dialog);
}
