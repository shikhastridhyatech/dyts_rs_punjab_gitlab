package com.trafficviolationdriver.videoplay.player;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.TimeBar;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoListener;
import com.trafficviolationdriver.R;
import com.trafficviolationdriver.dialogs.MessageDialog;

import static com.google.android.exoplayer2.C.TIME_UNSET;

public class VideoPlayer implements Player.EventListener, TimeBar.OnScrubListener, VideoListener {
	private SimpleExoPlayer player;
	private OnProgressUpdateListener mUpdateListener;
	private Handler progressHandler;
	private Runnable progressUpdater;
	private Context context;
	private AlertDialog alertDialog;
	private String uri = "";
	private long lastChalanTime = 0;
	private VideoPlayFinished videoPlayFinishedListener;
	public AlertDialog myDialog;
	private VideoPlayCancelled videoPlayCancelledListener;
	private String TAG = VideoPlayer.class.getSimpleName();
	private boolean dialogShown;

	public VideoPlayer(Context context) {
		BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
		TrackSelection.Factory videoTrackSelectionFactory =
				new AdaptiveTrackSelection.Factory(bandwidthMeter);
		TrackSelector trackSelector =
				new DefaultTrackSelector(videoTrackSelectionFactory);
		LoadControl loadControl = new DefaultLoadControl();
		player = ExoPlayerFactory.newSimpleInstance(context, trackSelector);
		player.addListener(this);
		progressHandler = new Handler();
		this.context = context;
	}
	private MediaSource buildMediaSourceNew(Uri uri ){
		DataSource.Factory datasourceFactroy = new DefaultDataSourceFactory(context, Util.getUserAgent(context,context.getString(R.string.app_name)));
		return new ExtractorMediaSource.Factory(datasourceFactroy).createMediaSource(uri);
	}
	public void initMediaSource(Context context, String uri) {
		DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context, Util.getUserAgent(context, context.getString(R.string.app_name)));
		ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
//		MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).setExtractorsFactory(extractorsFactory).createMediaSource(Uri.parse(uri));
		/*ExtractorMediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
				.createMediaSource(Uri.parse(uri), null, null);*/
		MediaSource	videoSource=buildMediaSourceNew(Uri.parse(uri));
	/*	DashMediaSource mediaSource = new DashMediaSource(Uri.parse(uri), dataSourceFactory,
				new DefaultDashChunkSource.Factory(dataSourceFactory), null, null);*/
		player.prepare(videoSource);
		player.addVideoListener(this);
		this.uri = uri;
	}

	public SimpleExoPlayer getPlayer() {
		return player;
	}

	public void play(boolean play) {
		if (player != null)
			player.setPlayWhenReady(play);
		if (!play) {
			removeUpdater();
		}
	}

	public void setLastChalanTime(long time) {
		lastChalanTime = time;
	}

	public void setPlayBackParams(float speed) {
		PlaybackParameters param = new PlaybackParameters(speed);
		player.setPlaybackParameters(param);
	}

	public void release() {
		if (player != null)
			player.release();
		removeUpdater();
		player = null;
	}

	public boolean isPlaying() {
		if (player != null)
			return player.getPlayWhenReady();
		else
			return false;
	}

	@Override
	public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
		updateProgress();
	}

	@Override
	public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

	}

	@Override
	public void onLoadingChanged(boolean isLoading) {

	}

	@Override
	public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
		if (playbackState == Player.STATE_ENDED && playWhenReady) {
			Log.e("onPlayerStateChanged", String.valueOf(playbackState));
			//player back ended
			showAlertOfDeleteVideo();
			player.setPlayWhenReady(!playWhenReady);
			return;
		}

		updateProgress();

	}

	@Override
	public void onPlayerError(ExoPlaybackException error) {
		switch (error.type) {
			case ExoPlaybackException.TYPE_SOURCE:
				Log.e(TAG, "TYPE_SOURCE: " + error.getSourceException().getMessage());
				break;

			case ExoPlaybackException.TYPE_RENDERER:
				Log.e(TAG, "TYPE_RENDERER: " + error.getRendererException().getMessage());

				if (dialogShown) {
					return;
				} else {
					dialogShown = true;
					new MessageDialog(context).setMessage(context.getString(R.string.something_went_wrong)).setPositiveButton(context.getString(R.string.ok), (dialog12, which) -> {
						videoPlayCancelledListener.onCancelled();
						dialog12.dismiss();
					}).show();
				}
//				showDialog(context);
				break;

			case ExoPlaybackException.TYPE_UNEXPECTED:
				Log.e(TAG, "TYPE_UNEXPECTED: " + error.getUnexpectedException().getMessage());
				break;
		}
	}

	@Override
	public void onRepeatModeChanged(int repeatMode) {

	}

	@Override
	public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

	}

	private void showAlertOfDeleteVideo() {
		if (alertDialog != null && alertDialog.isShowing())
			return;
		alertDialog = new AlertDialog.Builder(context)
				.setTitle(context.getString(R.string.analysis_complete_alert))
				.setMessage(context.getString(R.string.do_you_want_to_complete_this_analysis))
				.setCancelable(false)
				.setPositiveButton(context.getString(R.string.ok), (dialog, which) -> {
           /*         File fdelete = new File(uri);
                    if (fdelete.exists()) {
                        if (fdelete.delete()) {
                            System.out.println("file Deleted :" + uri);
                        } else {
                            System.out.println("file not Deleted :" + uri);
                        }
                    }
                    ((Activity) context).finish();*/
					videoPlayFinishedListener.onFinished();
					dialog.dismiss();
				})
				.setNegativeButton(context.getString(R.string.cancel), (dialog, which) -> {
					if (player != null) {
						player.seekTo(0);
						player.setPlayWhenReady(false);
					}
					dialog.dismiss();
				})
				.show();
	}

	public void showDialog(Context context) {
		if (myDialog != null && myDialog.isShowing()) return;

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(context.getString(R.string.something_went_wrong));
		builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int arg1) {
				videoPlayCancelledListener.onCancelled();
				dialog.dismiss();
			}
		});
		builder.setCancelable(false);
		myDialog = builder.create();
		myDialog.show();
	}

	@Override
	public void onPositionDiscontinuity(int reason) {
		if (reason == Player.DISCONTINUITY_REASON_SEEK && player.getCurrentPosition() < lastChalanTime) {
			seekTo(lastChalanTime);
		}
		Log.e("onPositionDiscontinuity", "reason" + reason);
		Log.e("onPositionDiscontinuity", "current" + player.getCurrentPosition());
		Log.e("onPositionDiscontinuity", "Position" + player.getDuration());
		updateProgress();

	}

	@Override
	public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

	}

	@Override
	public void onSeekProcessed() {

	}

	@Override
	public void onScrubStart(TimeBar timeBar, long position) {

	}

	@Override
	public void onScrubMove(TimeBar timeBar, long position) {
		seekTo(position);
		updateProgress();
	}

	@Override
	public void onScrubStop(TimeBar timeBar, long position, boolean canceled) {
		seekTo(position);
		updateProgress();
	}

	private void updateProgress() {
		if (mUpdateListener != null) {
			if (player != null) {
				mUpdateListener.onProgressUpdate(
						player.getCurrentPosition(),
						player.getDuration() == TIME_UNSET ? 0L : player.getDuration(),
						player.getBufferedPosition());
			} else {
				BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();

				TrackSelection.Factory videoTrackSelectionFactory =
						new AdaptiveTrackSelection.Factory(bandwidthMeter);
				TrackSelector trackSelector =
						new DefaultTrackSelector(videoTrackSelectionFactory);
				player = ExoPlayerFactory.newSimpleInstance(context, trackSelector);
				mUpdateListener.onProgressUpdate(
						player.getCurrentPosition(),
						player.getDuration() == TIME_UNSET ? 0L : player.getDuration(),
						player.getBufferedPosition());
			}
		}
		initUpdateTimer();
	}

	private void initUpdateTimer() {
		long position = player.getCurrentPosition();
		int playbackState = player.getPlaybackState();
		long delayMs;
		if (playbackState != Player.STATE_IDLE && playbackState != Player.STATE_ENDED) {
			if (player.getPlayWhenReady() && playbackState == Player.STATE_READY) {
				delayMs = 1000 - (position % 1000);
				if (delayMs < 200) {
					delayMs += 1000;
				}
			} else {
				delayMs = 1000;
			}

			removeUpdater();
			progressUpdater = new Runnable() {
				@Override
				public void run() {
					updateProgress();
				}
			};

			progressHandler.postDelayed(progressUpdater, delayMs);
		}
	}

	private void removeUpdater() {
		if (progressUpdater != null)
			progressHandler.removeCallbacks(progressUpdater);
	}

	public void seekTo(long position) {
		player.seekTo(position);
	}

	public void setUpdateListener(OnProgressUpdateListener updateListener) {
		mUpdateListener = updateListener;
	}

	@Override
	public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
		if (mUpdateListener != null) {
			mUpdateListener.onFirstTimeUpdate(player.getDuration(), player.getCurrentPosition());
		}
	}

	@Override
	public void onRenderedFirstFrame() {

	}

	public void setVideoFinishedListener(VideoPlayFinished listener) {
		this.videoPlayFinishedListener = listener;

	}

	public void setVideoCancelledListener(VideoPlayCancelled listener) {
		this.videoPlayCancelledListener = listener;

	}

	public interface OnProgressUpdateListener {
		void onProgressUpdate(long currentPosition, long duration, long bufferedPosition);

		void onFirstTimeUpdate(long duration, long currentPosition);
	}

	public interface VideoPlayFinished {
		void onFinished();
	}

	public interface VideoPlayCancelled {
		void onCancelled();
	}
}
