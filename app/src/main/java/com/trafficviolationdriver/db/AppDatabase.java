package com.trafficviolationdriver.db;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.trafficviolationdriver.db.dao.ConfigDao;
import com.trafficviolationdriver.db.dao.EventDao;
import com.trafficviolationdriver.db.dao.LocationDao;
import com.trafficviolationdriver.db.dao.VideoDao;
import com.trafficviolationdriver.db.dao.ViolationReasonDao;
import com.trafficviolationdriver.db.entity.ConfigTableModel;
import com.trafficviolationdriver.db.entity.DriverLocationTableModel;
import com.trafficviolationdriver.db.entity.EventTableModel;
import com.trafficviolationdriver.db.entity.VideoTableModel;
import com.trafficviolationdriver.db.entity.ViolationReasonTableModel;


@Database(entities = {VideoTableModel.class, EventTableModel.class, DriverLocationTableModel.class, ConfigTableModel.class, ViolationReasonTableModel.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public abstract VideoDao videoDao();

    public abstract EventDao eventDao();

    public abstract LocationDao locationDao();

    public abstract ConfigDao configDao();

    public abstract ViolationReasonDao violationReasonDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "traffic_violation.db") // If you write .db then you can explore database and can check table and entries. In production mode remove .db and keep only name
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .fallbackToDestructiveMigration() //Todo: Remove this line in production mode
                            .addCallback(roomCallback)
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    private static RoomDatabase.Callback roomCallback = new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            // Set default data in Config data
//            INSTANCE.configDao().insertConfigData(new ConfigTableModel(CONFIG_TARGET_EVENTS, CONFIG_QUOTA_MINUTES, CONFIG_DATA_DELETE_TIME));
        }

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
        }
    };
}
