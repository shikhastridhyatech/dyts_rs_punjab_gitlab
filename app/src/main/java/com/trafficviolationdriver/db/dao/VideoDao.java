package com.trafficviolationdriver.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.trafficviolationdriver.db.entity.VideoTableModel;

import java.util.List;

/**
 * Created by viraj.patel on 06-Apr-19
 */
@Dao
public interface VideoDao {

    @Query("Select * from video_master where driverId=:driver_Id")
    LiveData<List<VideoTableModel>> getAllVideos(int driver_Id);

    @Query("Select COUNT(id) from video_master where driverId=:driver_Id")
    int getMyVideoCount(int driver_Id);

    @Query("Select * from video_master where id =:video_id")
    VideoTableModel getVideoData(int video_id);

    @Query("Select * from video_master order by endTimestamp ASC limit 1")
    VideoTableModel getFirstCapturedVideo();

    @Query("Select * from video_master order by id DESC limit 1")
    VideoTableModel getLastCapturedVideo();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(VideoTableModel video);

    @Update
    void updateVideoData(VideoTableModel video);

    @Query("UPDATE video_master SET challanCaptureTime = :captureTime where id =:video_id and challanCaptureTime < :captureTime")
    void updateChallanCaptureTime(int video_id, long captureTime);

    @Query("DELETE FROM video_master WHERE id =:id")
    void removeVideo(int id);

    @Query("DELETE FROM video_master")
    void removeAllVideos();

    @Query("Select * from video_master where driverId=:driver_Id")
    LiveData<List<VideoTableModel>> getAllRecordedVideos(int driver_Id);

    @Query("UPDATE video_master SET tempCapturedTimeStamp = :tempCapturedTimeStamp where id = :video_id")
    void updateTempChallanCaptureTime(int video_id, long tempCapturedTimeStamp);

    @Query("UPDATE video_master SET encryptedPercentage = :progress where id =:video_id")
    void updateVideoEncryptionProgress(int video_id, int progress);

    @Query("UPDATE video_master SET decryptedPercentage = :progress where id =:video_id")
    void updateVideoDecryptionProgress(int video_id, int progress);

    @Query("Select COUNT(id) from video_master where driverId=:driver_Id and isEncrypted=0")
    int getRemainingEncryptionVideos(int driver_Id);

    @Query("Select * from video_master where isEncrypted =0 and driverId=:driver_Id order by id asc")
    VideoTableModel getRemainingVideoDetail(int driver_Id);
}
