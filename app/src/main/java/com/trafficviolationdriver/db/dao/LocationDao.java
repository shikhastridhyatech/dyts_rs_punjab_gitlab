package com.trafficviolationdriver.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.trafficviolationdriver.db.entity.DriverLocationTableModel;

import java.util.List;

/**
 * Created by viraj.patel on 06-Apr-19
 */
@Dao
public interface LocationDao {

    @Query("select * from location_master")
    List<DriverLocationTableModel> getAllLocations();

    @Query("select * from location_master where videoId =:id")
    List<DriverLocationTableModel> getVideoLocations(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(DriverLocationTableModel video);

    @Delete
    void delete(DriverLocationTableModel violation);

    @Query("UPDATE location_master SET videoId =:video_id where videoId =-1")
    void updateLocationVideoId(int video_id);

    @Query("UPDATE location_master SET isSync = 1 where videoId =:video_id")
    void updateLocationSyncStatus(int video_id);

    @Query("DELETE from location_master where videoId =:video_id AND isSync = 1")
    void deleteVideoLocationData(int video_id);

    @Query("select COUNT(data) from location_master where isSync = 0")
    int getRemainingUploadCount();

    @Query("select videoId from location_master where isSync = 0 AND videoId != -1 ORDER BY videoId ASC LIMIT 1")
    int getRemainingUploadVideoId();

    @Query("select data from location_master where isSync = 0 AND videoId = (select videoId from location_master where isSync = 0 AND videoId != -1 ORDER BY videoId ASC LIMIT 1)")
    List<String> getUploadDataArray();

    @Query("select data from location_master where isSync = 0 AND videoId =:video_id")
    List<String> getUploadDataArray(int video_id);

    @Query("select * from location_master where videoId = :video_id  and timestamp = (SELECT min(timestamp) as dateTime FROM location_master where timestamp >= :captureTimestamp and videoId=:video_id is not NULL UNION SELECT max(timestamp) as dateTime FROM location_master where timestamp <= :captureTimestamp and videoId=:video_id is not NULL limit 1) order by abs(:captureTimestamp-timestamp) limit 1")
    DriverLocationTableModel getLocationFromTimestamp(int video_id, long captureTimestamp);
}
