package com.trafficviolationdriver.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.trafficviolationdriver.db.entity.EventTableModel;

import java.util.List;

/**
 * Created by viraj.patel on 06-Apr-19
 */
@Dao
public interface EventDao {

    @Query("Select * from event_master")
    List<EventTableModel> getAllEventData();

    @Query("Select * from event_master where isSync = 0")
    List<EventTableModel> getEventData();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertEventData(EventTableModel violation);

    @Delete
    void delete(EventTableModel event);

    @Query("UPDATE event_master SET isSync = 1 where id =:id")
    void updateEventSyncStatus(int id);

    @Query("DELETE FROM event_master WHERE id =:id")
    void removeEvent(int id);

    @Query("DELETE FROM event_master where id <> (Select id from event_master order by id desc limit 1) and isSync != 0")
    void removeAllEventsExceptLast();

    @Query("Select * from event_master order by id DESC limit 1")
    EventTableModel getLastEventData();

    @Query("Select COUNT(id) from event_master where driverId=:driver_Id and isSync = 0 order by id ASC limit 1")
    int getRemainingEventCount(int driver_Id);

    @Query("Select * from event_master where driverId=:driver_Id and isSync = 0 order by id ASC limit 1")
    EventTableModel getRemainingEvent(int driver_Id);

    /*@Transaction
    public void updateEventStatusAndRemoveRemainingEvents(int id) {
        removeEvent(id);
        removeAllEventsExceptLast();
    }*/
}
