package com.trafficviolationdriver.db.dao;

import androidx.room.Dao;
import androidx.room.Query;

import com.trafficviolationdriver.db.entity.ViolationReasonTableModel;

import java.util.List;

/**
 * Created by viraj.patel on 24-Apr-19
 */
@Dao
public interface ViolationReasonDao {

    @Query("Select * from violation_reason_master")
    List<ViolationReasonTableModel> getAllViolationReasons();

    @Query("Select * from violation_reason_master DES order by featureCount")
    List<ViolationReasonTableModel> getAllViolationReasonsOrderByFeatured();
}
