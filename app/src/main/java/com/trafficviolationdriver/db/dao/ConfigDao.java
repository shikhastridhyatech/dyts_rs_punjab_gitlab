package com.trafficviolationdriver.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.trafficviolationdriver.db.entity.ConfigTableModel;

/**
 * Created by viraj.patel on 13-May-19
 */
@Dao
public interface ConfigDao {

    @Query("Select * from config_master order by id desc limit 1")
    ConfigTableModel getConfigData();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertConfigData(ConfigTableModel config);

    @Query("UPDATE config_master SET remainingMinutes =:minutesInMilliseconds where id = (Select id from config_master where driverId =:driver_id order by id desc limit 1)")
    void updateRemainingQuota(int driver_id, long minutesInMilliseconds);

    @Query("UPDATE config_master SET targetEvents =:target_events, quotaMinutes =:quota_minutes, remainingMinutes =:remaining_minutes, deleteTime =:delete_time where id = (Select id from config_master where driverId =:driver_id order by id desc limit 1)")
    void updateConfigData(int driver_id, int target_events, int quota_minutes, long remaining_minutes, String delete_time);

    @Query("DELETE FROM config_master")
    void deleteAll();

    @Query("UPDATE config_master SET remainingMinutes = quotaMinutes where id = (Select id from config_master where driverId =:driver_id order by id desc limit 1)")
    void resetRemainingMinutes(int driver_id);
}
