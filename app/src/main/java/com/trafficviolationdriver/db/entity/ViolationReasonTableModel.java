package com.trafficviolationdriver.db.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by viraj.patel on 24-Apr-19
 */
@Entity(tableName = "violation_reason_master")
public class ViolationReasonTableModel {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("reasonId")
    @Expose
    @ColumnInfo(name = "reasonId")
    private int reasonId;

    @SerializedName("featureCount")
    @Expose
    @ColumnInfo(name = "featureCount")
    private int featureCount;

    @SerializedName("reason")
    @Expose
    @ColumnInfo(name = "reason")
    private String reason;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getReasonId() {
        return reasonId;
    }

    public void setReasonId(int reasonId) {
        this.reasonId = reasonId;
    }

    public int getFeatureCount() {
        return featureCount;
    }

    public void setFeatureCount(int featureCount) {
        this.featureCount = featureCount;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "ViolationReasonTableModel{" +
                "id=" + id +
                ", reasonId=" + reasonId +
                ", featureCount=" + featureCount +
                ", reason='" + reason + '\'' +
                '}';
    }
}
