package com.trafficviolationdriver.db.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by viraj.patel on 06-Apr-19
 */
@Entity(tableName = "event_master")
public class EventTableModel implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("driverId")
    @Expose
    @ColumnInfo(name = "driverId")
    private int driverId = 0;

    @SerializedName("videoId")
    @Expose
    @ColumnInfo(name = "videoId")
    private int videoId = -1;

    @SerializedName("challanCaptureTime")
    @Expose
    @ColumnInfo(name = "challanCaptureTime")
    private long challanCaptureTime = 0;

    @SerializedName("vehicleNo")
    @Expose
    @ColumnInfo(name = "vehicleNo")
    private String vehicleNo = "";

    @SerializedName("reasonId")
    @Expose
    @ColumnInfo(name = "reasonId")
    private int reasonId;

    @SerializedName("croppedVideoPath")
    @Expose
    @ColumnInfo(name = "croppedVideoPath")
    private String croppedVideoPath = "";

    @SerializedName("imagePath")
    @Expose
    @ColumnInfo(name = "imagePath")
    private String imagePath = "";

    @SerializedName("numberplatePath")
    @Expose
    @ColumnInfo(name = "numberplatePath")
    private String numberplatePath = "";

    @SerializedName("dateTime")
    @Expose
    @ColumnInfo(name = "dateTime")
    private long dateTime = 0;

    @SerializedName("latitude")
    @Expose
    @ColumnInfo(name = "latitude")
    private double latitude = 0;

    @SerializedName("longitude")
    @Expose
    @ColumnInfo(name = "longitude")
    private double longitude = 0;

    @SerializedName("isSync")
    @Expose
    @ColumnInfo(name = "isSync")
    private boolean isSync = false;


    public EventTableModel() {
    }

    public EventTableModel(int driverId, int videoId, long challanCaptureTime, String vehicleNo, int reasonId, String croppedVideoPath, String imagePath, String numberplatePath, long dateTime, double latitude, double longitude) {
        this.driverId = driverId;
        this.videoId = videoId;
        this.challanCaptureTime = challanCaptureTime;
        this.vehicleNo = vehicleNo;
        this.reasonId = reasonId;
        this.croppedVideoPath = croppedVideoPath;
        this.imagePath = imagePath;
        this.numberplatePath = numberplatePath;
        this.dateTime = dateTime;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDriverId() {
        return driverId;
    }

    public void setDriverId(int driverId) {
        this.driverId = driverId;
    }

    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }

    public long getChallanCaptureTime() {
        return challanCaptureTime;
    }

    public void setChallanCaptureTime(long challanCaptureTime) {
        this.challanCaptureTime = challanCaptureTime;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public int getReasonId() {
        return reasonId;
    }

    public void setReasonId(int reasonId) {
        this.reasonId = reasonId;
    }

    public String getCroppedVideoPath() {
        return croppedVideoPath;
    }

    public void setCroppedVideoPath(String croppedVideoPath) {
        this.croppedVideoPath = croppedVideoPath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getNumberplatePath() {
        return numberplatePath;
    }

    public void setNumberplatePath(String numberplatePath) {
        this.numberplatePath = numberplatePath;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    @Override
    public String toString() {
        return "EventTableModel{" +
                "id=" + id +
                ", driverId=" + driverId +
                ", videoId=" + videoId +
                ", challanCaptureTime=" + challanCaptureTime +
                ", vehicleNo='" + vehicleNo + '\'' +
                ", reasonId=" + reasonId +
                ", croppedVideoPath='" + croppedVideoPath + '\'' +
                ", imagePath='" + imagePath + '\'' +
                ", numberplatePath='" + numberplatePath + '\'' +
                ", dateTime=" + dateTime +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", isSync=" + isSync +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.driverId);
        dest.writeInt(this.videoId);
        dest.writeLong(this.challanCaptureTime);
        dest.writeString(this.vehicleNo);
        dest.writeInt(this.reasonId);
        dest.writeString(this.croppedVideoPath);
        dest.writeString(this.imagePath);
        dest.writeString(this.numberplatePath);
        dest.writeLong(this.dateTime);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeByte(this.isSync ? (byte) 1 : (byte) 0);
    }

    protected EventTableModel(Parcel in) {
        this.id = in.readInt();
        this.driverId = in.readInt();
        this.videoId = in.readInt();
        this.challanCaptureTime = in.readLong();
        this.vehicleNo = in.readString();
        this.reasonId = in.readInt();
        this.croppedVideoPath = in.readString();
        this.imagePath = in.readString();
        this.numberplatePath = in.readString();
        this.dateTime = in.readLong();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.isSync = in.readByte() != 0;
    }

    public static final Parcelable.Creator<EventTableModel> CREATOR = new Parcelable.Creator<EventTableModel>() {
        @Override
        public EventTableModel createFromParcel(Parcel source) {
            return new EventTableModel(source);
        }

        @Override
        public EventTableModel[] newArray(int size) {
            return new EventTableModel[size];
        }
    };
}
