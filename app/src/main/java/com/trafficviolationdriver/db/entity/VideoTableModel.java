package com.trafficviolationdriver.db.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by viraj.patel on 06-Apr-19
 */
@Entity(tableName = "video_master")
public class VideoTableModel {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("driverId")
    @Expose
    @ColumnInfo(name = "driverId")
    private String driverId = "";

    @SerializedName("videoPath")
    @Expose
    @ColumnInfo(name = "videoPath")
    private String videoPath = "";

    @SerializedName("videoName")
    @Expose
    @ColumnInfo(name = "videoName")
    private String videoName = "";

    @SerializedName("videoThumbnail")
    @Expose
    @ColumnInfo(name = "videoThumbnail")
    private String videoThumbnail = "";

    @SerializedName("duration")
    @Expose
    @ColumnInfo(name = "duration")
    private long duration = 0;

    @SerializedName("endTimestamp")
    @Expose
    @ColumnInfo(name = "endTimestamp")
    private long endTimestamp = 0;

    @SerializedName("challanCaptureTime")
    @Expose
    @ColumnInfo(name = "challanCaptureTime")
    private long challanCaptureTime = 0;

    @ColumnInfo(name = "tempCapturedTimeStamp")
    private long tempCapturedTimeStamp = 0;

    @SerializedName("encryptedVideoPath")
    @Expose
    @ColumnInfo(name = "encryptedVideoPath")
    private String encryptedVideoPath = "";

    @SerializedName("encryptedPercentage")
    @Expose
    @ColumnInfo(name = "encryptedPercentage")
    private int encryptedPercentage = 0;

    @SerializedName("isEncrypted")
    @Expose
    @ColumnInfo(name = "isEncrypted")
    private boolean isEncrypted;

    @SerializedName("decryptedVideoPath")
    @Expose
    @ColumnInfo(name = "decryptedVideoPath")
    private String decryptedVideoPath = "";

    @SerializedName("decryptedPercentage")
    @Expose
    @ColumnInfo(name = "decryptedPercentage")
    private int decryptedPercentage = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public String getVideoThumbnail() {
        return videoThumbnail;
    }

    public void setVideoThumbnail(String videoThumbnail) {
        this.videoThumbnail = videoThumbnail;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getEndTimestamp() {
        return endTimestamp;
    }

    public void setEndTimestamp(long endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    public long getChallanCaptureTime() {
        return challanCaptureTime;
    }

    public void setChallanCaptureTime(long challanCaptureTime) {
        this.challanCaptureTime = challanCaptureTime;
    }

    public long getTempCapturedTimeStamp() {
        return tempCapturedTimeStamp;
    }

    public void setTempCapturedTimeStamp(long tempCapturedTimeStamp) {
        this.tempCapturedTimeStamp = tempCapturedTimeStamp;
    }

    public String getEncryptedVideoPath() {
        return encryptedVideoPath;
    }

    public void setEncryptedVideoPath(String encryptedVideoPath) {
        this.encryptedVideoPath = encryptedVideoPath;
    }

    public int getEncryptedPercentage() {
        return encryptedPercentage;
    }

    public void setEncryptedPercentage(int encryptedPercentage) {
        this.encryptedPercentage = encryptedPercentage;
    }

    public boolean isEncrypted() {
        return isEncrypted;
    }

    public void setEncrypted(boolean encrypted) {
        isEncrypted = encrypted;
    }

    public String getDecryptedVideoPath() {
        return decryptedVideoPath;
    }

    public void setDecryptedVideoPath(String decryptedVideoPath) {
        this.decryptedVideoPath = decryptedVideoPath;
    }

    public int getDecryptedPercentage() {
        return decryptedPercentage;
    }

    public void setDecryptedPercentage(int decryptedPercentage) {
        this.decryptedPercentage = decryptedPercentage;
    }

    @Override
    public String toString() {
        return "VideoTableModel{" +
                "id=" + id +
                ", driverId='" + driverId + '\'' +
                ", videoPath='" + videoPath + '\'' +
                ", videoName='" + videoName + '\'' +
                ", videoThumbnail='" + videoThumbnail + '\'' +
                ", duration=" + duration +
                ", endTimestamp=" + endTimestamp +
                ", challanCaptureTime=" + challanCaptureTime +
                ", tempCapturedTimeStamp=" + tempCapturedTimeStamp +
                ", encryptedVideoPath='" + encryptedVideoPath + '\'' +
                ", encryptedPercentage=" + encryptedPercentage +
                ", isEncrypted=" + isEncrypted +
                ", decryptedVideoPath='" + decryptedVideoPath + '\'' +
                ", decryptedPercentage=" + decryptedPercentage +
                '}';
    }
}
