package com.trafficviolationdriver.db.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by viraj.patel on 13-May-19
 */
@Entity(tableName = "config_master")
public class ConfigTableModel {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("driverId")
    @Expose
    @ColumnInfo(name = "driverId")
    private int driverId = 0;

    @SerializedName("targetEvents")
    @Expose
    @ColumnInfo(name = "targetEvents")
    private int targetEvents;

    @SerializedName("quotaMinutes")
    @Expose
    @ColumnInfo(name = "quotaMinutes")
    private int quotaMinutes;

    @SerializedName("remainingMinutes")
    @Expose
    @ColumnInfo(name = "remainingMinutes")
    private long remainingMinutes;

    @SerializedName("deleteTime")
    @Expose
    @ColumnInfo(name = "deleteTime")
    private String deleteTime;

    public ConfigTableModel(int driverId, int targetEvents, int quotaMinutes, long remainingMinutes, String deleteTime) {
        this.driverId = driverId;
        this.targetEvents = targetEvents;
        this.quotaMinutes = quotaMinutes;
        this.remainingMinutes = remainingMinutes;
        this.deleteTime = deleteTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDriverId() {
        return driverId;
    }

    public void setDriverId(int driverId) {
        this.driverId = driverId;
    }

    public int getTargetEvents() {
        return targetEvents;
    }

    public void setTargetEvents(int targetEvents) {
        this.targetEvents = targetEvents;
    }

    public int getQuotaMinutes() {
        return quotaMinutes;
    }

    public void setQuotaMinutes(int quotaMinutes) {
        this.quotaMinutes = quotaMinutes;
    }

    public long getRemainingMinutes() {
        return remainingMinutes;
    }

    public void setRemainingMinutes(long remainingMinutes) {
        this.remainingMinutes = remainingMinutes;
    }

    public String getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(String deleteTime) {
        this.deleteTime = deleteTime;
    }

    @Override
    public String toString() {
        return "ConfigTableModel{" +
                "id=" + id +
                ", driverId=" + driverId +
                ", targetEvents=" + targetEvents +
                ", quotaMinutes=" + quotaMinutes +
                ", remainingMinutes=" + remainingMinutes +
                ", deleteTime='" + deleteTime + '\'' +
                '}';
    }
}
