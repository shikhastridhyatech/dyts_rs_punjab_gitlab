package com.trafficviolationdriver.db.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by viraj.patel on 06-Apr-19
 */
@Entity(tableName = "location_master"/*,
        foreignKeys = @ForeignKey(entity = VideoTableModel.class,
                parentColumns = "id",
                childColumns = "videoId",
                onDelete = CASCADE),
        indices = {@Index(value = "videoId")}*/)
public class DriverLocationTableModel {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("videoId")
    @Expose
    @ColumnInfo(name = "videoId")
    private int videoId = -1;

    @SerializedName("driverId")
    @Expose
    @ColumnInfo(name = "driverId")
    private String driverId = "";

    @SerializedName("latitude")
    @Expose
    @ColumnInfo(name = "latitude")
    private double latitude = 0;

    @SerializedName("longitude")
    @Expose
    @ColumnInfo(name = "longitude")
    private double longitude = 0;

    @SerializedName("timestamp")
    @Expose
    @ColumnInfo(name = "timestamp")
    private long timestamp = 0;

    @SerializedName("data")
    @Expose
    @ColumnInfo(name = "data")
    private String data = "";

    @SerializedName("isSync")
    @Expose
    @ColumnInfo(name = "isSync")
    private boolean isSync = false;

    public DriverLocationTableModel() {
    }

    public DriverLocationTableModel(int videoId, String driverId, double latitude, double longitude, long timestamp) {
        this.videoId = videoId;
        this.driverId = driverId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.timestamp = timestamp;
        this.data = "\"".concat(String.valueOf(latitude)).concat(",").concat(String.valueOf(longitude)).concat(",").concat(String.valueOf(timestamp)).concat("\"");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    @Override
    public String toString() {
        return "DriverLocationTableModel{" +
                "id=" + id +
                ", videoId=" + videoId +
                ", driverId='" + driverId + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", timestamp=" + timestamp +
                ", data='" + data + '\'' +
                ", isSync=" + isSync +
                '}';
    }
}
